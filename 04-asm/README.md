# 組合語言與虛擬機

* [x86 處理器上的組合語言](./doc/x86asm.md)
* [使用 gcc 觀察 x86 組合語言](./01-gcc)
* [虛擬機簡介](./03-vm)
* [Java 的 JVM 虛擬機](./04-jvm)
* [跨平台的 QEMU 虛擬機](./05-qemu)
* [HackVM 函數呼叫如何處理](./doc/hackVmCall.md)
