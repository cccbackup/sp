#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];
void disasm(uint8_t *m, uint32_t mTop) {
  printf("asm                  addr:code     T op f3 f7 rd rs1 rs2\n---------------------------------------------------------\n");
  for (pc = 0; pc<mTop; pc+=4) {
    ir = bytesToWord(&m[pc]);
    Op *o = decode(ir);
    dumpInstr(ir);
  }
}

// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  disasm(m, mTop);
  return 0;
}
