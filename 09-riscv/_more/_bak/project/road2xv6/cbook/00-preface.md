# xv6：一个简单的、类似Unix的教学操作系统。

Russ Cox Frans Kaashoek Robert Morris

2020年8月31日

## 前言和致谢

这是一篇为操作系统课准备的草稿，它通过研究一个名为xv6的内核例子来解释操作系统的主要概念。xv6以Dennis Ritchie和Ken Thompson的Unix Version 6 (v6)[14]为蓝本。xv6大致沿用了v6的结构和风格，但在多核RISC-V[12]中用ANSI C[6]实现。

这篇文章应该和xv6的源代码一起阅读，这种方法受到John Lions的UNIX第六版注释[9]的启发。参见https://pdos.csail.mit.edu/6.S081，以获得 v6 和 xv6 的在线资源指针，包括一些使用 xv6 的实验作业。

我们在麻省理工学院的操作系统课6.828和6.S081中使用过这篇课文。我们感谢这些课程的教师、助教和学生，他们都对xv6做出了直接或间接的贡献。我们尤其要感谢Adam Belay、Austin Clements和Nickolai Zeldovich。最后，我们要感谢那些通过电子邮件向我们发送文本中的错误或改进建议的人。Abutalib Aghayev、Sebastian Boehm、Anton Burtsev、Raphael Carvalho、Tej Chajed、Rasit Eskicioglu、Color Fuzzy、Giuseppe、Tao Guo、Naoki Hayama、Robert Hilderman、Wolfgang Keller、Austin Liew、Pavan Maddamsetti。Jacek Masiulaniec、Michael cConville、m3hm00d、miguelgvieira、Mark Morrissey、Harry Pan、Askar Safin、Salman Shah、Adeodato Simó、Ruslan Savchenko、Pawel Szczurko、Warren Toomey、tyfkda、tzerbib、Xi Wang、邹昌伟。

如果您发现错误或有改进建议，请发邮件给Frans Kaashoek和Robert Morris（kaashoek,rtm@csail.mit.edu）。


通过www.DeepL.com/Translator（免费版）翻译