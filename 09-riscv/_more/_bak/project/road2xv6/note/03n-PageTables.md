# Chapter 3 -- Page tables

页表是操作系统为每个进程提供自己私有地址空间和内存的机制。页表决定了内存地址的含义，以及物理内存的哪些部分可以被访问。它们允许xv6隔离不同进程的地址空间，并将它们复用到单个物理内存上。页表还提供了一个间接层次，允许xv6执行一些技巧：在几个地址空间中映射同一内存（一个蹦床页），以及用一个未映射的页来保护内核和用户堆栈。本章其余部分将解释RISC-V硬件提供的页表以及xv6如何使用它们。

## 3.1 Paging hardware

提醒一下，RISC-V指令(包括用户和内核)操纵的是虚拟地址。机器的RAM，也就是物理内存，是以物理地址为索引的。RISC-V页表硬件将这两种地址连接起来，通过将每个虚拟地址映射到物理地址。

xv6运行在Sv39 RISC-V上，这意味着只使用64位虚拟地址的底部39位，顶部25位没有使用。在这种Sv39配置中，一个RISC-V页表在逻辑上是一个`2^27(134,217,728)`页表项(PTE)的数组。每个PTE包含一个44位的物理页号（PPN）和一些标志。分页硬件翻译虚拟地址时，利用39位中的前27位索引到页表中找到一个PTE，并做出一个56位的物理地址，其前44位来自于PTE中的PPN，而其后12位则是从原虚拟地址中复制过来的。图3.1显示了这个过程，逻辑上把页表看成是一个简单的PTE数组（更完整的故事见图3.2）。页表让操作系统控制虚拟地址到物理地址的转换，其粒度为4096（2^12）字节的对齐块。这样的块称为页。

![](../img/Figure3.1.png)

在Sv39 RISC-V中，虚拟地址的前25位不用于翻译；将来，RISC-V可能会使用这些位来定义更多的翻译级别。物理地址也有增长的空间：在PTE格式中，物理页数还有10位的增长空间。

如图3.2所示，实际翻译分三步进行。一个页表以三层树的形式存储在物理内存中。树的根部是一个4096字节的页表页，它包含512个PTE，这些PTE包含树的下一级页表页的物理地址。这些页面中的每一个都包含512个PTE，用于树的最后一级。分页硬件用27位中的顶9位选择根页表页中的PTE，用中间9位选择树中下一级页表页中的PTE，用底9位选择最后的PTE。

如果翻译一个地址所需的三个PTE中的任何一个不存在，寻呼硬件就会引发一个页面错误的异常，让内核来处理这个异常（见第4章）。

这种三级结构允许页表在常见的大范围虚拟地址没有映射的情况下省略整个页表页。

每个PTE包含标志位，告诉寻呼硬件如何允许使用相关的虚拟地址。PTE_V表示PTE是否存在：如果没有设置，对该页的引用会引起异常（即不允许）。PTE_R控制是否允许指令读取到页面。PTE_W控制是否允许指令向页面写入。PTE_X控制CPU是否可以将页面的内容解释为指令并执行。

PTE_U控制是否允许用户模式下的指令访问页面；如果不设置PTE_U，PTE只能在主管模式下使用。图3.2显示了这一切的工作原理。标志和其他所有与页硬件相关的结构在(kernel/riscv.h)中定义 要告诉硬件使用页表，内核必须将根页表页的物理地址写入satp寄存器中。每个CPU都有自己的satp。一个CPU将使用自己的satp指向的页表来翻译后续指令产生的所有地址。

每个CPU都有自己的satp，这样不同的CPU可以运行不同的进程，每个进程都有自己的页表所描述的私有地址空间。

![](../img/Figure3.2.png)

关于术语的一些说明。物理内存指的是DRAM中的存储单元。物理存储器的一个字节有一个地址，称为物理地址。指令只使用虚拟地址，分页硬件将其翻译成物理地址，然后发送给DRAM硬件，以读取或写入存储。与物理内存和虚拟地址不同，虚拟内存并不是一个物理对象，而是指内核提供的管理物理内存和虚拟地址的抽象和机制的集合。

## 3.2 Kernel address space

Xv6为每个进程维护一个页表，描述每个进程的用户地址空间，另外还有一个描述内核地址空间的单页表。内核配置其地址空间的布局，使自己能够在可预测的虚拟地址上访问物理内存和各种硬件资源。图3.3显示了这种布局如何将内核虚拟地址映射到物理地址。文件（kernel/memlayout.h）声明了xv6的内核内存布局的常量

kernel/memlayout.h

```cpp
// Physical memory layout

// qemu -machine virt is set up like this,
// based on qemu's hw/riscv/virt.c:
//
// 00001000 -- boot ROM, provided by qemu
// 02000000 -- CLINT
// 0C000000 -- PLIC
// 10000000 -- uart0 
// 10001000 -- virtio disk 
// 80000000 -- boot ROM jumps here in machine mode
//             -kernel loads the kernel here
// unused RAM after 80000000.
```

QEMU模拟的计算机包括RAM（物理内存），从物理地址0x80000000开始，一直到至少0x86400000，xv6称之为PHYSTOP。

QEMU模拟还包括I/O设备，如磁盘接口。QEMU将设备接口作为内存映射的控制寄存器暴露给软件，这些寄存器位于物理地址空间的0x80000000以下。内核可以通过读取/写入这些特殊的物理地址与设备进行交互；这种读取和写入与设备硬件而不是与RAM进行通信。第4章解释了xv6如何与设备交互。

![](../img/Figure3.3.png)

内核通过 "直接映射 "的方式获取RAM和内存映射的设备寄存器，也就是将资源映射到与物理地址相等的虚拟地址上，例如，内核本身在虚拟地址空间和物理内存中都位于KERNBASE=0x80000000处。例如，内核本身在虚拟地址空间和物理内存中的位置都是KERNBASE=0x80000000。直接映射简化了读或写物理内存的内核代码。例如,当fork为子进程分配用户内存时,分配器返回该内存的物理地址;fork在将父进程的用户内存复制到子进程时,直接将该地址作为虚拟地址。

有几个内核虚拟地址不是直接映射的。

* 蹦床页。它被映射在虚拟地址空间的顶端；用户页表也有这种映射。第4章讨论了蹦床页的作用，但我们在这里看到了页表的一个有趣的用例；一个物理页（存放蹦床代码）在内核的虚拟地址空间中被映射了两次：一次是在虚拟地址空间的顶部，一次是直接映射。
* 内核栈页。每个进程都有自己的内核栈，内核栈被映射得很高，所以在它下面xv6可以留下一个未映射的守卫页。守护页的PTE是无效的(即PTE_V没有设置)，这样如果内核溢出内核栈，很可能会引起异常，内核会恐慌。如果没有防护页，溢出的堆栈会覆盖其他内核内存，导致不正确的操作。恐慌性崩溃是比较好的。

当内核通过高内存映射使用堆栈时，它们也可以通过直接映射的地址被内核访问。另一种设计可能只有直接映射，并在直接映射的地址上使用堆栈。然而，在这种安排中，提供保护页将涉及到取消映射虚拟地址，否则这些地址将指向物理内存，这将很难使用。

内核将蹦床和内核文本的页面映射为PTE_R和PTE_X权限。内核从这些页面读取和执行指令。内核用PTE_R和PTE_W权限映射其他页面，这样它就可以读写这些页面的内存。守护页的映射是无效的。

## 3.3 Code: creating an address space

riscv.h

```cpp
typedef uint64 pte_t;
typedef uint64 *pagetable_t; // 512 PTEs
```

大部分的xv6操作地址空间和页表的代码都在vm.c(kernel/vm.c:1)中。中心数据结构是pagetable_t，它实际上是一个指向RISC-V根页表页的指针；pagetable_t可以是内核页表，也可以是每个进程的页表之一。核心函数是Walk和mappages，前者为虚拟地址寻找PTE，后者为新的映射安装PTE。以 kvm 开头的函数操作内核页表；以 uvm 开头的函数操作用户页表；其他函数同时用于这两个方面。copyout 和 copyin 将数据复制到作为系统调用参数提供的用户虚拟地址，并从这些地址复制数据；它们在 vm.c 中，因为它们需要显式翻译这些地址，以便找到相应的物理内存。

在启动序列的早期，main调用kvminit(kernel/vm.c:22)来创建内核的页表，这个调用发生在xv6在RISC-V上启用分页之前，所以地址直接指物理内存。这个调用发生在xv6在RISC-V上启用分页之前，所以地址直接指向物理内存。Kvminit首先分配一页物理内存来存放根页表页。然后调用kvmmap来安装内核需要的翻译。这些翻译包括内核的指令和数据，物理内存到PHYSTOP，以及实际上是设备的内存范围。

```cpp
/*
 * the kernel's page table.
 */
pagetable_t kernel_pagetable;

extern char etext[];  // kernel.ld sets this to end of kernel code.

extern char trampoline[]; // trampoline.S

// Make a direct-map page table for the kernel.
pagetable_t
kvmmake(void)
{
  pagetable_t kpgtbl;

  kpgtbl = (pagetable_t) kalloc();
  memset(kpgtbl, 0, PGSIZE);

  // uart registers
  kvmmap(kpgtbl, UART0, UART0, PGSIZE, PTE_R | PTE_W);

  // virtio mmio disk interface
  kvmmap(kpgtbl, VIRTIO0, VIRTIO0, PGSIZE, PTE_R | PTE_W);

  // PLIC
  kvmmap(kpgtbl, PLIC, PLIC, 0x400000, PTE_R | PTE_W);

  // map kernel text executable and read-only.
  kvmmap(kpgtbl, KERNBASE, KERNBASE, (uint64)etext-KERNBASE, PTE_R | PTE_X);

  // map kernel data and the physical RAM we'll make use of.
  kvmmap(kpgtbl, (uint64)etext, (uint64)etext, PHYSTOP-(uint64)etext, PTE_R | PTE_W);

  // map the trampoline for trap entry/exit to
  // the highest virtual address in the kernel.
  kvmmap(kpgtbl, TRAMPOLINE, (uint64)trampoline, PGSIZE, PTE_R | PTE_X);

  // map kernel stacks
  proc_mapstacks(kpgtbl);
  
  return kpgtbl;
}

// Initialize the one kernel_pagetable
void
kvminit(void)
{
  kernel_pagetable = kvmmake();
}
// ...
// add a mapping to the kernel page table.
// only used when booting.
// does not flush TLB or enable paging.
void
kvmmap(pagetable_t kpgtbl, uint64 va, uint64 pa, uint64 sz, int perm)
{
  if(mappages(kpgtbl, va, sz, pa, perm) != 0)
    panic("kvmmap");
}

//...
// Create PTEs for virtual addresses starting at va that refer to
// physical addresses starting at pa. va and size might not
// be page-aligned. Returns 0 on success, -1 if walk() couldn't
// allocate a needed page-table page.
int
mappages(pagetable_t pagetable, uint64 va, uint64 size, uint64 pa, int perm)
{
  uint64 a, last;
  pte_t *pte;

  a = PGROUNDDOWN(va);
  last = PGROUNDDOWN(va + size - 1);
  for(;;){
    if((pte = walk(pagetable, a, 1)) == 0)
      return -1;
    if(*pte & PTE_V)
      panic("remap");
    *pte = PA2PTE(pa) | perm | PTE_V;
    if(a == last)
      break;
    a += PGSIZE;
    pa += PGSIZE;
  }
  return 0;
}
```

kvmmap (kernel/vm.c:118) 调用 mappages (kernel/vm.c:149)，它将一个虚拟地址范围的映射安装到页表中，并将其映射到相应的物理地址范围。它对范围内的每一个虚拟地址按页间隔分别进行安装。对于每个要映射的虚拟地址，mapages调用walk找到该地址的PTE的地址。然后，它初始化PTE，使其持有相关的物理页号、所需的权限(PTE_W、PTE_X和/或PTE_R)，以及PTE_V来标记PTE为有效(kernel/vm.c:161)。

walk (kernel/vm.c:72)模仿RISC-V分页硬件查找虚拟地址的PTE(见图3.2).walk每次下降3级页表的9位。它利用每一级的9位虚拟地址来查找下一级页表或最后一页（kernel/vm.c:78）的PTE。如果PTE无效，则说明所需的页还没有被分配；如果设置了alloc参数，walk会分配一个新的页表页，并把它的物理地址放在PTE中。它返回树中最低层的PTE的地址(kernel/vm.c:88)。


vm.c

```cpp
// Return the address of the PTE in page table pagetable
// that corresponds to virtual address va.  If alloc!=0,
// create any required page-table pages.
//
// The risc-v Sv39 scheme has three levels of page-table
// pages. A page-table page contains 512 64-bit PTEs.
// A 64-bit virtual address is split into five fields:
//   39..63 -- must be zero.
//   30..38 -- 9 bits of level-2 index.
//   21..29 -- 9 bits of level-1 index.
//   12..20 -- 9 bits of level-0 index.
//    0..11 -- 12 bits of byte offset within the page.
pte_t *
walk(pagetable_t pagetable, uint64 va, int alloc)
{
  if(va >= MAXVA)
    panic("walk");

  for(int level = 2; level > 0; level--) {
    pte_t *pte = &pagetable[PX(level, va)];
    if(*pte & PTE_V) {
      pagetable = (pagetable_t)PTE2PA(*pte);
    } else {
      if(!alloc || (pagetable = (pde_t*)kalloc()) == 0)
        return 0;
      memset(pagetable, 0, PGSIZE);
      *pte = PA2PTE(pagetable) | PTE_V;
    }
  }
  return &pagetable[PX(0, va)];
}
```

上述代码依赖于物理内存直接映射到内核虚拟地址空间。例如，当 walk 下降页表的级别时，它从 PTE 中提取下一级页表的（物理）地址（kernel/vm.c:80），然后使用该地址作为虚拟地址来获取下一级的 PTE（kernel/vm.c:78）。

main 调用 kvminithart (kernel/vm.c:53) 来安装内核页表。它将根页表页的物理地址写入寄存器satp中。在这之后，CPU将使用内核页表翻译地址。由于内核使用單元映射 (identity mapping)，所以下一条指令的虚拟地址将映射到正确的物理内存地址。

```cpp
// start() jumps here in supervisor mode on all CPUs.
void
main()
{
  if(cpuid() == 0){
    consoleinit();
    printfinit();
    printf("\n");
    printf("xv6 kernel is booting\n");
    printf("\n");
    kinit();         // physical page allocator
    kvminit();       // create kernel page table
    kvminithart();   // turn on paging
    procinit();      // process table
    trapinit();      // trap vectors
    trapinithart();  // install kernel trap vector
    plicinit();      // set up interrupt controller
    plicinithart();  // ask PLIC for device interrupts
    binit();         // buffer cache
    iinit();         // inode cache
    fileinit();      // file table
    virtio_disk_init(); // emulated hard disk
    userinit();      // first user process
    __sync_synchronize();
    started = 1;
  } else {
    while(started == 0)
      ;
    __sync_synchronize();
    printf("hart %d starting\n", cpuid());
    kvminithart();    // turn on paging
    trapinithart();   // install kernel trap vector
    plicinithart();   // ask PLIC for device interrupts
  }

  scheduler();        
}

```

procinit (kernel/proc.c:26)，它由main调用，为每个进程分配一个内核栈。kvmmap将映射的PTE添加到内核页表中，调用kvminithart将内核页表重新加载到satp中，这样硬件就知道新的PTE了。

每个RISC-V CPU都会在Translation Look-aside Buffer(TLB)中缓存页表项，当xv6改变页表时，必须告诉CPU使相应的缓存TLB项无效。如果它没有这样做，那么在以后的某个时刻，TLB可能会使用一个旧的缓存映射，指向一个物理页，而这个物理页在此期间已经分配给了另一个进程，结果，一个进程可能会在其他进程的内存上乱写乱画。RISC-V有一条指令sfence.vma，可以刷新当前CPU的TLB。xv6在重新加载satp寄存器后，在kvminithart中执行sfence.vma，在返回用户空间前切换到用户页表的trampoline代码中执行sfence.vma（kernel/trampoline.S:79）。

## 3.4 Physical memory allocation

内核必须在运行时为页表、用户内存、内核堆栈和管道缓冲区分配和释放物理内存。

xv6使用内核结束和PHYSTOP之间的物理内存进行运行时分配。它每次分配和释放整个4096字节的页面。它通过对页面本身的链接列表进行线程化，来跟踪哪些页面是空闲的。分配包括从链接列表中删除一个页面；释放包括将释放的页面添加到列表中。

## 3.5 Code: Physical memory allocator

分配器驻留在kalloc.c（kernel/kalloc.c:1）中。分配器的数据结构是一个可供分配的物理内存页的空闲列表，每个空闲页的列表元素是一个结构run（kernel/kalloc.c:17）。每个空闲页的列表元素是一个结构run(kernel/kalloc.c:17)。分配器从哪里获得内存来存放这个数据结构呢？它把每个空闲页的run结构存储在空闲页本身，因为那里没有其他东西存储。空闲列表由一个自旋锁保护(kernel/kalloc.c:21-24)。列表和锁被包裹在一个结构中，以明确锁保护的是结构中的字段。现在，忽略锁以及获取和释放的调用；第6章将详细研究锁。

```cpp
// Physical memory allocator, for user processes,
// kernel stacks, page-table pages,
// and pipe buffers. Allocates whole 4096-byte pages.

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "riscv.h"
#include "defs.h"

void freerange(void *pa_start, void *pa_end);

extern char end[]; // first address after kernel.
                   // defined by kernel.ld.

struct run {
  struct run *next;
};

struct {
  struct spinlock lock;
  struct run *freelist;
} kmem;

void
kinit()
{
  initlock(&kmem.lock, "kmem");
  freerange(end, (void*)PHYSTOP);
}

void
freerange(void *pa_start, void *pa_end)
{
  char *p;
  p = (char*)PGROUNDUP((uint64)pa_start);
  for(; p + PGSIZE <= (char*)pa_end; p += PGSIZE)
    kfree(p);
}

// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(void *pa)
{
  struct run *r;

  if(((uint64)pa % PGSIZE) != 0 || (char*)pa < end || (uint64)pa >= PHYSTOP)
    panic("kfree");

  // Fill with junk to catch dangling refs.
  memset(pa, 1, PGSIZE);

  r = (struct run*)pa;

  acquire(&kmem.lock);
  r->next = kmem.freelist;
  kmem.freelist = r;
  release(&kmem.lock);
}

// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
void *
kalloc(void)
{
  struct run *r;

  acquire(&kmem.lock);
  r = kmem.freelist;
  if(r)
    kmem.freelist = r->next;
  release(&kmem.lock);

  if(r)
    memset((char*)r, 5, PGSIZE); // fill with junk
  return (void*)r;
}

```

函数main调用kinit来初始化分配器(kernel/kalloc.c:27)。kinit初始化自由列表，以保持内核结束和PHYSTOP之间的每一页。 xv6应该通过解析硬件提供的配置信息来确定有多少物理内存可用。kinit调用freerange通过每页调用kfree来增加内存到空闲列表中。一个PTE只能引用一个在4096字节边界上对齐的物理地址（是4096的倍数），所以freerange使用PGROUNDUP来确保它只释放对齐的物理地址。分配器开始时没有内存，这些对kfree的调用给了它一些管理内存的机会。

分配器有时把地址当作整数来处理，以便对其进行运算（如遍历自由区的所有页），有时把地址当作指针来读写内存（如操作存储在每页中的运行结构）；这种对地址的双重使用是分配器代码中充满C类型投的主要原因。另一个原因是，释放和分配本质上改变了内存的类型。

函数kfree (kernel/kalloc.c:47)首先将被释放的内存中的每一个字节都设置为1。这将导致在释放内存后使用内存的代码(使用 "dangling references")读取垃圾而不是旧的有效内容；希望这将导致这类代码更快地崩溃。

然后，kfree将页面预先添加到自由列表中：它将pa转换为指向struct run的指针，在r->next中记录自由列表的旧起点，并将自由列表设为r。kalloc删除并返回自由列表中的第一个元素。

## 3.6 Process address space

每个进程都有一个独立的页表，当xv6在进程间切换时，也会改变页表。如图2.3所示，一个进程的用户内存从虚拟地址0开始，可以增长到MAXVA(kernel/riscv.h:348)，原则上允许一个进程寻址256GB的内存。

当一个进程要求xv6提供更多的用户内存时，xv6首先使用kalloc来分配物理页。然后它将指向新物理页的PTE添加到进程的页表中。Xv6在这些PTE中设置PTE_W、PTE_X、PTE_R、PTE_U和PTE_V标志。大多数进程不会使用整个用户地址空间，xv6在未使用的PTE中让PTE_V保持清除。

我们在这里看到了几个很好的页表使用实例。首先，不同进程的页表将用户地址转化为物理内存的不同页，因此每个进程都有私有的用户内存。第二，每个进程都认为自己的内存具有从零开始的连续的虚拟地址，而进程的物理内存可以是非连续的。第三，内核在用户地址空间的顶端映射出一个带有蹦床代码的页面，因此，在所有的地址空间中，都会出现一个物理内存的页面。

![](../img/Figure3.4.png)

图3.4更详细地显示了xv6中执行进程的用户内存布局。栈是一个单页，图中显示的是由exec创建的初始内容。包含命令行参数的字符串，以及指向它们的指针数组，位于堆栈的最顶端。在其下面是允许程序从main开始的值，就像函数main(argc, argv)刚刚被调用一样。

为了检测用户堆栈溢出分配的堆栈内存，xv6会在堆栈的正下方放置一个无效的保护页。如果用户堆栈溢出，而进程试图使用堆栈下面的地址，硬件会因为映射无效而产生一个页错误异常。现实世界中的操作系统可能会在用户堆栈溢出时自动为其分配更多的内存。

## 3.7 Code: sbrk

Sbrk是一个进程收缩或增长内存的系统调用，系统调用由函数growproc(kernel/proc.c:239)实现。该系统调用由函数growproc(kernel/proc.c:239)实现，growproc调用uvmalloc或uvmdealloc，这取决于n是正数还是负数。 uvmdealloc 调用 uvmunmap (kernel/vm.c:174)，它使用 walk 来查找 PTEs，使用 kfree 来释放它们所引用的物理内存。

proc.c

```cpp
// Grow or shrink user memory by n bytes.
// Return 0 on success, -1 on failure.
int
growproc(int n)
{
  uint sz;
  struct proc *p = myproc();

  sz = p->sz;
  if(n > 0){
    if((sz = uvmalloc(p->pagetable, sz, sz + n)) == 0) {
      return -1;
    }
  } else if(n < 0){
    sz = uvmdealloc(p->pagetable, sz, sz + n);
  }
  p->sz = sz;
  return 0;
}
```

xv6使用进程的页表不仅是为了告诉硬件如何映射用户虚拟地址，也是作为分配给该进程哪些物理内存页的唯一记录。这就是为什么释放用户内存（在uvmunmap中）需要检查用户页表的原因。

## 3.8 Code: exec

Exec是创建地址空间用户部分的系统调用。它从文件系统中存储的文件初始化地址空间的用户部分。Exec (kernel/exec.c:13)使用namei (kernel/exec.c:26)打开命名的二进制路径，这在第8章中有解释。然后，它读取ELF头。Xv6应用程序用广泛使用的ELF格式来描述，定义在(kernel/elf.h)。一个ELF二进制文件包括一个ELF头，elfhdr结构(kernel/elf.h:6)，后面是一个程序节头序列，proghdr结构(kernel/elf.h:25)。每一个proghdr描述了一个必须加载到内存中的程序部分；xv6程序只有一个程序部分头，但其他系统可能有单独的指令和数据部分。

exec.c

```cpp
// ...
static int loadseg(pde_t *pgdir, uint64 addr, struct inode *ip, uint offset, uint sz);

int
exec(char *path, char **argv)
{
  char *s, *last;
  int i, off;
  uint64 argc, sz = 0, sp, ustack[MAXARG+1], stackbase;
  struct elfhdr elf;
  struct inode *ip;
  struct proghdr ph;
  pagetable_t pagetable = 0, oldpagetable;
  struct proc *p = myproc();

  begin_op();

  if((ip = namei(path)) == 0){
    end_op();
    return -1;
  }
  ilock(ip);

  // Check ELF header
  if(readi(ip, 0, (uint64)&elf, 0, sizeof(elf)) != sizeof(elf))
    goto bad;
  if(elf.magic != ELF_MAGIC)
    goto bad;

  if((pagetable = proc_pagetable(p)) == 0)
    goto bad;

  // Load program into memory.
  for(i=0, off=elf.phoff; i<elf.phnum; i++, off+=sizeof(ph)){
    if(readi(ip, 0, (uint64)&ph, off, sizeof(ph)) != sizeof(ph))
      goto bad;
    if(ph.type != ELF_PROG_LOAD)
      continue;
    if(ph.memsz < ph.filesz)
      goto bad;
    if(ph.vaddr + ph.memsz < ph.vaddr)
      goto bad;
    uint64 sz1;
    if((sz1 = uvmalloc(pagetable, sz, ph.vaddr + ph.memsz)) == 0)
      goto bad;
    sz = sz1;
    if(ph.vaddr % PGSIZE != 0)
      goto bad;
    if(loadseg(pagetable, ph.vaddr, ip, ph.off, ph.filesz) < 0)
      goto bad;
  }
  iunlockput(ip);
  end_op();
  ip = 0;

  p = myproc();
  uint64 oldsz = p->sz;

  // Allocate two pages at the next page boundary.
  // Use the second as the user stack.
  sz = PGROUNDUP(sz);
  uint64 sz1;
  if((sz1 = uvmalloc(pagetable, sz, sz + 2*PGSIZE)) == 0)
    goto bad;
  sz = sz1;
  uvmclear(pagetable, sz-2*PGSIZE);
  sp = sz;
  stackbase = sp - PGSIZE;

  // Push argument strings, prepare rest of stack in ustack.
  for(argc = 0; argv[argc]; argc++) {
    if(argc >= MAXARG)
      goto bad;
    sp -= strlen(argv[argc]) + 1;
    sp -= sp % 16; // riscv sp must be 16-byte aligned
    if(sp < stackbase)
      goto bad;
    if(copyout(pagetable, sp, argv[argc], strlen(argv[argc]) + 1) < 0)
      goto bad;
    ustack[argc] = sp;
  }
  ustack[argc] = 0;

  // push the array of argv[] pointers.
  sp -= (argc+1) * sizeof(uint64);
  sp -= sp % 16;
  if(sp < stackbase)
    goto bad;
  if(copyout(pagetable, sp, (char *)ustack, (argc+1)*sizeof(uint64)) < 0)
    goto bad;

  // arguments to user main(argc, argv)
  // argc is returned via the system call return
  // value, which goes in a0.
  p->trapframe->a1 = sp;

  // Save program name for debugging.
  for(last=s=path; *s; s++)
    if(*s == '/')
      last = s+1;
  safestrcpy(p->name, last, sizeof(p->name));
    
  // Commit to the user image.
  oldpagetable = p->pagetable;
  p->pagetable = pagetable;
  p->sz = sz;
  p->trapframe->epc = elf.entry;  // initial program counter = main
  p->trapframe->sp = sp; // initial stack pointer
  proc_freepagetable(oldpagetable, oldsz);

  return argc; // this ends up in a0, the first argument to main(argc, argv)

 bad:
  if(pagetable)
    proc_freepagetable(pagetable, sz);
  if(ip){
    iunlockput(ip);
    end_op();
  }
  return -1;
}
```

第一步是快速检查文件是否包含一个ELF二进制文件。一个ELF二进制文件以四个字节的 "魔数 "0x7F、"E"、"L"、"F "或ELF_MAGIC(kernel/elf.h:3)开头。如果ELF头有正确的魔数，exec就会认为二进制格式良好。

Exec用proc_pagetable(kernel/exec.c:38)分配一个没有用户映射的新页表，用uvmalloc(kernel/exec.c:52)为每个ELF段分配内存，用loadseg(kernel/exec.c:10)把每个段加载到内存中。loadseg用walkaddr找到分配的内存的物理地址，在这个地址上写入ELF段的每一页，用readi从文件中读取。

用exec创建的第一个用户程序/init的程序段头是这样的。

```sh
# objdump -p _init

user/_init: file format elf64-littleriscv
Program Header:
LOAD off 0x00000000000000b0 vaddr 0x0000000000000000 paddr 0x0000000000000000 align 2**3 filesz 0x0000000000000840 memsz 0x0000000000000858 flags rwx
STACK off 0x0000000000000000 vaddr 0x0000000000000000 paddr 0x0000000000000000 align 2**4 filesz 0x0000000000000000 memsz 0x0000000000000000 flags rw
```

程序节头的filesz可能小于memsz，说明它们之间的空隙应该用零来填充（对于C语言全局变量），而不是从文件中读取。

对于/init来说，filesz是2112字节，memsz是2136字节，因此uvmalloc分配了足够的物理内存来容纳2136字节，但只从文件/init中读取了2112字节。

现在exec分配并初始化用户栈。它只分配一个栈页。Exec每次将参数字符串复制到堆栈的顶部，在ustack中记录它们的指针。

它将一个空指针放在传递给main的argv列表的最后。ustack中的前三个条目是假返回程序计数器、argc和argv指针。

exec在堆栈页的下方放置了一个不可访问页，这样程序如果试图使用多个页面，就会出现故障。这个不可访问的页面也允许exec处理过大的参数；在这种情况下，exec用来复制参数到堆栈的copyout(kernel/vm.c:355)函数会注意到目标页不可访问，并返回-1。

在准备新的内存映像的过程中，如果exec检测到一个错误，比如一个无效的程序段，它就会跳转到标签bad，释放新的映像，并返回-1。exec必须等待释放旧映像，直到它确定系统调用会成功：如果旧映像消失了，系统调用就不能向它返回-1。exec中唯一的错误情况发生在创建映像的过程中。一旦镜像完成，exec就可以提交到新的页表(kernel/exec.c:113)并释放旧的页表(kernel/exec.c:117)

Exec将ELF文件中的字节按ELF文件指定的地址加载到内存中。用户或进程可以将任何他们想要的地址放入ELF文件中。因此，Exec是有风险的，因为ELF文件中的地址可能会意外地或故意地指向内核。对于不小心的内核来说，后果可能从崩溃到恶意颠覆内核的隔离机制(即安全漏洞)不等。xv6执行了一些检查来避免这些风险。例如if(ph.vaddr + ph.memsz < ph.vaddr)检查和是否溢出64位整数。危险的是，用户可以用指向用户选择的地址的 ph.vaddr 和足够大的 ph.memsz 来构造一个 ELF 二进制，使总和溢出到 0x1000，这看起来像是一个有效值。在旧版本的xv6中，用户地址空间也包含了内核（但在用户模式下不可读/写），用户可以选择一个对应内核内存的地址，从而将ELF二进制中的数据复制到内核中。在RISC-V版本的xv6中，这是不可能发生的，因为内核有自己独立的页表；loadseg加载到进程的页表中，而不是内核的页表中。

内核开发者很容易遗漏一个关键的检查，现实世界的内核有很长的历史遗漏检查，其缺失可以被用户程序利用来获取内核权限。很可能xv6并没有对提供给内核的用户级数据进行完整的验证，恶意的用户程序可能会利用这一点来规避xv6的隔离。

## 3.9 Real world

像大多数操作系统一样，xv6使用分页硬件进行内存保护和映射。大多数操作系统对分页的使用要比xv6复杂得多，它将分页和分页错误异常结合起来，我们将在第4章中讨论。

Xv6通过内核使用虚拟地址和物理地址之间的直接映射来简化，并假设在地址0x8000000处有物理RAM，即内核期望加载的地方。这在QEMU上是可行的，但在真实的硬件上，它被证明是一个糟糕的想法；真实的硬件将RAM和设备放置在不可预测的物理地址上，所以（例如）在0x8000000处可能没有RAM，xv6期望能够在那里存储内核。更严重的内核设计利用页表将任意的硬件物理内存布局变成可预测的内核虚拟地址布局。

RISC-V支持物理地址级别的保护，但xv6没有使用这个功能。

在内存很大的机器上，使用RISC-V对 "超级页 "的支持可能是有意义的。当物理内存很小的时候，小页是有意义的，可以细粒度地分配和分页到磁盘。例如，如果一个程序只使用8千字节的内存，给它整整4兆字节的超级物理内存页是浪费的。更大的页面在有大量内存的机器上是有意义的，并且可以减少页表操作的开销。

xv6内核缺乏一个类似malloc的分配器，可以为小对象提供内存，这使得内核无法使用需要动态分配的复杂数据结构。

内存分配是一个常年的热门话题，基本问题是有效利用有限的内存和为未来未知的请求做准备[7]。如今人们更关心的是速度而不是空间效率。此外，一个更复杂的内核可能会分配许多不同大小的小块，而不是（在xv6中）只分配4096字节的块；一个真正的内核分配器需要处理小块分配以及大块分配。

## 3.10 Exercises

1-解析RISC-V的设备树，找出计算机的物理内存量。

2- 编写一个用户程序，通过调用sbrk(1)使其地址空间增长一个字节。运行该程序，调查调用sbrk之前和调用sbrk之后的程序页表。内核分配了多少空间？新的PTE是多少？
内存包含哪些内容？

3- 修改xv6，使内核使用超级页。

4- 修改xv6，使用户程序取消引用一个空指针时，会收到一个异常。也就是修改xv6，使虚拟地址0不被映射给用户程序。

5- Unix实现的exec传统上包含了对shell脚本的特殊处理。如果要执行的文件以文本#！开头，那么第一行就会被认为是要运行解释文件的程序。例如，如果调用exec运行myprog arg1，而myprog的第一行是#！/interp，那么exec运行/interp，命令行为/interp myprog arg1。在xv6中实现对这个约定的支持。

6- 实现内核的地址空间随机化。

