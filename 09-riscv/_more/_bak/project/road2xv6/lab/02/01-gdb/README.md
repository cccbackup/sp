# gdb

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp/11-os/xv6-riscv-win (master)
$ make qemu-gdb
...
nmeta 46 (boot, super, log blocks 30 inode blocks 13, bitmap blocks 1) blocks 954 total 1000
balloc: first 593 blocks have been allocated
balloc: write bitmap block at sector 45
*** Now run 'gdb' in another window.
qemu-system-riscv64 -machine virt -bios none -kernel kernel/kernel -m 256M -smp 3 -nographic -drive file=fs.img,if=none,format=raw,id=x0 -device virtio-blk-device,drive=x0,bus=virtio-mmio-bus.0 -S -gdb tcp::27609
```

然後在另一個視窗的同一個資料夾中，使用 gdb 除錯：

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp/11-os/xv6-riscv-win (master)
$ riscv64-unknown-elf-gdb
GNU gdb (SiFive GDB 8.3.0-2020.04.1) 8.3
....
(gdb) target remote localhost:27609
A program is being debugged already.  Kill it? (y or n) n
Program not killed.
(gdb) break *0x3ffffff10e
Note: breakpoint 1 also set at pc 0x3ffffff10e.
Breakpoint 2 at 0x3ffffff10e
(gdb) continue
Continuing.

Thread 1 hit Breakpoint 1, 0x0000003ffffff10e in ?? ()
(gdb) continue

... 這樣會一次一次慢慢執行

... 然後我們刪除中斷點，一次執行

(gdb) delete breakpoints
Delete all breakpoints? (y or n) y
(gdb) c
Continuing.
```

接著在 qemu 視窗中會看到 kernel 啟動完畢了！

```
xv6 kernel is booting

hart 1 starting
hart 2 starting
init: starting sh
$ ls
.              1 1 1024
..             1 1 1024
README         2 2 2102
cat            2 3 23944
echo           2 4 22776
forktest       2 5 13112
grep           2 6 27256
init           2 7 23904
kill           2 8 22744
ln             2 9 22728
ls             2 10 26152
mkdir          2 11 22864
rm             2 12 22848
sh             2 13 41800
stressfs       2 14 23776
usertests      2 15 151184
grind          2 16 38008
wc             2 17 25040
zombie         2 18 22272
console        3 19 0
```

