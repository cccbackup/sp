# Chapter 5 -- Interrupts and device drivers

A driver is the code in an operating system that manages a particular device: it configures the device hardware, tells the device to perform operations, handles the resulting interrupts, and interacts with processes that may be waiting for I/O from the device. Driver code can be tricky because a driver executes concurrently with the device that it manages. In addition, the driver must understand the device’s hardware interface, which can be complex and poorly documented.

驱动程序是操作系统中管理特定设备的代码：它配置设备硬件，告诉设备执行操作，处理由此产生的中断，并与可能等待设备I/O的进程进行交互。驱动程序代码可能很棘手，因为驱动程序与它所管理的设备同时执行。此外，驱动程序必须理解设备的硬件接口，而硬件接口可能是复杂的，而且文档化程度不高。

Devices that need attention from the operating system can usually be configured to generate interrupts, which are one type of trap. The kernel trap handling code recognizes when a device has raised an interrupt and calls the driver’s interrupt handler; in xv6, this dispatch happens in devintr (kernel/trap.c:177).

需要操作系统关注的设备通常可以被配置为产生中断，这是陷阱的一种类型。内核陷阱处理代码会在设备引发中断时识别并调用驱动的中断处理程序；在xv6中，这个调度发生在devintr(kernel/trap.c:177)。

Many device drivers execute code in two contexts: a top half that runs in a process’s kernel thread, and a bottom half that executes at interrupt time. The top half is called via system calls such as read and write that want the device to perform I/O. This code may ask the hardware to start an operation (e.g., ask the disk to read a block); then the code waits for the operation to complete. Eventually the device completes the operation and raises an interrupt. The driver’s interrupt handler, acting as the bottom half, figures out what operation has completed, wakes up a waiting process if appropriate, and tells the hardware to start work on any waiting next operation.

许多设备驱动程序在两个上下文中执行代码：上半部分在进程的内核线程中运行，下半部分在中断时执行。上半部分是通过系统调用（如read和write）来调用，希望设备执行I/O。这段代码可能会要求硬件开始一个操作（比如要求磁盘读取一个块）；然后代码会等待操作完成。最终设备完成操作，并引发一个中断。驱动程序的中断处理程序作为下半部分，计算出什么操作已经完成，如果合适的话，唤醒一个等待的进程，并告诉硬件开始对任何等待的下一个操作进行工作。

## 5.1 Code: Console input

The console driver (console.c) is a simple illustration of driver structure. The console driver accepts characters typed by a human, via the UART serial-port hardware attached to the RISC-V. The console driver accumulates a line of input at a time, processing special input characters such as backspace and control-u. User processes, such as the shell, use the read system call to fetch lines of input from the console. When you type input to xv6 in QEMU, your keystrokes are delivered to xv6 by way of QEMU’s simulated UART hardware.

控制台驱动程序(console.c)是驱动程序结构的一个简单说明。控制台驱动程序通过连接到RISC-V上的UART串行端口硬件，接受人输入的字符。控制台驱动程序每次积累一行输入，处理特殊的输入字符，如退格键和control-u。用户进程，如shell，使用read系统调用从控制台获取输入行。当你在QEMU中向xv6键入输入时，你的击键会通过QEMU的模拟UART硬件传递给xv6。

The UART hardware that the driver talks to is a 16550 chip [11] emulated by QEMU. On a real computer, a 16550 would manage an RS232 serial link connecting to a terminal or other computer.

When running QEMU, it’s connected to your keyboard and display.

The UART hardware appears to software as a set of memory-mapped control registers. That is, there are some physical addresses that RISC-V hardware connects to the UART device, so that loads and stores interact with the device hardware rather than RAM. The memory-mapped addresses for the UART start at 0x10000000, or UART0 (kernel/memlayout.h:21). There are a handful of UART control registers, each the width of a byte. Their offsets from UART0 are defined in (kernel/uart.c:22). For example, the LSR register contain bits that indicate whether input characters are waiting to be read by the software. These characters (if any) are available for reading from the RHR register. Each time one is read, the UART hardware deletes it from an internal FIFO of waiting characters, and clears the “ready” bit in LSR when the FIFO is empty. The UART transmit hardware is largely independent of the receive hardware; if software writes a byte to the THR, the UART transmit that byte.

驱动程序与之对话的UART硬件是由QEMU仿真的16550芯片[11]。在真实的计算机上，16550将管理一个连接到终端或其他计算机的RS232串行链接。

当运行QEMU时，它连接到你的键盘和显示器上。

UART硬件在软件看来是一组内存映射的控制寄存器。也就是说，有一些RISC-V硬件连接到UART设备的物理地址，因此加载和存储与设备硬件而不是RAM交互。UART的内存映射地址从0x10000000开始，即UART0（kernel/memlayout.h:21）。有一些UART控制寄存器，每个寄存器的宽度是一个字节。它们与UART0的偏移量定义在(kernel/uart.c:22)。例如，LSR寄存器中包含了表示输入字符是否在等待软件读取的位。这些字符（如果有的话）可以从RHR寄存器中读取。每次读取一个字符，UART硬件就会将其从内部等待字符的FIFO中删除，并在FIFO为空时清除LSR中的 "ready "位。UART发送硬件在很大程度上是独立于接收硬件的，如果软件向THR写入一个字节，UART就会发送该字节。

Xv6’s main calls consoleinit (kernel/console.c:184) to initialize the UART hardware. This code configures the UART to generate a receive interrupt when the UART receives each byte of input, and a transmit complete interrupt each time the UART finishes sending a byte of output (kernel/uart.c:53).

The xv6 shell reads from the console by way of a file descriptor opened by init.c (user/init.c:19).

Calls to the read system call make their way through the kernel to consoleread (kernel/console.c:82). consoleread waits for input to arrive (via interrupts) and be buffered in cons.buf, copies the input to user space, and (after a whole line has arrived) returns to the user process. If the user hasn’t typed a full line yet, any reading processes will wait in the sleep call (kernel/console.c:98) (Chapter 7 explains the details of sleep).

Xv6的main调用consoleinit（kernel/console.c:184）来初始化UART硬件。这段代码配置了UART，当UART接收到每个字节的输入时，产生一个接收中断，当UART每次完成发送一个字节的输出时，产生一个发送完成中断(kernel/uart.c:53)。

xv6的shell通过init.c打开的文件描述符（user/init.c:19）从控制台读取。

对读取系统调用的调用通过内核进入consoleread (kernel/console.c:82)。consoleread等待输入的到来(通过中断)并在cons.buf中缓冲，将输入复制到用户空间，然后(在一整行到达后)返回到用户进程。如果用户还没有输入一整行，任何读取进程都会在sleep调用中等待（kernel/console.c:98）（第7章解释了sleep的细节）。

When the user types a character, the UART hardware asks the RISC-V to raise an interrupt, which activates xv6’s trap handler. The trap handler calls devintr (kernel/trap.c:177), which looks at the RISC-V scause register to discover that the interrupt is from an external device. Then it asks a hardware unit called the PLIC [1] to tell it which device interrupted (kernel/trap.c:186). If it was the UART, devintr calls uartintr.

当用户键入一个字符时，UART硬件要求RISC-V引发一个中断，从而激活xv6的陷阱处理程序。陷阱处理程序调用devintr(kernel/trap.c:177)，它查看RISC-V的scause寄存器，发现中断来自外部设备。然后它要求一个叫做PLIC[1]的硬件单元告诉它是哪个设备中断了(kernel/trap.c:186)。如果是UART，devintr调用uartintr。

uartintr (kernel/uart.c:180) reads any waiting input characters from the UART hardware and hands them to consoleintr (kernel/console.c:138); it doesn’t wait for characters, since future input will raise a new interrupt. The job of consoleintr is to accumulate input characters in cons.buf until a whole line arrives. consoleintr treats backspace and a few other characters specially. When a newline arrives, consoleintr wakes up a waiting consoleread (if there is one).

Once woken, consoleread will observe a full line in cons.buf, copy it to user space, and return (via the system call machinery) to user space.

uartintr (kernel/uart.c:180) 从UART硬件中读取任何等待的输入字符，并将它们交给consoleintr (kernel/console.c:138)；它不会等待字符，因为未来的输入会引发一个新的中断。consoleintr的工作是在cons.buf中积累输入字符，直到有整行字符到来。 consoleintr会特别处理退格键和其他一些字符。当一个新行到达时，consoleintr会唤醒一个等待的consoleread（如果有的话）。

一旦被唤醒，consoleread将观察cons.buf中的整行，将其复制到用户空间，然后返回（通过系统调用机制）到用户空间。

## 5.2 Code: Console output

A write system call on a file descriptor connected to the console eventually arrives at uartputc (kernel/uart.c:87). The device driver maintains an output buffer (uart_tx_buf) so that writing processes do not have to wait for the UART to finish sending; instead, uartputc appends each character to the buffer, calls uartstart to start the device transmitting (if it isn’t already), and returns. The only situation in which uartputc waits is if the buffer is already full.

Each time the UART finishes sending a byte, it generates an interrupt. uartintr calls uartstart, which checks that the device really has finished sending, and hands the device the next buffered output character. Thus if a process writes multiple bytes to the console, typically the first byte will be sent by uartputc’s call to uartstart, and the remaining buffered bytes will be sent by uartstart calls from uartintr as transmit complete interrupts arrive.

A general pattern to note is the decoupling of device activity from process activity via buffering and interrupts. The console driver can process input even when no process is waiting to read it; a subsequent read will see the input. Similarly, processes can send output without having to wait for the device. This decoupling can increase performance by allowing processes to execute concurrently with device I/O, and is particularly important when the device is slow (as with the UART) or needs immediate attention (as with echoing typed characters). This idea is sometimes called I/O concurrency.

对连接到控制台的文件描述符的写系统调用最终会到达uartputc(kernel/uart.c:87)。设备驱动维护了一个输出缓冲区(uart_tx_buf)，这样写过程就不必等待UART完成发送；相反，uartputc将每个字符追加到缓冲区，调用uartstart来启动设备发送(如果还没有的话)，然后返回。uartputc等待的唯一情况是缓冲区已经满了。

uartintr调用uartstart，检查设备是否真的已经完成发送，并将下一个缓冲区的输出字符交给设备。因此，如果一个进程向控制台写入多个字节，通常第一个字节将由uartputc对uartstart的调用发送，剩余的缓冲字节将由uartintr的uartstart调用发送，因为发送完成中断到来。

一个需要注意的一般模式是通过缓冲和中断将设备活动与进程活动解耦。控制台驱动程序可以处理输入，即使没有进程等待读取它；随后的读取将看到输入。同样，进程可以发送输出，而不必等待设备。这种解耦可以通过允许进程与设备I/O并发执行来提高性能，当设备速度很慢（如UART）或需要立即关注（如呼应打字字符）时，这种解耦尤为重要。这种想法有时被称为I/O并发。


## 5.3 Concurrency in drivers

You may have noticed calls to acquire in consoleread and in consoleintr. These calls acquire a lock, which protects the console driver’s data structures from concurrent access. There are three concurrency dangers here: two processes on different CPUs might call consoleread at the same time; the hardware might ask a CPU to deliver a console (really UART) interrupt while that CPU is already executing inside consoleread; and the hardware might deliver a console interrupt on a different CPU while consoleread is executing. Chapter 6 explores how locks help in these scenarios.

Another way in which concurrency requires care in drivers is that one process may be waiting for input from a device, but the interrupt signaling arrival of the input may arrive when a different process (or no process at all) is running. Thus interrupt handlers are not allowed to think about the process or code that they have interrupted. For example, an interrupt handler cannot safely call copyout with the current process’s page table. Interrupt handlers typically do relatively little work (e.g., just copy the input data to a buffer), and wake up top-half code to do the rest.

你可能已经注意到在consoleread和consoleintr中调用获取。这些调用会获取一个锁，保护控制台驱动的数据结构不被并发访问。这里有三个并发危险：不同CPU上的两个进程可能会调用consoleread 同时；硬件可能会要求一个CPU传递一个控制台（实际上是UART）中断，而这个CPU已经在consoleread里面执行了；硬件可能会在consoleread执行的时候在另一个CPU上传递一个控制台中断。第6章将探讨锁如何在这些情况下提供帮助。

并发需要驱动程序小心的另一种方式是，一个进程可能正在等待来自设备的输入，但当一个不同的进程（或者根本没有进程）正在运行时，输入的中断信号可能会到达。因此，中断处理程序不允许思考被中断的进程或代码。例如，中断处理程序不能用当前进程的页表安全地调用copyout。中断处理程序通常只做相对较少的工作（例如，只是将输入数据复制到缓冲区），并唤醒上半部分代码来做剩下的工作。

## 5.4 Timer interrupts

Xv6 uses timer interrupts to maintain its clock and to enable it to switch among compute-bound processes; the yield calls in usertrap and kerneltrap cause this switching. Timer interrupts come from clock hardware attached to each RISC-V CPU. Xv6 programs this clock hardware
to interrupt each CPU periodically.

RISC-V requires that timer interrupts be taken in machine mode, not supervisor mode. RISCV machine mode executes without paging, and with a separate set of control registers, so it’s not practical to run ordinary xv6 kernel code in machine mode. As a result, xv6 handles timer interrupts completely separately from the trap mechanism laid out above.

Code executed in machine mode in start.c, before main, sets up to receive timer interrupts (kernel/start.c:57). Part of the job is to program the CLINT hardware (core-local interruptor) to generate an interrupt after a certain delay. Another part is to set up a scratch area, analogous to the trapframe, to help the timer interrupt handler save registers and the address of the CLINT registers.

Finally, start sets mtvec to timervec and enables timer interrupts.

Xv6使用定时器中断来维持它的时钟，并使它能够在计算绑定的进程之间进行切换；usertrap和kerneltrap中的yield调用导致这种切换。定时器中断来自连接到每个RISC-V CPU的时钟硬件。Xv6对这个时钟硬件进行编程
来周期性地中断每个CPU。

RISC-V要求在机器模式下采取定时器中断，而不是主管模式。RISCV机器模式执行时不需要分页，而且有一套单独的控制寄存器，所以在机器模式下运行普通的xv6内核代码是不现实的。因此，xv6对定时器中断的处理与上面布置的陷阱机制完全分开。

在main之前的start.c中，在机器模式下执行的代码，设置为接收定时器中断（kernel/start.c:57）。一部分工作是对CLINT硬件（核心本地中断器）进行编程，使其在一定的延迟后产生一个中断。另一部分是设置一个类似于trapframe的scratch区域，帮助定时器中断处理程序保存寄存器和CLINT寄存器的地址。

最后，start将mtvec设置为timervec，启用定时器中断。

A timer interrupt can occur at any point when user or kernel code is executing; there’s no way for the kernel to disable timer interrupts during critical operations. Thus the timer interrupt handler must do its job in a way guaranteed not to disturb interrupted kernel code. The basic strategy is for the handler to ask the RISC-V to raise a “software interrupt” and immediately return. The RISC-V delivers software interrupts to the kernel with the ordinary trap mechanism, and allows the kernel to disable them. The code to handle the software interrupt generated by a timer interrupt can be seen in devintr (kernel/trap.c:204).

The machine-mode timer interrupt vector is timervec (kernel/kernelvec.S:93). It saves a few registers in the scratch area prepared by start, tells the CLINT when to generate the next timer interrupt, asks the RISC-V to raise a software interrupt, restores registers, and returns. There’s no C code in the timer interrupt handler.

定时器中断可能发生在用户或内核代码执行的任何时候；内核没有办法在关键操作期间禁用定时器中断。因此，定时器中断处理程序必须以保证不干扰被中断的内核代码的方式进行工作。基本的策略是处理程序要求RISC-V提出一个 "软件中断 "并立即返回。RISC-V用普通的陷阱机制将软件中断传递给内核，并允许内核将其禁用。处理定时器中断产生的软件中断的代码可以在devintr（kernel/trap.c:204）中看到。

机器模式的定时器中断向量是timervec(kernel/kernelvec.S:93)。它在start准备的scratch区域保存几个寄存器，告诉clint什么时候产生下一个定时器中断，要求RISC-V提出一个软件中断，恢复寄存器，然后返回。在定时器中断处理程序中没有C代码。

## 5.5 Real world

Xv6 allows device and timer interrupts while executing in the kernel, as well as when executing user programs. Timer interrupts force a thread switch (a call to yield) from the timer interrupt handler, even when executing in the kernel. The ability to time-slice the CPU fairly among kernel threads is useful if kernel threads sometimes spend a lot of time computing, without returning to user space. However, the need for kernel code to be mindful that it might be suspended (due to a timer interrupt) and later resume on a different CPU is the source of some complexity in xv6.

The kernel could be made somewhat simpler if device and timer interrupts only occurred while executing user code.

Supporting all the devices on a typical computer in its full glory is much work, because there are many devices, the devices have many features, and the protocol between device and driver can be complex and poorly documented. In many operating systems, the drivers account for more code than the core kernel.

Xv6允许在内核中执行以及在执行用户程序时使用设备和定时器中断。定时器中断可以强制从定时器中断处理程序进行线程切换（调用让步），即使是在内核中执行。如果内核线程有时会花费大量的时间进行计算，而不返回用户空间，那么在内核线程之间公平地对CPU进行时间划分的能力是很有用的。然而，内核代码需要注意它可能会被暂停（由于定时器中断），然后在不同的CPU上恢复，这是xv6中一些复杂的根源。

如果设备和定时器中断只发生在执行用户代码的时候，内核可以变得更简单一些。

在一台典型的计算机上支持所有设备的全部功能是一件很辛苦的事情，因为设备很多，设备有很多功能，设备和驱动程序之间的协议可能很复杂，而且文档也不完善。在许多操作系统中，驱动程序所占的代码比核心内核还要多。

The UART driver retrieves data a byte at a time by reading the UART control registers; this pattern is called programmed I/O, since software is driving the data movement. Programmed I/O is simple, but too slow to be used at high data rates. Devices that need to move lots of data at high speed typically use direct memory access (DMA). DMA device hardware directly writes incoming data to RAM, and reads outgoing data from RAM. Modern disk and network devices use DMA.

A driver for a DMA device would prepare data in RAM, and then use a single write to a control register to tell the device to process the prepared data.

Interrupts make sense when a device needs attention at unpredictable times, and not too often.

UART驱动器通过读取UART控制寄存器，一次检索一个字节的数据；这种模式被称为编程I/O，因为软件在驱动数据移动。程序化I/O简单，但速度太慢，无法在高数据速率下使用。需要高速移动大量数据的设备通常使用直接内存访问（DMA）。DMA设备硬件直接将传入数据写入RAM，并从RAM中读取传出数据。现代磁盘和网络设备都使用DMA。

DMA设备的驱动程序会在RAM中准备数据，然后用对控制寄存器的一次写入告诉设备处理准备好的数据。

当设备在不可预知的时间需要关注时，中断是有意义的，而且不要太频繁。

But interrupts have high CPU overhead. Thus high speed devices, such networks and disk controllers, use tricks that reduce the need for interrupts. One trick is to raise a single interrupt for a whole batch of incoming or outgoing requests. Another trick is for the driver to disable interrupts entirely, and to check the device periodically to see if it needs attention. This technique is called polling. Polling makes sense if the device performs operations very quickly, but it wastes CPU time if the device is mostly idle. Some drivers dynamically switch between polling and interrupts depending on the current device load.

The UART driver copies incoming data first to a buffer in the kernel, and then to user space.

This makes sense at low data rates, but such a double copy can significantly reduce performance for devices that generate or consume data very quickly. Some operating systems are able to directly move data between user-space buffers and device hardware, often with DMA.

但中断对CPU的开销很大。因此，高速设备，如网络和磁盘控制器，使用了减少对中断需求的技巧。其中一个技巧是对整批传入或传出的请求提出一个单一的中断。另一个技巧是让驱动程序完全禁用中断，并定期检查设备是否需要关注。这种技术称为轮询。如果设备执行操作的速度非常快，轮询是有意义的，但如果设备大部分时间处于空闲状态，则会浪费CPU时间。一些驱动程序根据当前设备的负载情况，在轮询和中断之间动态切换。

UART驱动首先将传入的数据复制到内核的缓冲区，然后再复制到用户空间。

这在低数据速率下是有意义的，但对于那些快速生成或消耗数据的设备来说，这样的双重拷贝会显著降低性能。一些操作系统能够直接在用户空间缓冲区和设备硬件之间移动数据，通常使用DMA。

## 5.6 Exercises

1- Modify uart.c to not use interrupts at all. You may need to modify console.c as well.

2- Add a driver for an Ethernet card.

1- 修改uart.c，使其完全不使用中断。你可能还需要修改 console.c。

2- 为以太网卡添加一个驱动程序。