# Chapter 8 -- File system

The purpose of a file system is to organize and store data. File systems typically support sharing of data among users and applications, as well as persistence so that data is still available after a reboot.

The xv6 file system provides Unix-like files, directories, and pathnames (see Chapter 1), and stores its data on a virtio disk for persistence (see Chapter 4). The file system addresses several challenges:

* The file system needs on-disk data structures to represent the tree of named directories and files, to record the identities of the blocks that hold each file’s content, and to record which areas of the disk are free.
* The file system must support crash recovery. That is, if a crash (e.g., power failure) occurs, the file system must still work correctly after a restart. The risk is that a crash might interrupt a sequence of updates and leave inconsistent on-disk data structures (e.g., a block that is both used in a file and marked free).
* Different processes may operate on the file system at the same time, so the file-system code must coordinate to maintain invariants.
* Accessing a disk is orders of magnitude slower than accessing memory, so the file system must maintain an in-memory cache of popular blocks.

The rest of this chapter explains how xv6 addresses these challenges.

文件系统的目的是组织和存储数据。文件系统通常支持用户和应用程序之间的数据共享，以及持久性，以便数据在重启后仍然可用。

xv6文件系统提供了类似Unix的文件、目录和路径名（见第1章），并将其数据存储在virtio磁盘上以实现持久性（见第4章）。该文件系统解决了几个挑战。

* 文件系统需要磁盘上的数据结构来表示命名的目录和文件树，记录保存每个文件内容的块的身份，并记录磁盘上哪些区域是空闲的。
* 文件系统必须支持崩溃恢复。也就是说，如果发生崩溃（如断电），文件系统必须在重新启动后仍能正常工作。风险是崩溃可能会中断更新序列，并在磁盘上留下不一致的数据结构（例如，一个块既在文件中使用，又被标记为空闲）。
* 不同的进程可能同时在文件系统上操作，因此文件系统代码必须协调以保持不变性。
* 访问磁盘的速度比访问内存的速度慢几个数量级，所以文件系统必须维护一个常用块的内存缓存。

本章其余部分将解释xv6如何应对这些挑战。

## 8.1 Overview

![](../img/Figure8.1.png)

The xv6 file system implementation is organized in seven layers, shown in Figure 8.1. The disk layer reads and writes blocks on an virtio hard drive. The buffer cache layer caches disk blocks and synchronizes access to them, making sure that only one kernel process at a time can modify the data stored in any particular block. The logging layer allows higher layers to wrap updates to several blocks in a transaction, and ensures that the blocks are updated atomically in the face of crashes (i.e., all of them are updated or none). The inode layer provides individual files, each represented as an inode with a unique i-number and some blocks holding the file’s data. The directory layer implements each directory as a special kind of inode whose content is a sequence of directory entries, each of which contains a file’s name and i-number. The pathname layer provides hierarchical path names like /usr/rtm/xv6/fs.c, and resolves them with recursive lookup. The file descriptor layer abstracts many Unix resources (e.g., pipes, devices, files, etc.) using the file system interface, simplifying the lives of application programmers.

xv6文件系统的实现分为七层，如图8.1所示。磁盘层在virtio硬盘上读写块。缓冲区缓存层缓存磁盘块，并同步访问它们，确保一次只有一个内核进程可以修改存储在任何特定块中的数据。日志层允许上层在一个事务中包揽几个块的更新，并确保在面对崩溃时，块的更新是原子性的（即全部更新或不更新）。inode层提供了各个文件，每个文件都表示为一个inode，有一个唯一的i号和一些存放文件数据的块。目录层将每个目录实现为一种特殊的inode，其内容是一个目录项的序列，每个目录项包含一个文件的名称和i-number。路径名层提供了层次化的路径名，如/usr/rtm/xv6/fs.c，并通过递归查找进行解析。文件描述符层使用文件系统接口抽象了许多Unix资源（如管道、设备、文件等），简化了应用程序员的生活。

The file system must have a plan for where it stores inodes and content blocks on the disk. To do so, xv6 divides the disk into several sections, as Figure 8.2 shows. The file system does not use block 0 (it holds the boot sector). Block 1 is called the superblock; it contains metadata about the file system (the file system size in blocks, the number of data blocks, the number of inodes, and the number of blocks in the log). Blocks starting at 2 hold the log. After the log are the inodes, with multiple inodes per block. After those come bitmap blocks tracking which data blocks are in use. The remaining blocks are data blocks; each is either marked free in the bitmap block, or holds content for a file or directory. The superblock is filled in by a separate program, called mkfs, which builds an initial file system.

The rest of this chapter discusses each layer, starting with the buffer cache. Look out for situations where well-chosen abstractions at lower layers ease the design of higher ones.

文件系统必须对它在磁盘上存储inodes和内容块的位置有一个计划。为此，xv6将磁盘分为几个部分，如图8.2所示。文件系统不使用块0（它存放引导扇区）。第1块称为超级块；它包含了文件系统的元数据（以块为单位的文件系统大小、数据块的数量、inodes的数量和日志中的块数）。从2开始的块存放着日志。日志之后是inodes，每个块有多个inodes。在这些之后是位图块，跟踪哪些数据块在使用。其余的块是数据块，每个数据块要么在位图块中标记为空闲，要么持有文件或目录的内容。超级块由一个单独的程序填充，称为mkfs，它建立了一个初始文件系统。

本章的其余部分将讨论每一层，从缓冲区缓存开始。注意在低层的抽象选择得当的情况下，会使更高层的设计变得容易。

## 8.2 Buffer cache layer

The buffer cache has two jobs: (1) synchronize access to disk blocks to ensure that only one copy of a block is in memory and that only one kernel thread at a time uses that copy; (2) cache popular blocks so that they don’t need to be re-read from the slow disk. The code is in bio.c.

缓冲区缓存有两项工作。(1) 同步访问磁盘块，以确保内存中只有一个块的副本，并且一次只有一个内核线程使用该副本；(2) 缓存流行的块，这样它们就不需要从慢速磁盘中重新读取。代码在bio.c中。

![](../img/Figure8.2.png)

The main interface exported by the buffer cache consists of bread and bwrite; the former obtains a buf containing a copy of a block which can be read or modified in memory, and the latter writes a modified buffer to the appropriate block on the disk. A kernel thread must release a buffer by calling brelse when it is done with it. The buffer cache uses a per-buffer sleep-lock to ensure that only one thread at a time uses each buffer (and thus each disk block); bread returns a locked buffer, and brelse releases the lock.

Let’s return to the buffer cache. The buffer cache has a fixed number of buffers to hold disk blocks, which means that if the file system asks for a block that is not already in the cache, the buffer cache must recycle a buffer currently holding some other block. The buffer cache recycles the least recently used buffer for the new block. The assumption is that the least recently used buffer is the one least likely to be used again soon.

缓冲区缓存输出的主要接口包括bread和bwrite，前者获取一个包含内存中可以读取或修改的块的副本的buf，后者将修改后的缓冲区写入磁盘上相应的块。内核线程在处理完一个缓冲区后，必须通过调用brelse释放它。缓冲区缓存使用每个缓冲区的睡眠锁来保证每次只有一个线程使用每个缓冲区（从而使用每个磁盘块）；bread返回一个锁定的缓冲区，而brelse释放锁。

我们再来看看缓冲区缓存。缓冲区缓存有固定数量的缓冲区来存放磁盘块，这意味着如果文件系统要求使用一个尚未在缓存中的块，缓冲区缓存必须回收一个当前存放其他块的缓冲区。缓冲区缓存为新块回收最近使用最少的缓冲区。假设最近使用最少的缓冲区是最不可能很快被再次使用的缓冲区。

## 8.3 Code: Buffer cache

The buffer cache is a doubly-linked list of buffers. The function binit, called by main (kernel/main.c:27), initializes the list with the NBUF buffers in the static array buf (kernel/bio.c:43-52). All other access to the buffer cache refer to the linked list via bcache.head, not the buf array.

A buffer has two state fields associated with it. The field valid indicates that the buffer contains a copy of the block. The field disk indicates that the buffer content has been handed to the disk, which may change the buffer (e.g., write data from the disk into data).
Bread (kernel/bio.c:93) calls bget to get a buffer for the given sector (kernel/bio.c:97). If the buffer needs to be read from disk, bread calls virtio_disk_rw to do that before returning the buffer.

Bget (kernel/bio.c:59) scans the buffer list for a buffer with the given device and sector numbers (kernel/bio.c:65-73). If there is such a buffer, bget acquires the sleep-lock for the buffer. Bget then returns the locked buffer.

缓冲区缓存是一个双链路的缓冲区列表。由main(kernel/main.c:27)调用的函数binit用静态数组buf(kernel/bio.c:43-52)中的NBUF缓冲区来初始化这个列表。其他所有对缓冲区缓存的访问都是通过bcache.head引用链接列表，而不是buf数组。

一个缓冲区有两个与之相关的状态字段。字段valid表示缓冲区包含块的副本。字段disk表示缓冲区的内容已经交给了磁盘，磁盘可能会改变缓冲区的内容（例如，将数据从磁盘写入数据）。
面包（kernel/bio.c:93）调用bget来获取给定扇区（kernel/bio.c:97）的缓冲区。如果缓冲区需要从磁盘上读取，bread会在返回缓冲区之前调用virtio_disk_rw进行读取。

Bget (kernel/bio.c:59)扫描缓冲区列表，寻找带有给定设备和扇区号的缓冲区(kernel/bio.c:65-73)。如果有这样一个缓冲区，bget就会获取该缓冲区的睡眠锁。然后Bget返回被锁定的缓冲区。

If there is no cached buffer for the given sector, bget must make one, possibly reusing a buffer that held a different sector. It scans the buffer list a second time, looking for a buffer that is not in use (b->refcnt = 0); any such buffer can be used. Bget edits the buffer metadata to record the new device and sector number and acquires its sleep-lock. Note that the assignment b->valid = 0 ensures that bread will read the block data from disk rather than incorrectly using the buffer’s previous contents.

It is important that there is at most one cached buffer per disk sector, to ensure that readers see writes, and because the file system uses locks on buffers for synchronization. Bget ensures this invariant by holding the bache.lock continuously from the first loop’s check of whether the block is cached through the second loop’s declaration that the block is now cached (by setting dev, blockno, and refcnt). This causes the check for a block’s presence and (if not present) the designation of a buffer to hold the block to be atomic.

It is safe for bget to acquire the buffer’s sleep-lock outside of the bcache.lock critical section, since the non-zero b->refcnt prevents the buffer from being re-used for a different disk block. The sleep-lock protects reads and writes of the block’s buffered content, while the bcache.lock protects information about which blocks are cached.

如果给定扇区没有缓存的缓冲区，bget必须制作一个，可能会重复使用一个存放不同扇区的缓冲区，它第二次扫描缓冲区列表，寻找没有使用的缓冲区(b->refcnt = 0)；任何这样的缓冲区都可以使用。它第二次扫描缓冲区列表，寻找一个没有使用的缓冲区（b->refcnt = 0）；任何这样的缓冲区都可以使用。Bget编辑缓冲区元数据，记录新的设备和扇区号，并获得其睡眠锁。请注意，赋值b->valid = 0确保面包将从磁盘读取块数据，而不是错误地使用缓冲区以前的内容。

每个磁盘扇区最多只有一个缓存缓冲区，这一点很重要，以确保读者看到写，而且因为文件系统使用缓冲区上的锁进行同步。Bget通过从第一个循环检查块是否被缓存到第二个循环声明块已经被缓存（通过设置dev、blockno和refcnt）的过程中持续保持bache.lock来保证这个不变性。这将导致块的存在检查和（如果不存在）指定一个缓冲区来保存块是原子的。

bget在bcache.lock关键部分之外获取缓冲区的sleep-lock是安全的，因为非零的b->refcnt可以防止缓冲区被重新用于不同的磁盘块。睡眠锁保护块的缓冲内容的读写，而bcache.lock则保护哪些块被缓存的信息。

If all the buffers are busy, then too many processes are simultaneously executing file system calls; bget panics. A more graceful response might be to sleep until a buffer became free, though there would then be a possibility of deadlock.

Once bread has read the disk (if needed) and returned the buffer to its caller, the caller has exclusive use of the buffer and can read or write the data bytes. If the caller does modify the buffer, it must call bwrite to write the changed data to disk before releasing the buffer. Bwrite (kernel/bio.c:107) calls virtio_disk_rw to talk to the disk hardware.

When the caller is done with a buffer, it must call brelse to release it. (The name brelse, a shortening of b-release, is cryptic but worth learning: it originated in Unix and is used in BSD, Linux, and Solaris too.) Brelse (kernel/bio.c:117) releases the sleep-lock and moves the buffer to the front of the linked list (kernel/bio.c:128-133). Moving the buffer causes the list to be ordered by how recently the buffers were used (meaning released): the first buffer in the list is the most recently used, and the last is the least recently used. The two loops in bget take advantage of this: the scan for an existing buffer must process the entire list in the worst case, but checking the most recently used buffers first (starting at bcache.head and following next pointers) will reduce scan time when there is good locality of reference. The scan to pick a buffer to reuse picks the least recently used buffer by scanning backward (following prev pointers).

如果所有的缓冲区都很忙，那么太多的进程同时在执行文件系统调用；bget就会慌乱。更优雅的响应可能是睡眠，直到缓冲区变得空闲为止，不过这样就有可能出现死锁。

一旦面包读取了磁盘（如果需要的话）并将缓冲区返回给调用者，调用者就拥有了对缓冲区的独占使用权，可以读取或写入数据字节。如果调用者确实修改了缓冲区，它必须在释放缓冲区之前调用bwrite将修改后的数据写入磁盘。Bwrite (kernel/bio.c:107)调用virtio_disk_rw与磁盘硬件对话。

当调用者处理完一个缓冲区后，必须调用brelse来释放它。(brelse这个名字是b-release的缩写，很神秘，但值得学习：它起源于Unix，在BSD、Linux和Solaris中也有使用。) Brelse (kernel/bio.c:117)释放睡眠锁，并将缓冲区移动到链接列表的前面(kernel/bio.c:128-133)。移动缓冲区会导致列表按照缓冲区最近使用的时间（意味着释放）排序：列表中第一个缓冲区是最近使用的，最后一个是最近使用的。bget中的两个循环利用了这一点：在最坏的情况下，对现有缓冲区的扫描必须处理整个列表，但当有良好的引用位置性时，先检查最近使用的缓冲区（从bcache.head开始，跟随next指针）将减少扫描时间。扫描选取缓冲区重用的方法是通过向后扫描（跟随prev指针）选取最近使用最少的缓冲区。

## 8.4 Logging layer

One of the most interesting problems in file system design is crash recovery. The problem arises because many file-system operations involve multiple writes to the disk, and a crash after a subset of the writes may leave the on-disk file system in an inconsistent state. For example, suppose a crash occurs during file truncation (setting the length of a file to zero and freeing its content blocks). Depending on the order of the disk writes, the crash may either leave an inode with a reference to a content block that is marked free, or it may leave an allocated but unreferenced content block.

The latter is relatively benign, but an inode that refers to a freed block is likely to cause serious problems after a reboot. After reboot, the kernel might allocate that block to another file, and now we have two different files pointing unintentionally to the same block. If xv6 supported multiple users, this situation could be a security problem, since the old file’s owner would be able to read and write blocks in the new file, owned by a different user.

Xv6 solves the problem of crashes during file-system operations with a simple form of logging.

文件系统设计中最有趣的问题之一是崩溃恢复。这个问题的出现是因为许多文件系统操作涉及到对磁盘的多次写入，而在写入的子集之后发生崩溃可能会使磁盘上的文件系统处于不一致的状态。例如，假设在文件截断（将文件的长度设置为零并释放其内容块）时发生崩溃。根据磁盘写入的顺序，崩溃可能会留下一个对内容块有引用的inode，该内容块被标记为空闲，也可能会留下一个已分配但未引用的内容块。

后者相对来说是良性的，但是一个引用了释放块的inode很可能在重启后造成严重的问题。重启后，内核可能会将该块分配给另一个文件，现在我们有两个不同的文件无意中指向了同一个块。如果xv6支持多个用户，这种情况可能是一个安全问题，因为旧文件的所有者将能够在新文件中读写由不同用户拥有的块。

Xv6用一种简单的日志形式解决了文件系统操作过程中的崩溃问题。

An xv6 system call does not directly write the on-disk file system data structures. Instead, it places a description of all the disk writes it wishes to make in a log on the disk. Once the system call has logged all of its writes, it writes a special commit record to the disk indicating that the log contains a complete operation. At that point the system call copies the writes to the on-disk file system data structures. After those writes have completed, the system call erases the log on disk.

If the system should crash and reboot, the file-system code recovers from the crash as follows, before running any processes. If the log is marked as containing a complete operation, then the recovery code copies the writes to where they belong in the on-disk file system. If the log is not marked as containing a complete operation, the recovery code ignores the log. The recovery code finishes by erasing the log.

Why does xv6’s log solve the problem of crashes during file system operations? If the crash occurs before the operation commits, then the log on disk will not be marked as complete, the recovery code will ignore it, and the state of the disk will be as if the operation had not even started. If the crash occurs after the operation commits, then recovery will replay all of the operation’s writes, perhaps repeating them if the operation had started to write them to the on-disk data structure. In either case, the log makes operations atomic with respect to crashes: after recovery, either all of the operation’s writes appear on the disk, or none of them appear.

xv6系统调用不直接写入磁盘上的文件系统数据结构。相反，它将它希望进行的所有磁盘写入的描述放在磁盘上的日志中。一旦系统调用记录了它的所有写法，它就会向磁盘写入一个特殊的提交记录，表明该日志包含一个完整的操作。这时，系统调用就会把这些写法复制到磁盘上的文件系统数据结构中。在这些写入完成后，系统调用将磁盘上的日志清除。

如果系统应该崩溃并重启，在运行任何进程之前，文件系统代码从崩溃中恢复如下。如果日志被标记为包含一个完整的操作，那么恢复代码就会将写入的内容复制到它们在磁盘文件系统中的所属位置。如果日志未被标记为包含完整的操作，则恢复代码将忽略该日志。恢复代码通过擦除日志来完成。

为什么xv6的日志可以解决文件系统操作过程中的崩溃问题？如果崩溃发生在操作提交之前，那么磁盘上的日志将不会被标记为完整的操作，恢复代码会忽略它，磁盘的状态就像操作还没有开始一样。如果崩溃发生在操作提交之后，那么恢复将重放操作的所有写法，如果操作已经开始将它们写到磁盘数据结构中，也许会重复这些写法。不管是哪种情况，日志都会使操作与崩溃成为原子性的：恢复后，要么所有操作的写都出现在磁盘上，要么都不出现。

## 8.5 Log design

The log resides at a known fixed location, specified in the superblock. It consists of a header block followed by a sequence of updated block copies (“logged blocks”). The header block contains an array of sector numbers, one for each of the logged blocks, and the count of log blocks. The count in the header block on disk is either zero, indicating that there is no transaction in the log, or nonzero, indicating that the log contains a complete committed transaction with the indicated number of logged blocks. Xv6 writes the header block when a transaction commits, but not before, and sets the count to zero after copying the logged blocks to the file system. Thus a crash midway through a transaction will result in a count of zero in the log’s header block; a crash after a commit will result in a non-zero count.

Each system call’s code indicates the start and end of the sequence of writes that must be atomic with respect to crashes. To allow concurrent execution of file-system operations by different processes, the logging system can accumulate the writes of multiple system calls into one transaction.

Thus a single commit may involve the writes of multiple complete system calls. To avoid splitting a system call across transactions, the logging system only commits when no file-system system calls are underway.

日志驻留在一个已知的固定位置，由超级块指定。它由一个头块和一系列更新的块副本（"记录块"）组成。头块包含一个扇区号数组，每个日志块都有一个扇区号，以及日志块的计数。磁盘上头块中的计数要么为零，表示日志中没有事务，要么为非零，表示日志中包含一个完整的提交事务，并有指定数量的日志块。Xv6在事务提交时写入头块，但不在提交前写入，将日志块复制到文件系统后，将计数设为零。因此，一个事务中途的崩溃将导致日志头块的计数为零；提交后的崩溃将导致计数非零。

每个系统调用的代码都会指明写入序列的开始和结束，这些写入序列对于崩溃必须是原子性的。为了允许不同进程并发执行文件系统操作，日志系统可以将多个系统调用的写入累积到一个事务中。

因此，一次提交可能涉及多个完整系统调用的写入。为了避免一个系统调用在不同的事务中分裂，日志系统只有在没有文件系统系统调用正在进行时才会提交。

The idea of committing several transactions together is known as group commit. Group commit reduces the number of disk operations because it amortizes the fixed cost of a commit over multiple operations. Group commit also hands the disk system more concurrent writes at the same time, perhaps allowing the disk to write them all during a single disk rotation. Xv6’s virtio driver doesn’t support this kind of batching, but xv6’s file system design allows for it.

Xv6 dedicates a fixed amount of space on the disk to hold the log. The total number of blocks written by the system calls in a transaction must fit in that space. This has two consequences.

No single system call can be allowed to write more distinct blocks than there is space in the log.

This is not a problem for most system calls, but two of them can potentially write many blocks: write and unlink. A large file write may write many data blocks and many bitmap blocks as well as an inode block; unlinking a large file might write many bitmap blocks and an inode. Xv6’s write system call breaks up large writes into multiple smaller writes that fit in the log, and unlink doesn’t cause problems because in practice the xv6 file system uses only one bitmap block. The other consequence of limited log space is that the logging system cannot allow a system call to start unless it is certain that the system call’s writes will fit in the space remaining in the log.

将几个事务一起提交的想法被称为组提交。组提交减少了磁盘操作的数量，因为它将提交的固定成本摊在了多个操作上。组提交还可以在同一时间交给磁盘系统更多的并发写入，或许可以让磁盘在一次磁盘轮转中把它们全部写入。Xv6的virtio驱动并不支持这种批处理，但xv6的文件系统设计允许这样做。

Xv6在磁盘上划出固定的空间来存放日志。一个事务中系统调用所写的块总数必须适合这个空间。这有两个后果。

任何一个系统调用都不能允许写入超过日志空间的不同块。

这对大多数系统调用来说都不是问题，但其中有两个系统调用可能会写很多块：写和解除链接。一个大文件的写可能会写很多数据块和很多位图块以及一个inode块；解链接一个大文件可能会写很多位图块和一个inode。Xv6的写系统调用将大的写分解成多个适合在日志中的小写，解链接不会引起问题，因为实际上xv6文件系统只使用一个位图块。日志空间有限的另一个后果是，日志系统不能允许系统调用启动，除非确定系统调用的写能适合日志中剩余的空间。

## 8.6 Code: logging

A typical use of the log in a system call looks like this:

一个典型的系统调用中的日志使用是这样的。

```cpp
begin_op();
...
bp = bread(...);
bp->data[...] = ...;
log_write(bp);
...
end_op();
```

begin_op (kernel/log.c:126) waits until the logging system is not currently committing, and until there is enough unreserved log space to hold the writes from this call. log.outstanding counts the number of system calls that have reserved log space; the total reserved space is log.outstanding times MAXOPBLOCKS. Incrementing log.outstanding both reserves space and prevents a commit from occuring during this system call. The code conservatively assumes that each system call might write up to MAXOPBLOCKS distinct blocks.

begin_op(kernel/log.c:126)会一直等到日志系统当前没有提交，并且有足够的未保留的日志空间来容纳这次调用的写入。log.outstanding会统计有保留的日志空间的系统调用次数，总的保留空间是log.outstanding乘以MAXOPBLOCKS。增加log.outstanding既能保留空间，又能防止该系统调用期间发生提交。该代码保守地假设每次系统调用最多可以写入MAXOPBLOCKS不同的块。

log_write (kernel/log.c:214) acts as a proxy for bwrite. It records the block’s sector number in memory, reserving it a slot in the log on disk, and pins the buffer in the block cache to prevent the block cache from evicting it. The block must stay in the cache until committed: until then, the cached copy is the only record of the modification; it cannot be written to its place on disk until after commit; and other reads in the same transaction must see the modifications. log_write notices when a block is written multiple times during a single transaction, and allocates that block the same slot in the log. This optimization is often called absorption. It is common that, for example, the disk block containing inodes of several files is written several times within a transaction. By absorbing several disk writes into one, the file system can save log space and can achieve better performance because only one copy of the disk block must be written to disk.

log_write (kernel/log.c:214) 是bwrite的代理。它将块的扇区号记录在内存中，在磁盘上的日志中保留一个槽，并将缓冲区钉在块缓存中以防止块缓存将其驱逐。在提交之前，块必须留在缓存中：在这之前，缓存的副本是修改的唯一记录；在提交之后才能将其写入磁盘上的位置；同一事务中的其他读必须看到修改的内容。 log_write会注意到当一个块在一个事务中被多次写入时，并在日志中为该块分配相同的槽位。这种优化通常被称为吸收。例如，在一个事务中，包含多个文件的inodes的磁盘块被写入多次，这是常见的情况。通过将几次磁盘写入吸收为一次，文件系统可以节省日志空间，并且可以获得更好的性能，因为只有一份磁盘块的副本必须写入磁盘。

end_op (kernel/log.c:146) first decrements the count of outstanding system calls. If the count is now zero, it commits the current transaction by calling commit(). There are four stages in this process. write_log() (kernel/log.c:178) copies each block modified in the transaction from the buffer cache to its slot in the log on disk. write_head() (kernel/log.c:102) writes the header block to disk: this is the commit point, and a crash after the write will result in recovery replaying the transaction’s writes from the log. install_trans (kernel/log.c:69) reads each block from the log and writes it to the proper place in the file system. Finally end_op writes the log header with a count of zero; this has to happen before the next transaction starts writing logged blocks, so that a crash doesn’t result in recovery using one transaction’s header with the subsequent transaction’s logged blocks.

end_op (kernel/log.c:146)首先递减未完成的系统调用次数。如果计数为零，则通过调用commit()来提交当前事务。write_log()(kernel/log.c:178)将事务中修改的每个块从缓冲区缓存中复制到磁盘上的日志槽中。 write_head()(kernel/log.c:102)将头块写到磁盘上：这是提交点，写完后的崩溃将导致恢复从日志中重放事务的写法。 install_trans(kernel/log.c:69)从日志中读取每个块，并将其写到文件系统中的适当位置。最后 end_op 写入计数为零的日志头；这必须在下一个事务开始写日志块之前发生，这样就不会因为崩溃而导致使用一个事务的头和后续事务的日志块进行恢复。

recover_from_log (kernel/log.c:116) is called from initlog (kernel/log.c:55), which is called from fsinit(kernel/fs.c:42) during boot before the first user process runs (kernel/proc.c:539). It reads the log header, and mimics the actions of end_op if the header indicates that the log contains a committed transaction An example use of the log occurs in filewrite (kernel/file.c:135). The transaction looks like this:

recover_from_log (kernel/log.c:116) 是在 initlog (kernel/log.c:55) 中调用的，而 initlog 是在启动过程中，在第一个用户进程运行 (kernel/proc.c:539) 之前，由 fsinit(kernel/fs.c:42) 调用的。它读取日志头，如果日志头显示日志中包含一个已提交的事务，它就会模仿end_op的操作。这个事务看起来像这样。

```cpp
begin_op();
ilock(f->ip);
r = writei(f->ip, ...);
iunlock(f->ip);
end_op();
```

This code is wrapped in a loop that breaks up large writes into individual transactions of just a few sectors at a time, to avoid overflowing the log. The call to writei writes many blocks as part of this transaction: the file’s inode, one or more bitmap blocks, and some data blocks.

这段代码被包裹在一个循环中，它将大的写入分解成每次只有几个扇区的单独事务，以避免溢出日志。调用 writei 写入许多块作为这个事务的一部分：文件的 inode，一个或多个位图块，以及一些数据块。

## 8.7 Code: Block allocator

File and directory content is stored in disk blocks, which must be allocated from a free pool. xv6’s block allocator maintains a free bitmap on disk, with one bit per block. A zero bit indicates that the corresponding block is free; a one bit indicates that it is in use. The program mkfs sets the bits corresponding to the boot sector, superblock, log blocks, inode blocks, and bitmap blocks.

The block allocator provides two functions: balloc allocates a new disk block, and bfree frees a block. Balloc The loop in balloc at (kernel/fs.c:71) considers every block, starting at block 0 up to sb.size, the number of blocks in the file system. It looks for a block whose bitmap bit is zero, indicating that it is free. If balloc finds such a block, it updates the bitmap and returns the block. For efficiency, the loop is split into two pieces. The outer loop reads each block of bitmap bits. The inner loop checks all BPB bits in a single bitmap block. The race that might occur if two processes try to allocate a block at the same time is prevented by the fact that the buffer cache only lets one process use any one bitmap block at a time.

Bfree (kernel/fs.c:90) finds the right bitmap block and clears the right bit. Again the exclusive use implied by bread and brelse avoids the need for explicit locking.

As with much of the code described in the remainder of this chapter, balloc and bfree must be called inside a transaction.

文件和目录内容存储在磁盘块中，必须从空闲池中分配。xv6的块分配器在磁盘上维护一个空闲的位图，每个块有一个位。0位表示对应的块是空闲的，1位表示正在使用中。程序mkfs设置引导扇区、超级块、日志块、inode块和位图块对应的位。

块分配器提供了两个函数：balloc分配一个新的磁盘块，bfree释放一个块。balloc 位于(kernel/fs.c:71)的 balloc 循环会考虑每一个块，从块0开始，直到文件系统中的块数sb.size。它寻找一个位图位为0的块，表示它是空闲的。如果 balloc 找到了这样一个块，它就会更新位图并返回该块。为了提高效率，这个循环被分成两部分。外循环读取每个位图位块。内循环检查单个位图块中的所有BPB位。由于缓冲区缓存只允许一个进程同时使用一个位图块，因此避免了两个进程同时分配一个位图块时可能发生的竞赛。

Bfree (kernel/fs.c:90) 找到正确的位图块并清除正确的位。同样，面包和brelse所暗示的独占使用避免了显式锁定的需要。

就像本章其余部分描述的大部分代码一样，balloc和bfree必须在事务中调用。

## 8.8 Inode layer

The term inode can have one of two related meanings. It might refer to the on-disk data structure containing a file’s size and list of data block numbers. Or “inode” might refer to an in-memory inode, which contains a copy of the on-disk inode as well as extra information needed within the kernel.

The on-disk inodes are packed into a contiguous area of disk called the inode blocks. Every inode is the same size, so it is easy, given a number n, to find the nth inode on the disk. In fact, this number n, called the inode number or i-number, is how inodes are identified in the implementation.

The on-disk inode is defined by a struct dinode (kernel/fs.h:32). The type field distinguishes between files, directories, and special files (devices). A type of zero indicates that an on disk inode is free. The nlink field counts the number of directory entries that refer to this inode, in order to recognize when the on-disk inode and its data blocks should be freed. The size field records the number of bytes of content in the file. The addrs array records the block numbers of the disk blocks holding the file’s content.

术语inode可以有两种相关含义之一。它可能指的是磁盘上的数据结构，其中包含了文件的大小和数据块号的列表。或者 "inode "可能指的是内存中的inode，它包含了磁盘上inode的副本以及内核中需要的额外信息。

磁盘上的inode被打包成一个连续的区域，称为inode块。每一个inode的大小都是一样的，所以给定一个数字n，很容易找到磁盘上的第n个inode。事实上，这个数字n，被称为inode号或i-number，在实现中就是这样识别inode的。

磁盘上的inode是由一个结构dinode（kernel/fs.h:32）定义的。类型字段区分了文件、目录和特殊文件（设备）。类型为0表示一个磁盘上的inode是空闲的。nlink字段计算引用这个inode的目录条目的数量，以便识别何时应该释放磁盘上的inode和它的数据块。size字段记录了文件中内容的字节数。addrs数组记录了存放文件内容的磁盘块的块号。

The kernel keeps the set of active inodes in memory; struct inode (kernel/file.h:17) is the in-memory copy of a struct dinode on disk. The kernel stores an inode in memory only if there are C pointers referring to that inode. The ref field counts the number of C pointers referring to the in-memory inode, and the kernel discards the inode from memory if the reference count drops to zero. The iget and iput functions acquire and release pointers to an inode, modifying the reference count. Pointers to an inode can come from file descriptors, current working directories, and transient kernel code such as exec.

There are four lock or lock-like mechanisms in xv6’s inode code. icache.lock protects the invariant that an inode is present in the cache at most once, and the invariant that a cached inode’s ref field counts the number of in-memory pointers to the cached inode. Each in-memory inode has a lock field containing a sleep-lock, which ensures exclusive access to the inode’s fields (such as file length) as well as to the inode’s file or directory content blocks. An inode’s ref, if it is greater than zero, causes the system to maintain the inode in the cache, and not re-use the cache entry for a different inode. Finally, each inode contains a nlink field (on disk and copied in memory if it is cached) that counts the number of directory entries that refer to a file; xv6 won’t free an inode if its link count is greater than zero.

内核将活动的inode集保存在内存中；struct inode (kernel/file.h:17)是磁盘上一个struct dinode的内存拷贝。内核只在有C指针指向一个inode的时候才会在内存中存储这个inode。ref字段计算引用内存中inode的C指针的数量，如果引用数量降到零，内核就会从内存中丢弃这个inode。iget和iput函数获取和释放指向inode的指针，修改引用计数。指向inode的指针可以来自文件描述符、当前工作目录和瞬时内核代码，如exec。

在xv6的inode代码中，有四种锁或类似锁的机制。icache.lock保护了一个inode在缓存中最多存在一次的不变性，以及一个缓存inode的ref字段计算缓存inode的内存指针数的不变性。每个内存中的inode都有一个包含睡眠锁的锁字段，它保证了对inode的字段（如文件长度）以及inode的文件或目录内容块的独占访问。一个inode的ref如果大于0，则会导致系统将该inode保留在缓存中，而不会为不同的inode重新使用缓存条目。最后，每个inode都包含一个nlink字段(在磁盘上，如果是缓存，则复制在内存中)，用来统计引用文件的目录项的数量；如果一个inode的链接数大于零，xv6不会释放它。

A struct inode pointer returned by iget() is guaranteed to be valid until the corresponding call to iput(); the inode won’t be deleted, and the memory referred to by the pointer won’t be re-used for a different inode. iget() provides non-exclusive access to an inode, so that there can be many pointers to the same inode. Many parts of the file-system code depend on this behavior of iget(), both to hold long-term references to inodes (as open files and current directories) and to prevent races while avoiding deadlock in code that manipulates multiple inodes (such as pathname lookup).

The struct inode that iget returns may not have any useful content. In order to ensure it holds a copy of the on-disk inode, code must call ilock. This locks the inode (so that no other process can ilock it) and reads the inode from the disk, if it has not already been read. iunlock releases the lock on the inode. Separating acquisition of inode pointers from locking helps avoid deadlock in some situations, for example during directory lookup. Multiple processes can hold a C pointer to an inode returned by iget, but only one process can lock the inode at a time.

The inode cache only caches inodes to which kernel code or data structures hold C pointers. Its main job is really synchronizing access by multiple processes; caching is secondary. If an inode is used frequently, the buffer cache will probably keep it in memory if it isn’t kept by the inode cache. The inode cache is write-through, which means that code that modifies a cached inode must immediately write it to disk with iupdate 

iget()返回的inode结构指针保证有效，直到相应的调用iput()为止；inode不会被删除，指针引用的内存也不会被另一个inode重新使用。iget()提供了对inode的非独占性访问，因此可以有许多指针指向同一个inode。文件系统代码的许多部分都依赖于iget()的这种行为，既是为了保持对inode的长期引用(如打开的文件和当前的目录)，也是为了在操作多个inode的代码(如路径名查找)中避免死锁的同时防止比赛。

iget返回的inode结构可能没有任何有用的内容。为了确保它拥有磁盘上inode的副本，代码必须调用ilock。这将锁住inode(这样其他进程就不能将其锁住)，并从磁盘上读取inode(如果它还没有被读取)。将获取inode指针和锁定分开，有助于在某些情况下避免死锁，例如在目录查找时。多个进程可以持有一个由iget返回的inode的C指针，但每次只有一个进程可以锁定inode。

inode缓存只缓存内核代码或数据结构持有C指针的inode。它的主要工作其实是同步多个进程的访问，缓存是次要的。如果一个inode被频繁使用，如果不被inode缓存保存，缓冲区缓存可能会把它保存在内存中。inode缓存是穿透式的，这意味着修改缓存的inode的代码必须立即用iupdate把它写入磁盘。

## 8.9 Code: Inodes

To allocate a new inode (for example, when creating a file), xv6 calls ialloc (kernel/fs.c:196).

Ialloc is similar to balloc: it loops over the inode structures on the disk, one block at a time, looking for one that is marked free. When it finds one, it claims it by writing the new type to the disk and then returns an entry from the inode cache with the tail call to iget (kernel/fs.c:210). The correct operation of ialloc depends on the fact that only one process at a time can be holding a reference to bp: ialloc can be sure that some other process does not simultaneously see that the inode is available and try to claim it.

Iget (kernel/fs.c:243) looks through the inode cache for an active entry (ip->ref > 0) with the desired device and inode number. If it finds one, it returns a new reference to that inode (kernel/fs.c:252-256). As iget scans, it records the position of the first empty slot (kernel/fs.c:257-258), which it uses if it needs to allocate a cache entry.

Code must lock the inode using ilock before reading or writing its metadata or content. Ilock (kernel/fs.c:289) uses a sleep-lock for this purpose. Once ilock has exclusive access to the inode, it reads the inode from disk (more likely, the buffer cache) if needed. The function iunlock (kernel/fs.c:317) releases the sleep-lock, which may cause any processes sleeping to be woken up.

要分配一个新的inode(例如，当创建一个文件时)，xv6调用ialloc(kernel/fs.c:196)。

Ialloc 类似于 balloc：它循环浏览磁盘上的 inode 结构，一次一个块，寻找一个标记为空闲的。当它找到一个时，它会通过向磁盘写入新的类型来声明它，然后用尾部调用 iget (kernel/fs.c:210) 从 inode 缓存中返回一个条目。ialloc的正确操作依赖于这样一个事实，即一次只能有一个进程持有对bp的引用：ialloc可以确保其他进程不会同时看到这个inode是可用的，并试图声称它。

Iget (kernel/fs.c:243)在inode缓存中寻找具有所需设备和inode号的活动条目(ip->ref > 0)。如果它找到了，它就返回一个新的对该inode的引用(kernel/fs.c:252-256)。当iget扫描时，它会记录第一个空槽的位置(kernel/fs.c:257-258)，如果它需要分配一个缓存条目，就会使用它。

在读写inode的元数据或内容之前，代码必须使用ilock锁定它。Ilock(kernel/fs.c:289)使用睡眠锁来实现这一目的。一旦ilock拥有对inode的独占访问权，如果需要的话，它就会从磁盘（更有可能是缓冲区缓存）读取inode。函数iunlock (kernel/fs.c:317)释放睡眠锁，这可能会导致任何正在睡眠的进程被唤醒。

Iput (kernel/fs.c:333) releases a C pointer to an inode by decrementing the reference count (kernel/fs.c:356). If this is the last reference, the inode’s slot in the inode cache is now free and can be re-used for a different inode.

If iput sees that there are no C pointer references to an inode and that the inode has no links to it (occurs in no directory), then the inode and its data blocks must be freed. Iput calls itrunc to truncate the file to zero bytes, freeing the data blocks; sets the inode type to 0 (unallocated); and writes the inode to disk (kernel/fs.c:338).

The locking protocol in iput in the case in which it frees the inode deserves a closer look. One danger is that a concurrent thread might be waiting in ilock to use this inode (e.g., to read a file or list a directory), and won’t be prepared to find that the inode is not longer allocated. This can’t happen because there is no way for a system call to get a pointer to a cached inode if it has no links to it and ip->ref is one. That one reference is the reference owned by the thread calling iput. It’s true that iput checks that the reference count is one outside of its icache.lock critical section, but at that point the link count is known to be zero, so no thread will try to acquire a new reference.

The other main danger is that a concurrent call to ialloc might choose the same inode that iput is freeing. This can only happen after the iupdate writes the disk so that the inode has type zero.

Iput (kernel/fs.c:333) 通过递减引用次数 (kernel/fs.c:356) 来释放一个指向 inode 的 C 指针。如果这是最后一次引用，那么inode缓存中的inode插槽现在是空闲的，可以重新用于其他inode。

如果iput看到没有对inode的C指针引用，而且inode没有链接到它（不在任何目录中出现），那么必须释放inode和它的数据块。Iput调用itrunc将文件截断为零字节，释放数据块；将inode类型设置为0（未分配）；并将inode写入磁盘（kernel/fs.c:338）。

iput在释放inode的情况下，其锁定协议值得仔细研究。一个危险是，一个并发线程可能会在ilock中等待使用这个inode（例如，读取一个文件或列出一个目录），而不会准备好发现这个inode不再被分配。这是不可能发生的，因为如果一个系统调用没有链接到一个缓存的inode，而ip->ref是一个指针，那么系统调用就没有办法得到这个指针。这一个引用就是调用iput的线程所拥有的引用。的确，iput会在其icache.lock临界部分之外检查引用数是否为1，但此时已知链接数为0，所以没有线程会尝试获取新的引用。

另一个主要的危险是，对ialloc的并发调用可能会选择iput正在释放的同一个inode。这种情况只有在iupdate写入磁盘，使inode的类型为零之后才能发生。

This race is benign; the allocating thread will politely wait to acquire the inode’s sleep-lock before reading or writing the inode, at which point iput is done with it.

iput() can write to the disk. This means that any system call that uses the file system may write the disk, because the system call may be the last one having a reference to the file. Even calls like read() that appear to be read-only, may end up calling iput(). This, in turn, means that even read-only system calls must be wrapped in transactions if they use the file system.

这种竞赛是良性的，分配线程会礼貌地等待获取inode的sleep-lock，然后再读写inode，这时iput就结束了。

iput()可以对磁盘进行写入。这意味着任何使用文件系统的系统调用都可能会写入磁盘，因为系统调用可能是最后一个对文件有引用的调用。甚至像read()这样看似只读的调用，最终也可能会调用iput()。这又意味着，即使是只读的系统调用，如果使用了文件系统，也必须用事务来包装。

![](../img/Figure8.3.png)

There is a challenging interaction between iput() and crashes. iput() doesn’t truncate a file immediately when the link count for the file drops to zero, because some process might still hold a reference to the inode in memory: a process might still be reading and writing to the file, because it successfully opened it. But, if a crash happens before the last process closes the file descriptor for the file, then the file will be marked allocated on disk but no directory entry will point to it.

File systems handle this case in one of two ways. The simple solution is that on recovery, after reboot, the file system scans the whole file system for files that are marked allocated, but have no directory entry pointing to them. If any such file exists, then it can free those files.

The second solution doesn’t require scanning the file system. In this solution, the file system records on disk (e.g., in the super block) the inode inumber of a file whose link count drops to zero but whose reference count isn’t zero. If the file system removes the file when its reference counts reaches 0, then it updates the on-disk list by removing that inode from the list. On recovery, the file system frees any file in the list.

Xv6 implements neither solution, which means that inodes may be marked allocated on disk, even though they are not in use anymore. This means that over time xv6 runs the risk that it may run out of disk space.

iput()和崩溃之间有一个具有挑战性的交互作用。当文件的链接数降到零时，iput()不会立即截断一个文件，因为一些进程可能仍然在内存中持有对inode的引用：一个进程可能仍然在对文件进行读写，因为它成功地打开了它。但是，如果在最后一个进程关闭该文件的文件描述符之前发生了崩溃，那么该文件将在磁盘上被标记为已分配，但没有目录条目指向它。

文件系统对这种情况的处理方式有两种。简单的解决办法是，在恢复时，重启后，文件系统会扫描整个文件系统，寻找那些被标记为已分配，但没有目录条目指向的文件。如果有这样的文件存在，那么就可以释放这些文件。

第二种解决方案不需要扫描文件系统。在这种解决方案中，文件系统在磁盘上记录（例如在超级块中）链接数降为零但引用数不为零的文件的inode inumber。如果文件系统在其引用计数达到0时删除文件，那么它就会更新磁盘上的列表，从列表中删除该inode。在恢复时，文件系统会释放列表中的任何文件。

Xv6实现了这两种解决方案，这意味着inode可能被标记为在磁盘上分配，即使它们不再使用。这意味着随着时间的推移，xv6会面临磁盘空间耗尽的风险。

## 8.10 Code: Inode content

The on-disk inode structure, struct dinode, contains a size and an array of block numbers (see Figure 8.3). The inode data is found in the blocks listed in the dinode ’s addrs array. The first NDIRECT blocks of data are listed in the first NDIRECT entries in the array; these blocks are called direct blocks. The next NINDIRECT blocks of data are listed not in the inode but in a data block called the indirect block. The last entry in the addrs array gives the address of the indirect block.

Thus the first 12 kB ( NDIRECT x BSIZE) bytes of a file can be loaded from blocks listed in the inode, while the next 256 kB ( NINDIRECT x BSIZE) bytes can only be loaded after consulting the indirect block. This is a good on-disk representation but a complex one for clients. The function bmap manages the representation so that higher-level routines such as readi and writei, which we will see shortly. Bmap returns the disk block number of the bn’th data block for the inode ip. If ip does not have such a block yet, bmap allocates one. 

磁盘上的inode结构，即dinode结构，包含一个大小和一个块号数组（见图8.3）。在dinode's addrs数组中列出的块中可以找到inode数据。第一个NDIRECT数据块列在数组中的第一个NDIRECT条目中，这些块称为直接块。接下来的NINDIRECT数据块不是列在inode中，而是列在称为间接块的数据块中。addrs数组中的最后一个条目给出了间接块的地址。

因此，一个文件的前12 kB ( NDIRECT x BSIZE)字节可以从inode中列出的块中加载，而接下来的256 kB ( NINDIRECT x BSIZE)字节只能在查阅间接块后才能加载。这是一种很好的磁盘上的表示方式，但对客户机来说是一种复杂的表示方式。函数bmap管理了这种表示方式，所以更高级别的例程，如readi和writei，我们将很快看到。Bmap返回inode ip的第bn'th个数据块的磁盘块号。如果ip还没有这样的数据块，bmap会分配一个。

The function bmap (kernel/fs.c:378) begins by picking off the easy case: the first NDIRECT blocks are listed in the inode itself (kernel/fs.c:383-387). The next NINDIRECT blocks are listed in the indirect block at ip->addrs[NDIRECT]. Bmap reads the indirect block (kernel/fs.c:394) and then reads a block number from the right position within the block (kernel/fs.c:395). If the block number exceeds NDIRECT+NINDIRECT, bmap panics; writei contains the check that prevents this from happening (kernel/fs.c:490).

Bmap allocates blocks as needed. An ip->addrs[] or indirect entry of zero indicates that no block is allocated. As bmap encounters zeros, it replaces them with the numbers of fresh blocks, allocated on demand (kernel/fs.c:384-385) (kernel/fs.c:392-393).

itrunc frees a file’s blocks, resetting the inode’s size to zero. Itrunc (kernel/fs.c:410) starts by freeing the direct blocks(kernel/fs.c:416-421), then the ones listed in the indirect block (kernel/fs.c:426-429), and finally the indirect block itself (kernel/fs.c:431-432).

函数bmap(kernel/fs.c:378)首先挑出简单的情况：第一个NDIRECT块列在inode本身(kernel/fs.c:383-387)中，接下来的NINDIRECT块列在ip->addrs[NDIRECT]的间接块中。接下来的NINDIRECT块被列在ip->addrs[NDIRECT]的间接块中。Bmap读取间接块(kernel/fs.c:394)，然后从块内的右边位置读取一个块号(kernel/fs.c:395)。如果块号超过NDIRECT+NINDIRECT，bmap就会慌乱；writei包含防止这种情况发生的检查(kernel/fs.c:490)。

Bmap根据需要分配块。ip->addrs[]或间接条目为0表示没有分配块。当bmap遇到零的时候，它会用按需分配的新块的数量来替换它们(kernel/fs.c:384-385) (kernel/fs.c:392-393)。

itrunc 释放一个文件的块，将inode的大小重置为零。Itrunc (kernel/fs.c:410) 首先释放直接块(kernel/fs.c:416-421)，然后释放间接块中列出的块(kernel/fs.c:426-429)，最后释放间接块本身(kernel/fs.c:431-432)。

Bmap makes it easy for readi and writei to get at an inode’s data. Readi (kernel/fs.c:456) starts by making sure that the offset and count are not beyond the end of the file. Reads that start beyond the end of the file return an error (kernel/fs.c:461-462) while reads that start at or cross the end of the file return fewer bytes than requested (kernel/fs.c:463-464). The main loop processes each block of the file, copying data from the buffer into dst (kernel/fs.c:466-474). writei (kernel/fs.c:483) is identical to readi, with three exceptions: writes that start at or cross the end of the file grow the file, up to the maximum file size (kernel/fs.c:490-491); the loop copies data into the buffers instead of out (kernel/fs.c:36); and if the write has extended the file, writei must update its size (kernel/fs.c:504-511).

Both readi and writei begin by checking for ip->type == T_DEV. This case handles special devices whose data does not live in the file system; we will return to this case in the file descriptor layer.

The function stati (kernel/fs.c:442) copies inode metadata into the stat structure, which is exposed to user programs via the stat system call.

Bmap 使得 readi 和 writei 可以很容易地获取一个 inode 的数据。Readi (kernel/fs.c:456)首先要确定偏移量和计数没有超过文件的末端。从文件末尾开始的读取会返回一个错误(kernel/fs.c:461-462)，而从文件末尾开始或越过文件末尾的读取会返回比请求的字节数更少的数据(kernel/fs.c:463-464)。writei (kernel/fs.c:483)与readi相同，但有三个例外：从文件末尾开始或越过文件末尾的写入会使文件增长，最大文件大小不超过(kernel/fs.c:490-491)。 c:490-491）；循环将数据复制到缓冲区而不是输出（kernel/fs.c:36）；如果写扩展了文件，writei必须更新它的大小（kernel/fs.c:504-511）。

readi 和 writei 都是通过检查 ip->type == T_DEV 开始的。这种情况处理的是数据不在文件系统中的特殊设备；我们将在文件描述符层中返回这种情况。

函数stati(kernel/fs.c:442)将inode元数据复制到stat结构中，该结构通过stat系统调用暴露给用户程序。

## 8.11 Code: directory layer

A directory is implemented internally much like a file. Its inode has type T_DIR and its data is a sequence of directory entries. Each entry is a struct dirent (kernel/fs.h:56), which contains a name and an inode number. The name is at most DIRSIZ (14) characters; if shorter, it is terminated by a NUL (0) byte. Directory entries with inode number zero are free.

The function dirlookup (kernel/fs.c:527) searches a directory for an entry with the given name. If it finds one, it returns a pointer to the corresponding inode, unlocked, and sets *poff to the byte offset of the entry within the directory, in case the caller wishes to edit it. If dirlookup finds an entry with the right name, it updates *poff and returns an unlocked inode obtained via iget. Dirlookup is the reason that iget returns unlocked inodes. The caller has locked dp, so if the lookup was for ., an alias for the current directory, attempting to lock the inode before returning would try to re-lock dp and deadlock. (There are more complicated deadlock scenarios involving multiple processes and .., an alias for the parent directory; . is not the only problem.) The caller can unlock dp and then lock ip, ensuring that it only holds one lock at a time.

The function dirlink (kernel/fs.c:554) writes a new directory entry with the given name and inode number into the directory dp. If the name already exists, dirlink returns an error (kernel/fs.c:560-564). The main loop reads directory entries looking for an unallocated entry. When it finds one, it stops the loop early (kernel/fs.c:538-539), with off set to the offset of the available entry. Otherwise, the loop ends with off set to dp->size. Either way, dirlink then adds a new entry to the directory by writing at offset off (kernel/fs.c:574-577).

目录的内部实现很像文件。它的inode类型是T_DIR，它的数据是一个目录项的序列。每个条目是一个结构dirent(kernel/fs.h:56)，它包含一个名称和一个inode号。名称最多包含DIRSIZ(14)个字符，如果较短，则以NUL(0)字节结束。索引码号为0的目录条目是空闲的。

函数dirlookup (kernel/fs.c:527)在目录中搜索给定名称的条目。如果找到了，它返回一个指向对应的inode的指针，解锁，并将*poff设置为目录内条目的字节偏移量，以备调用者想编辑它。如果dirlookup找到一个名字正确的条目，则更新*poff，并返回一个通过iget获得的解锁的inode。Dirlookup是iget返回未锁定的inode的原因。调用者已经锁定了dp，所以如果查找的是.，是当前目录的别名，在返回之前试图锁定inode，就会试图重新锁定dp而死锁。(还有更复杂的死锁情况，涉及到多个进程和.，一个父目录的别名；.不是唯一的问题。) 调用者可以先解锁dp，然后再锁定ip，保证一次只持有一个锁。

函数dirlink(kernel/fs.c:554)用给定的名称和inode号向目录dp中写入一个新的目录条目。如果名字已经存在，dirlink 返回一个错误(kernel/fs.c:560-564)。主循环读取目录条目，寻找一个未分配的条目。当它找到一个时，它会提前停止循环(kernel/fs.c:538-539)，并将off set设置为可用条目的偏移量。否则，循环结束时，off设置为dp->size。不管是哪种方式，dirlink都会在偏移量off的位置添加一个新的条目到目录中(kernel/fs.c:574-577)。

## 8.12 Code: Path names

Path name lookup involves a succession of calls to dirlookup, one for each path component.

Namei (kernel/fs.c:661) evaluates path and returns the corresponding inode. The function nameiparent is a variant: it stops before the last element, returning the inode of the parent directory and copying the final element into name. Both call the generalized function namex to do the real work.

Namex (kernel/fs.c:626) starts by deciding where the path evaluation begins. If the path begins with a slash, evaluation begins at the root; otherwise, the current directory (kernel/fs.c:630-633).

Then it uses skipelem to consider each element of the path in turn (kernel/fs.c:635). Each iteration of the loop must look up name in the current inode ip. The iteration begins by locking ip and checking that it is a directory. If not, the lookup fails (kernel/fs.c:636-640). (Locking ip is necessary not because ip->type can change underfoot—it can’t—but because until ilock runs, ip->type is not guaranteed to have been loaded from disk.) If the call is nameiparent and this is the last path element, the loop stops early, as per the definition of nameiparent; the final path element has already been copied into name, so namex need only return the unlocked ip (kernel/fs.c:641-645).

Finally, the loop looks for the path element using dirlookup and prepares for the next iteration by setting ip = next (kernel/fs.c:646-651). When the loop runs out of path elements, it returns ip.

路径名查找涉及到对dirlookup的一系列调用，每个路径组件调用一次。

Namei (kernel/fs.c:661) 评估路径并返回相应的inode。函数nameiparent是一个变体：它在最后一个元素之前停止，返回父目录的inode，并将最后一个元素复制到name中。两者都调用通用函数namex来完成真正的工作。

Namex (kernel/fs.c:626)首先决定路径评估从哪里开始。如果路径以斜线开头，则从根目录开始计算；否则，从当前目录开始计算(kernel/fs.c:630-633)。

然后它使用 skipelem 依次考虑路径中的每个元素(kernel/fs.c:635)。循环的每次迭代都必须在当前inode ip中查找name。迭代的开始是锁定ip并检查它是否是一个目录。如果不是，查找就会失败(kernel/fs.c:636-640)。(锁定ip是必要的，并不是因为ip->type可以在脚下改变--它不能--而是因为在ilock运行之前，不能保证ip->type已经从磁盘上加载了。) 如果调用的是nameiparent，并且这是最后一个路径元素，那么按照nameiparent的定义，循环会提前停止；最后一个路径元素已经被复制到name中，所以namex只需要返回解锁的ip（kernel/fs.c:641-645）。

最后，循环使用dirlookup查找路径元素，并通过设置ip = next为下一次迭代做准备(kernel/fs.c:646-651)。当循环用完路径元素时，它返回ip。

The procedure namex may take a long time to complete: it could involve several disk operations to read inodes and directory blocks for the directories traversed in the pathname (if they are not in the buffer cache). Xv6 is carefully designed so that if an invocation of namex by one kernel thread is blocked on a disk I/O, another kernel thread looking up a different pathname can proceed concurrently. Namex locks each directory in the path separately so that lookups in different directories can proceed in parallel.

This concurrency introduces some challenges. For example, while one kernel thread is looking up a pathname another kernel thread may be changing the directory tree by unlinking a directory.

A potential risk is that a lookup may be searching a directory that has been deleted by another kernel thread and its blocks have been re-used for another directory or file.

Xv6 avoids such races. For example, when executing dirlookup in namex, the lookup thread holds the lock on the directory and dirlookup returns an inode that was obtained using iget.

Iget increases the reference count of the inode. Only after receiving the inode from dirlookup does namex release the lock on the directory. Now another thread may unlink the inode from the directory but xv6 will not delete the inode yet, because the reference count of the inode is still larger than zero.

Another risk is deadlock. For example, next points to the same inode as ip when looking up ".". Locking next before releasing the lock on ip would result in a deadlock. To avoid this deadlock, namex unlocks the directory before obtaining a lock on next. Here again we see why the separation between iget and ilock is important.

存储过程namex可能需要很长的时间来完成：它可能涉及几个磁盘操作来读取路径名中遍历的目录的inodes和目录块（如果它们不在缓冲区缓存中）。Xv6经过精心设计，如果一个内核线程对namex的调用在磁盘I/O上受阻，另一个内核线程查找不同的路径名可以同时进行。Namex分别锁定了路径中的每个目录，因此不同目录中的查找可以并行进行。

这种并发性带来了一些挑战。例如，当一个内核线程正在查找一个路径名时，另一个内核线程可能会通过取消一个目录的链接来改变目录树。

一个潜在的风险是，一个查找可能正在搜索一个已经被另一个内核线程删除的目录，而它的块已经被另一个目录或文件重新使用。

Xv6避免了这种竞赛。例如，当在namex中执行dirlookup时，查找线程持有目录的锁，dirlookup返回一个使用iget获得的inode。

Iget会增加inode的引用次数。只有从dirlookup接收到inode后，namex才会释放目录上的锁。现在另一个线程可能会从目录中解开inode的链接，但xv6还不会删除inode，因为inode的引用数仍然大于零。

另一个风险是死锁。例如，在查找". "时，next与ip指向同一个inode。在释放对ip的锁之前锁定next会导致死锁。为了避免这种死锁，namex会在取得对next的锁之前解锁目录。这里我们再次看到为什么 iget和ilock之间的分离很重要。

## 8.13 File descriptor layer

A cool aspect of the Unix interface is that most resources in Unix are represented as files, including devices such as the console, pipes, and of course, real files. The file descriptor layer is the layer that achieves this uniformity.

Xv6 gives each process its own table of open files, or file descriptors, as we saw in Chapter 1.

Each open file is represented by a struct file (kernel/file.h:1), which is a wrapper around either an inode or a pipe, plus an I/O offset. Each call to open creates a new open file (a new struct file): if multiple processes open the same file independently, the different instances will have different I/O offsets. On the other hand, a single open file (the same struct file) can appear multiple times in one process’s file table and also in the file tables of multiple processes. This would happen if one process used open to open the file and then created aliases using dup or shared it with a child using fork. A reference count tracks the number of references to a particular open file. A file can be open for reading or writing or both. The readable and writable fields track this.

All the open files in the system are kept in a global file table, the ftable. The file table has functions to allocate a file (filealloc), create a duplicate reference (filedup), release a reference (fileclose), and read and write data (fileread and filewrite).

The first three follow the now-familiar form. Filealloc (kernel/file.c:30) scans the file table for an unreferenced file (f->ref == 0) and returns a new reference; filedup (kernel/file.c:48) increments the reference count; and fileclose (kernel/file.c:60) decrements it. When a file’s reference count reaches zero, fileclose releases the underlying pipe or inode, according to the type.

The functions filestat, fileread, and filewrite implement the stat, read, and write operations on files. Filestat (kernel/file.c:88) is only allowed on inodes and calls stati. Fileread and filewrite check that the operation is allowed by the open mode and then pass the call
through to either the pipe or inode implementation. If the file represents an inode, fileread and filewrite use the I/O offset as the offset for the operation and then advance it (kernel/file.c:122-123) (kernel/file.c:153-154). Pipes have no concept of offset. Recall that the inode functions require the caller to handle locking (kernel/file.c:94-96) (kernel/file.c:121-124) (kernel/file.c:163-166). The inode locking has the convenient side effect that the read and write offsets are updated atomically, so that multiple writing to the same file simultaneously cannot overwrite each other’s data, though their writes may end up interlaced.

Unix界面的一个很酷的方面是，Unix中的大部分资源都是以文件的形式来表示的，包括控制台、管道等设备，当然还有真实的文件。文件描述符层就是实现这种统一性的一层。

Xv6给每个进程提供了自己的打开文件表，或者说文件描述符，就像我们在第1章看到的那样。

每个打开的文件由一个结构文件（kernel/file.h:1）来表示，它是一个inode或管道的包装器，加上一个I/O偏移。每次调用open都会创建一个新的打开文件（一个新的结构文件）：如果多个进程独立打开同一个文件，那么不同的实例会有不同的I/O偏移量。另一方面，一个打开的文件（同一个结构文件）可以多次出现在一个进程的文件表中，也可以出现在多个进程的文件表中。如果一个进程使用open打开文件，然后使用dup创建别名，或者使用fork与子进程共享文件，就会出现这种情况。引用计数跟踪特定打开文件的引用数量。一个文件可以为读或写或两者都打开。可读和可写字段跟踪这一点。

系统中所有打开的文件都保存在一个全局文件表中，即ftable。文件表有分配文件(filealloc)、创建重复引用(fileup)、释放引用(fileclose)、读写数据(fileeread和filewrite)等功能。

前三个遵循现在熟悉的形式。Filealloc(kernel/file.c:30)扫描文件表，寻找一个未引用的文件(f->ref == 0)，并返回一个新的引用；fileup(kernel/file.c:48)递增引用计数；fileclose(kernel/file.c:60)递减引用计数。当一个文件的引用数为零时，fileclose会根据类型释放底层管道或inode。

函数filestat、fileread和filewrite实现了对文件的统计、读和写操作。Filestat(kernel/file.c:88)只允许在inode上调用stati。Fileread和filewrite检查操作是否被打开模式允许，然后通过调用
通过管道或inode实现。如果文件代表一个inode，fileread和filewrite使用I/O偏移量作为操作的偏移量，然后将其推进(kernel/file.c:122-123)(kernel/file.c:153-154)。Pipes没有偏移量的概念。记得inode函数要求调用者处理锁定（kernel/file.c:94-96）（kernel/file.c:121-124）（kernel/file.c:163-166）。inode锁定有一个很方便的副作用，那就是读写偏移量是原子式更新的，这样多个同时向同一个文件写入的数据就不能覆盖对方的数据，虽然他们的写入最终可能会交错进行。

## 8.14 Code: System calls

With the functions that the lower layers provide the implementation of most system calls is trivial (see (kernel/sysfile.c)). There are a few calls that deserve a closer look.

The functions sys_link and sys_unlink edit directories, creating or removing references to inodes. They are another good example of the power of using transactions. Sys_link (kernel/sysfile.c:120) begins by fetching its arguments, two strings old and new (kernel/sysfile.c:125). Assuming old exists and is not a directory (kernel/sysfile.c:129-132), sys_link increments its ip->nlink count. Then sys_link calls nameiparent to find the parent directory and final path element of new (kernel/sysfile.c:145) and creates a new directory entry pointing at old ’s inode (kernel/sysfile.c:148). The new parent directory must exist and be on the same device as the existing inode: inode numbers only have a unique meaning on a single disk. If an error like this occurs, sys_link must go back and decrement ip->nlink.

Transactions simplify the implementation because it requires updating multiple disk blocks, but we don’t have to worry about the order in which we do them. They either will all succeed or none. For example, without transactions, updating ip->nlink before creating a link, would put the file system temporarily in an unsafe state, and a crash in between could result in havoc. With transactions we don’t have to worry about this.

Sys_link creates a new name for an existing inode. The function create (kernel/sysfile.c:242) creates a new name for a new inode. It is a generalization of the three file creation system calls: open with the O_CREATE flag makes a new ordinary file, mkdir makes a new directory, and mkdev makes a new device file. Like sys_link, create starts by caling nameiparent to get the inode of the parent directory. It then calls dirlookup to check whether the name already exists (kernel/sysfile.c:252). If the name does exist, create’s behavior depends on which system call it is being used for: open has different semantics from mkdir and mkdev. If create is being
used on behalf of open (type == T_FILE) and the name that exists is itself a regular file, then open treats that as a success, so create does too (kernel/sysfile.c:256). Otherwise, it is an error (kernel/sysfile.c:257-258). If the name does not already exist, create now allocates a new inode with ialloc (kernel/sysfile.c:261). If the new inode is a directory, create initializes it with . and .. entries. Finally, now that the data is initialized properly, create can link it into the parent directory (kernel/sysfile.c:274). Create, like sys_link, holds two inode locks simultaneously: ip and dp. There is no possibility of deadlock because the inode ip is freshly allocated: no other process in the system will hold ip ’s lock and then try to lock dp.

Using create, it is easy to implement sys_open, sys_mkdir, and sys_mknod. Sys_open (kernel/sysfile.c:287) is the most complex, because creating a new file is only a small part of what it can do. If open is passed the O_CREATE flag, it calls create (kernel/sysfile.c:301). Otherwise, it calls namei (kernel/sysfile.c:307). Create returns a locked inode, but namei does not, so sys_open must lock the inode itself. This provides a convenient place to check that directories are only opened for reading, not writing. Assuming the inode was obtained one way or the other, sys_open allocates a file and a file descriptor (kernel/sysfile.c:325) and then fills in the file (kernel/sysfile.c:337-342). Note that no other process can access the partially initialized file since it is only in the current process’s table.

Chapter 7 examined the implementation of pipes before we even had a file system. The function sys_pipe connects that implementation to the file system by providing a way to create a pipe pair.

Its argument is a pointer to space for two integers, where it will record the two new file descriptors. Then it allocates the pipe and installs the file descriptors.

有了下层提供的函数，大多数系统调用的实现都是微不足道的（见(kernel/sysfile.c)）。有一些调用值得仔细研究。

函数 sys_link 和 sys_unlink 编辑目录，创建或删除对 inodes 的引用。它们是使用事务的另一个很好的例子。Sys_link (kernel/sysfile.c:120) 首先获取它的参数，两个字符串 old 和 new (kernel/sysfile.c:125) 。假设 old 存在并且不是一个目录 (kernel/sysfile.c:129-132)，sys_link 会递增它的 ip->nlink 计数。然后 sys_link 调用 nameiparent 找到 new 的父目录和最终路径元素 (kernel/sysfile.c:145) 并创建一个指向 old 的 inode 的新目录 (kernel/sysfile.c:148)。新的父目录必须存在，并且和现有的inode在同一个设备上：inode号在一个磁盘上只有唯一的意义。如果出现这样的错误，sys_link必须返回并递减ip->nlink。

事务简化了实现，因为它需要更新多个磁盘块，但我们不必担心做这些事情的顺序。它们要么全部成功，要么都不成功。例如，如果没有事务，在创建链接之前更新ip->nlink，会使文件系统暂时处于不安全的状态，中间的崩溃可能会造成破坏。有了事务，我们就不用担心这个问题了。

Sys_link为一个现有的inode创建一个新的名字。函数create (kernel/sysfile.c:242)为一个新的inode创建一个新的名字。它是三个文件创建系统调用的概括：使用O_CREATE标志的open创建一个新的普通文件，mkdir创建一个新的目录，以及mkdev创建一个新的设备文件。和sys_link一样，create也是通过调用nameiparent来获取父目录的inode。然后调用 dirlookup 来检查名称是否已经存在 (kernel/sysfile.c:252)。如果名称确实存在，create的行为取决于它被用于哪个系统调用：open与mkdir和mkdev的语义不同。如果create被用于
(type == T_FILE)，并且存在的文件名本身就是一个普通的文件，那么open将其视为成功，所以create也会成功(kernel/sysfile.c:256)。否则，就是一个错误（kernel/sysfile.c:257-258）。如果这个名字还不存在，create就会用ialloc分配一个新的inode(kernel/sysfile.c:261)。如果新的inode是一个目录，create会用.和.条目初始化它。最后，现在数据已经被正确地初始化了，create可以把它链接到父目录中(kernel/sysfile.c:274)。和sys_link一样，create同时拥有两个inode锁：ip和dp。没有死锁的可能性，因为inode ip是新分配的：系统中没有其他进程会持有ip的锁，然后试图锁定dp。

使用create，很容易实现sys_open，sys_mkdir和sys_mknod。Sys_open (kernel/sysfile.c:287)是最复杂的，因为创建一个新文件只是它能做的一小部分。如果open被传递了O_CREATE标志，它就会调用create (kernel/sysfile.c:301)。否则，它会调用 namei (kernel/sysfile.c:307)。Create会返回一个锁定的inode，但namei不会，所以sys_open必须锁定inode本身。这提供了一个方便的地方来检查目录是否只被打开用于读，而不是写。假设inode是通过某种方式获得的，sys_open会分配一个文件和一个文件描述符(kernel/sysfile.c:325)，然后填入文件(kernel/sysfile.c:337-342)。注意，没有其他进程可以访问部分初始化的文件，因为它只在当前进程的表中。

第7章在我们还没有文件系统的时候就研究了管道的实现。函数sys_pipe通过提供一种创建管道对的方法将该实现与文件系统连接起来。

它的参数是一个指向两个整数空间的指针，它将在这里记录两个新的文件描述符。然后它分配管道并安装文件描述符。

## 8.15 Real world

The buffer cache in a real-world operating system is significantly more complex than xv6’s, but it serves the same two purposes: caching and synchronizing access to the disk. Xv6’s buffer cache, like V6’s, uses a simple least recently used (LRU) eviction policy; there are many more complex policies that can be implemented, each good for some workloads and not as good for others. A more efficient LRU cache would eliminate the linked list, instead using a hash table for lookups and a heap for LRU evictions. Modern buffer caches are typically integrated with the virtual memory system to support memory-mapped files.

Xv6’s logging system is inefficient. A commit cannot occur concurrently with file-system system calls. The system logs entire blocks, even if only a few bytes in a block are changed. It performs synchronous log writes, a block at a time, each of which is likely to require an entire disk rotation time. Real logging systems address all of these problems.

Logging is not the only way to provide crash recovery. Early file systems used a scavenger during reboot (for example, the UNIX fsck program) to examine every file and directory and the block and inode free lists, looking for and resolving inconsistencies. Scavenging can take hours for large file systems, and there are situations where it is not possible to resolve inconsistencies in a way that causes the original system calls to be atomic. Recovery from a log is much faster and causes system calls to be atomic in the face of crashes.

Xv6 uses the same basic on-disk layout of inodes and directories as early UNIX; this scheme has been remarkably persistent over the years. BSD’s UFS/FFS and Linux’s ext2/ext3 use essentially the same data structures. The most inefficient part of the file system layout is the directory, which requires a linear scan over all the disk blocks during each lookup. This is reasonable when directories are only a few disk blocks, but is expensive for directories holding many files. Microsoft Windows’s NTFS, Mac OS X’s HFS, and Solaris’s ZFS, just to name a few, implement a directory as an on-disk balanced tree of blocks. This is complicated but guarantees logarithmic-time directory lookups.

实际操作系统中的缓冲区缓存比xv6的要复杂得多，但它有同样的两个目的：缓存和同步访问磁盘。xv6的缓冲区缓存和V6的一样，使用简单的最近使用（LRU）驱逐策略；可以实现许多更复杂的策略，每种策略都对某些工作负载有好处，而对其他工作负载没有好处。更高效的LRU缓存将取消链接列表，而使用哈希表进行查找，使用堆进行LRU驱逐。现代的缓冲区缓存一般都是和虚拟内存系统集成在一起，支持内存映射文件。

Xv6的日志系统效率很低。一个提交不能与文件系统系统调用同时发生。系统会记录整个块，即使一个块中只有几个字节被改变。它执行同步的日志写入，一次写一个块，每一个块都可能需要整个磁盘旋转时间。真正的日志系统可以解决所有这些问题。

日志不是提供崩溃恢复的唯一方法。早期的文件系统在重启期间使用清道夫（例如UNIX fsck程序）来检查每个文件和目录以及块和inode空闲列表，寻找并解决不一致的问题。对于大型文件系统来说，清扫可能需要几个小时的时间，而且在某些情况下，不可能以导致原始系统调用原子化的方式来解决不一致的问题。从日志中恢复要快得多，并且在面对崩溃的情况下，会导致系统调用是原子的。

Xv6使用了与早期UNIX相同的inodes和目录的基本磁盘布局；这个方案多年来一直非常持久。BSD的UFS/FFS和Linux的ext2/ext3使用基本相同的数据结构。文件系统布局中最低效的部分是目录，在每次查找的过程中需要对所有的磁盘块进行线性扫描。当目录只有几个磁盘块时，这样做是合理的，但对于存放许多文件的目录来说，这样做是昂贵的。微软Windows的NTFS，Mac OS X的HFS，以及Solaris的ZFS等等，都是将目录实现为磁盘上平衡的块树。这很复杂，但可以保证对数时间的目录查找。

Xv6 is naive about disk failures: if a disk operation fails, xv6 panics. Whether this is reasonable depends on the hardware: if an operating systems sits atop special hardware that uses redundancy to mask disk failures, perhaps the operating system sees failures so infrequently that panicking is okay. On the other hand, operating systems using plain disks should expect failures and handle them more gracefully, so that the loss of a block in one file doesn’t affect the use of the rest of the file system.

Xv6 requires that the file system fit on one disk device and not change in size. As large databases and multimedia files drive storage requirements ever higher, operating systems are developing ways to eliminate the “one disk per file system” bottleneck. The basic approach is to combine many disks into a single logical disk. Hardware solutions such as RAID are still the most popular, but the current trend is moving toward implementing as much of this logic in software as possible. These software implementations typically allow rich functionality like growing or shrinking the logical device by adding or removing disks on the fly. Of course, a storage layer that can grow or shrink on the fly requires a file system that can do the same: the fixed-size array of inode blocks used by xv6 would not work well in such environments. Separating disk management from the file system may be the cleanest design, but the complex interface between the two has led some systems, like Sun’s ZFS, to combine them.

Xv6’s file system lacks many other features of modern file systems; for example, it lacks support for snapshots and incremental backup.

Modern Unix systems allow many kinds of resources to be accessed with the same system calls as on-disk storage: named pipes, network connections, remotely-accessed network file systems, and monitoring and control interfaces such as /proc. Instead of xv6’s if statements in fileread and filewrite, these systems typically give each open file a table of function pointers, one per operation, and call the function pointer to invoke that inode’s implementation of the call. Network file systems and user-level file systems provide functions that turn those calls into network RPCs and wait for the response before returning.

Xv6对磁盘故障很天真：如果磁盘操作失败，xv6就会惊慌失措。这是否合理取决于硬件：如果一个操作系统位于特殊的硬件之上，使用冗余来掩盖磁盘故障，也许操作系统看到故障的频率很低，以至于恐慌是可以的。另一方面，使用普通磁盘的操作系统应该期待故障，并更优雅地处理故障，这样一个文件中一个块的丢失就不会影响文件系统其他部分的使用。

Xv6要求文件系统要适合在一个磁盘设备上，并且大小不能改变。随着大型数据库和多媒体文件对存储的要求越来越高，操作系统正在开发消除 "一个文件系统一个磁盘 "瓶颈的方法。基本方法是将许多磁盘组合成一个逻辑磁盘。硬件解决方案（如RAID）仍然是最流行的，但目前的趋势是尽可能地在软件中实现这种逻辑。这些软件实现通常允许丰富的功能，如通过快速添加或删除磁盘来增长或缩小逻辑设备。当然，一个能够快速增长或收缩的存储层需要一个能够做到同样的文件系统：xv6使用的固定大小的inode块阵列在这样的环境中不能很好地工作。将磁盘管理与文件系统分离可能是最简洁的设计，但两者之间复杂的接口导致一些系统，如Sun的ZFS，将两者结合起来。

Xv6的文件系统缺乏现代文件系统的许多其他功能，例如，它缺乏对快照和增量备份的支持。

现代Unix系统允许用与磁盘存储相同的系统调用来访问许多种类的资源：命名管道、网络连接、远程访问的网络文件系统，以及监视和控制接口，如/proc。这些系统没有xv6在fileread和filewrite中的if语句，而是通常给每个打开的文件一个函数指针表，每个操作一个，调用函数指针来调用该inode的实现调用。网络文件系统和用户级文件系统提供了将这些调用变成网络RPC的函数，并等待响应后再返回。

## 8.16 Exercises

1- Why panic in balloc ? Can xv6 recover?

2- Why panic in ialloc ? Can xv6 recover?

3- Why doesn’t filealloc panic when it runs out of files? Why is this more common and therefore worth handling?

4- Suppose the file corresponding to ip gets unlinked by another process between sys_link ’s calls to iunlock(ip) and dirlink. Will the link be created correctly? Why or why not?

5- create makes four function calls (one to ialloc and three to dirlink) that it requires to succeed. If any doesn’t, create calls panic. Why is this acceptable? Why can’t any of those four calls fail?

6- sys_chdir calls iunlock(ip) before iput(cp->cwd), which might try to lock cp->cwd, yet postponing iunlock(ip) until after the iput would not cause deadlocks. Why not?

7- Implement the lseek system call. Supporting lseek will also require that you modify filewrite to fill holes in the file with zero if lseek sets off beyond f->ip->size.

8- Add O_TRUNC and O_APPEND to open, so that > and >> operators work in the shell.

9- Modify the file system to support symbolic links.

10- Modify the file system to support named pipes.

11- Modify the file and VM system to support memory-mapped files.

1-为什么在balloc中恐慌？xv6 能否恢复？

2- 为什么在ialloc中出现恐慌？xv6 可以恢复吗？

3- 为什么filealloc用完文件时不慌？为什么这种情况比较常见，因此值得处理？

4- 假设ip对应的文件在sys_link调用iunlock(ip)和dirlink之间被另一个进程解除了链接。链接会被正确创建吗？为什么会或为什么不会？

5- create调用了四个函数（一个调用ialloc，三个调用dirlink），它需要这些函数才能成功。如果任何一个不成功，create调用就会恐慌。为什么这可以接受？为什么这四个调用不能有一个失败？

6- sys_chdir在iput(cp->cwd)之前调用iunlock(ip)，可能会尝试锁定cp->cwd，然而将iunlock(ip)推迟到iput之后就不会造成死锁。为什么不这样做呢？

7-实现lseek系统调用。支持lseek还需要你修改filewrite，如果lseek设置超过f->ip->size，就用零来填补文件中的漏洞。

8-增加O_TRUNC和O_APPEND打开，使>和>>操作符在shell中工作。

9- 修改文件系统以支持符号链接。

10-修改文件系统，支持命名管道。

11-修改文件和虚拟机系统，以支持内存映射文件。