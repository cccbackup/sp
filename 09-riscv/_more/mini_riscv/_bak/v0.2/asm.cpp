#include "riscv.hpp"

// using namespace std;

map<string, int> symMap {};

Op opFind(char *op) {
  string op0(op);
  map<string, Op>::const_iterator pos = opMap.find(op0);
  if (pos == opMap.end()) return opNull;
  return pos->second;
}

int rFind(char *name) {
  string name0(name);
  map<string, int>::const_iterator pos = rMap.find(name0);
  if (pos == rMap.end()) return -1;
  return pos->second;
}

int symFind(char *name) {
  string name0(name);
  map<string, int>::const_iterator pos = symMap.find(name0);
  if (pos == symMap.end()) return -1;
  printf("symFind(%s)=%d\n", name, pos->second);
  return pos->second;
}

void symDump(map<string, int> *symMap) {
  printf("=================symMap===================\n");
  for(auto it = symMap->cbegin(); it != symMap->cend(); ++it) {
      cout << it->first << " " << it->second << "\n";
  }
}

int tokens;
char op[SMAX], p1[SMAX], p2[SMAX], p3[SMAX];
uint32_t rd, rs1, rs2, address;
int32_t imm;

char parse(char *pline) {
  printf("// %s", pline);
  char line[SMAX], *s = line;
  strcpy(line, pline);
  while(*s != '\0') {
    if (strchr(",()\t\n", *s)!=NULL) *s=' ';
    if (*s == '#') break;
    s++;
  }
  *s = '\0';
  op[0]='\0'; p1[0]='\0'; p2[0]='\0'; p3[0]='\0';
  sscanf(line, "%s", op);
  if (strchr(op, ':') > 0) return ':';
  if (op[0] == '.') return '.';
  Op o = opFind(op);
  sscanf(line, "%s %s %s %s", op, p1, p2, p3);
  // printf("// type=%c op=%s p1=%s p2=%s p3=%s\n", o.type, op, p1, p2, p3);
  return o.type;
}

void pass1(char *inFile) {
  printf("============= PASS1 ================\n");
  char line[100]="";
  FILE *fp = fopen(inFile, "r");
  address = 0;
  while (fgets(line, sizeof(line), fp)) {
    char type = parse(line);
    if (type==':') {
      char label[SMAX];
      sscanf(op, "%[^:]", label);
      printf("label=%s\n", label);
      string slabel(label);
      symMap[slabel] = address;
    } else if (strchr("RISBUJ", type)!=NULL) {
      address += 4;
    }
  }
  fclose(fp);
}

uint32_t format(Op *o) {
  uint32_t f7 = o->f7, f3=o->f3, op=o->op;
  switch (o->type) {
    case 'R':
      return (f7<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (rd<<7) | (op);
    case 'I':
      return (imm<<20) | (rs1<<15) | (f3<<12) | (rd<<7) | (op);
    case 'S':
      return (BITS(imm, 5, 11)<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (BITS(imm, 0, 4)<<7) | (op);
    case 'B':
      return (BITS(imm, 12, 12)<<31) | (BITS(imm, 5, 10)<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (BITS(imm, 1, 4)<<8) | (BITS(imm, 11, 11)<<7) | (op);
    case 'U':
      return (BITS(imm, 12, 31)<<12) | (rd<<7) | (op);
    case 'J':
      return (BITS(imm, 20, 20)<<31) | (BITS(imm, 1, 10)<<21) | (BITS(imm, 11, 11)<<20) | (BITS(imm, 12,19)<<12) | (rd<<7) | (op);
    default:
      printf("format: error! type=%c\n", o->type);
  }
}

int arg2i(char *arg) {
  // printf("arg2i: arg=%s\n", arg);
  if (strchr("0123456789-", arg[0])!=NULL) {
    return atoi(arg);
  } else {
    return symFind(arg);
  }
}

uint32_t gen() {
  Op o = opFind(op); // opMap[op0];
  rd = 0; rs1 = 0; rs2 = 0; imm = -1;
  switch (o.type) {
    case 'R': // ex: add rd, rs1, rs2
      rd=rFind(p1); rs1=rFind(p2); rs2=rFind(p3);
      break;
    case 'I':
      if (isalpha(p2[0])) { // ex: addi rd, rs1, imm; 
        rd=rFind(p1); rs1=rFind(p2); imm=arg2i(p3);
      } else { // ex: lw	a5,-40(s0)
        rd=rFind(p1); imm=arg2i(p2); rs1=rFind(p3);
      }
      break;
    case 'S': // ex: SB rs1, imm(rs2)
      rs1=rFind(p1); imm=arg2i(p2); rs2=rFind(p3);
      break;
    case 'B': // ex: BEQ rs1, rs2, imm
      rs1=rFind(p1); rs2=rFind(p2); imm=arg2i(p3);
      break;
    case 'U': // ex: LUI rd, imm
      rd=rFind(p1); imm=arg2i(p2);
      break;
    case 'J': // ex: JAL rd, imm
      rd=rFind(p1); imm=arg2i(p2);
      break;
    default:
      printf("format: error! type=%c\n", o.type);
  }
  // printf("gen %c:op=%d rd=%d rs1=%d rs2=%d imm=%d\n", o.type, o.op, rd, rs1, rs2, imm);
  // printf("gen: %s:op=%02X f3=%01X f7=%02X\n", op, o.op, o.f3, o.f7);
  uint32_t b = format(&o);
  printf("%04X:%08X %c %02X %01X %02X r:%02d %02d %02d imm:%d\n", address, b, o.type, o.op, o.f3, o.f7, rd, rs1, rs2, imm);
  return b;
}

void pass2(char* inFile, char* binFile) {
  printf("============= PASS2 ================\n");
  char line[SMAX], binary[17];
  FILE *fp = fopen(inFile, "r"); // 開啟組合語言檔
  FILE *bfp = fopen(binFile, "wb"); // 開啟輸出的 .bin 二進位檔
  address = 0;
  while (fgets(line, sizeof(line), fp)) { // 一行一行讀
    char type = parse(line);
    if (strchr("RISBUJ", type)!=NULL) {
      // printf("%02d:op=%s p1=%s p2=%s p3=%s\n", address, op, p1, p2, p3);
      uint32_t b = gen();
      fwrite(&b, sizeof(b), 1, bfp); // 輸出 .bin 的二進位檔
      // printf("%02X:%08X\n", address, b);
      address += 4;
    }
  }
  fclose(fp);
  fclose(bfp);
}

void assemble(char *file) {
  char inFile[100], hackFile[100], binFile[100];
  sprintf(inFile, "%s.s", file);
  sprintf(binFile, "%s.bin", file);
  // symDump(&symMap);
  pass1(inFile);
  // symDump(&symMap);
  pass2(inFile, binFile);
}

// run: ./asm <file>
// notice : <file> with no extension.
int main(int argc, char *argv[]) {
  assemble(argv[1]);
}
