#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];

void dumpInstr(uint32_t I) {
  char sasm[SMAX];
  imm = 0;
  uint32_t t;
  switch (type) {
    case 'R': sprintf(sasm, "%s x%d,x%d,x%d", name, rd, rs1, rs2); break;
    case 'I': t=BITS(I, 20, 31); imm = SGN(t, 11); sprintf(sasm, "%s x%d,x%d,%d", name, rd, rs1, imm); break;
    case 'S': t=BITS(I, 25, 31)<<5|BITS(I, 7, 11); imm=SGN(t,11); sprintf(sasm, "%s x%d,%d(x%d)", name, rs1, imm, rs2); break;
    case 'B': t=BITS(I, 31, 31)<<12|BITS(I,7,7)<<11|BITS(I,25,30)<<5|BITS(I,8,11)<<1; imm=SGN(t, 12); sprintf(sasm, "%s x%d,x%d,%d", name, rs1, rs2, imm); break;
    case 'U': imm=(int32_t)(BITS(I, 12, 31)<<12); sprintf(sasm, "%s x%d,%d", name, rd, imm); break;
    case 'J': t=BITS(I, 31, 31)<<20|BITS(I,21,30)<<1|BITS(I,20,20)<<11|BITS(I,12,19)<<12; imm=SGN(t,20); sprintf(sasm, "%s %d(x%d)", name, imm, rd); break;
    default: sprintf(sasm, "Instruction error !");
  }
  printf("%-20s %04X:%08X %c %02X  %01X %02X %02X  %02X  %02X\n", sasm, PC, I, type, op, f3, f7, rd, rs1, rs2);
}

void vm(uint8_t *m, uint32_t mTop) {
  PC = 0;
  while (1) {
    I = *(uint32_t*)&m[PC];
    Op *o = decode(I);
    dumpInstr(I);

    int isJump = 0;
    if (o==&oSll) R[rd]=(uint32_t)R[rs1]<<R[rs2];
    else if (o==&oSrl) R[rd]=(uint32_t)R[rs1]>>R[rs2];
    else if (o==&oSra) R[rd]=R[rs1]>>R[rs2];
    else if (o==&oAdd) R[rd]=R[rs1]+R[rs2];
    else if (o==&oSub) R[rd]=R[rs1]-R[rs2];
    else if (o==&oXor) R[rd]=R[rs1]^R[rs2];
    else if (o==&oAnd) R[rd]=R[rs1]&R[rs2];
    else if (o==&oSlli) R[rd]=(uint32_t)R[rs1]<<imm;
    else if (o==&oSrli) R[rd]=(uint32_t)R[rs1]>>imm;
    else if (o==&oSrai) R[rd]=R[rs1]>>imm;
    else if (o==&oAddi) R[rd]=R[rs1]+imm;
    else if (o==&oXori) R[rd]=R[rs1]^imm;
    else if (o==&oAndi) R[rd]=R[rs1]&imm;
    else if (o==&oSlt) R[rd] = (R[rs1] < R[rs2]);
    else if (o==&oSlti) R[rd] = (R[rs1] < imm);
    else if (o==&oSltu) R[rd] = ((uint32_t)R[rs1] < (uint32_t)R[rs2]);
    else if (o==&oSltiu) R[rd] = ((uint32_t)R[rs1] < (uint32_t)imm);
    else if (o==&oLb) R[rd]=*(int8_t*)&m[R[rs1]+imm];
    else if (o==&oLbu) R[rd]=*(uint8_t*)&m[R[rs1]+imm];
    else if (o==&oLh) R[rd]=*(int16_t*)&m[R[rs1]+imm];
    else if (o==&oLhu) R[rd]=*(uint16_t*)&m[R[rs1]+imm];
    else if (o==&oLw) R[rd]=*(int32_t*)&m[R[rs1]+imm];
    else if (o==&oSb) *(int8_t*)&m[R[rs1]+imm]=R[rd];
    else if (o==&oSh) *(int16_t*)&m[R[rs1]+imm]=R[rd];
    else if (o==&oSw) *(int32_t*)&m[R[rs1]+imm]=R[rd];
    else if (o==&oLui) R[rd]=imm<<12;
    else if (o==&oAuipc) R[rd]=PC+(imm<<12);
    else {
      isJump = 1;
      if (o==&oJal) { R[rd]=PC; PC+= imm; }
      else if (o==&oJalr) { int32_t t=PC+4; PC=R[rs1]+imm; R[rd] = t; } // 保存返回位址然後跳到 R[rs1]+imm
      else if (o==&oBeq) { if (R[rs1] == R[rs2]) PC+=imm; else PC+=4; } // 疑問：跳躍位址應該是 pc加上 sign-extended 13-bit，而非直接設定。
      else if (o==&oBlt) { if (R[rs1] <  R[rs2]) PC+=imm; else PC+=4; }
      else if (o==&oBne) { if (R[rs1] != R[rs2]) PC+=imm; else PC+=4; }
      else if (o==&oBge) { if (R[rs1] >= R[rs2]) PC+=imm; else PC+=4; }
      else if (o==&oBltu) { if ((uint32_t)R[rs1] <  (uint32_t)R[rs2]) PC += imm; else PC+=4; }
      else if (o==&oBgeu) { if ((uint32_t)R[rs1] >= (uint32_t)R[rs2]) PC += imm; else PC+=4; }
      else printf("command not found!\n");
    }
    printf("R[%d]=%d\n", rd, R[rd]);
    if (!isJump) PC += 4;
    printf("PC=%d mTop=%d\n", PC, mTop);
    if (PC >= mTop) break;
  }
}

/*
Op oFence = {"fence", 'I',0x0F,0x0};
Op oFenceI = {"fence.i", 'I',0x0F,0x1};
Op oEcall = {"ecall", 'I',0x73};
Op oEbreak = {"ebreak", 'I',0x73};
Op oCsrrw = {"csrrw", 'I',0x73,0x1};
Op oCsrrs = {"csrrs", 'I',0x73,0x2};
Op oCsrrc = {"csrrc", 'I',0x73,0x3};
Op oCsrrwi = {"csrrwi", 'I',0x73,0x5}; // 格式 I 是把 rs1 改為 uimm
Op oCsrrsi = {"csrrsi", 'I',0x73,0x6};
Op oCsrrci = {"csrrci", 'I',0x73,0x7};
*/
// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  vm(m, mTop);
  return 0;
}
