#include <stdio.h>
#include <string.h>
#include <iostream> 
#include <map> 
#define SMAX 100
#define TMAX 10000

#define BITS(x, from, to) (((x)<<(32-(to+1)))>>(32-((to)-(from)+1))) // 包含 to, 用 BITS(x, 1, 30) 去思考

using namespace std;

typedef struct _Op {
  char type;
  uint32_t op, f3, f7;
} Op;

Op opNull = { '?', 0, 0, 0 };

map<string, Op> opMap {
  {"sll", Op{'R',0x33,0x1,0x0}}, 
  {"slli",Op{'I',0x13,0x1,0x0}}, 
  {"srl", Op{'R',0x33,0x5,0x0}}, 
  {"srli",Op{'I',0x13,0x5,0x0}}, 
  {"sra", Op{'R',0x33,0x5,0x20}}, 
  {"srai",Op{'I',0x13,0x5,0x20}}, 
  {"add", Op{'R',0x33,0x0,0x0}}, 
  {"addi",Op{'I',0x13,0x0,0x0}}, 
  {"sub", Op{'R',0x33,0x0,0x20}}, 
  {"lui", Op{'U',0x37}}, 
  {"auipc", Op{'U',0x17}}, 
  {"xor", Op{'R',0x33,0x4}}, 
  {"xori", Op{'I',0x13,0x4}}, 
  {"or", Op{'R',0x33,0x6}}, 
  {"ori", Op{'I',0x13,0x6}},
  {"and", Op{'R',0x33,0x7}},
  {"andi", Op{'I',0x13,0x7}},
  {"slt", Op{'R',0x33,0x2}},
  {"slti", Op{'I',0x13,0x2}},
  {"sltu", Op{'R',0x33,0x3}},
  {"sltiu", Op{'I',0x13,0x3}},
  {"beq", Op{'B',0x63,0x0}},
  {"bne", Op{'B',0x63,0x1}},
  {"blt", Op{'B',0x63,0x4}},
  {"bge", Op{'B',0x63,0x5}},
  {"bltu", Op{'B',0x63,0x6}},
  {"bgeu", Op{'B',0x63,0x7}},
  {"jal", Op{'J',0x6F}},
  {"jalr", Op{'I',0x67}},
  {"fence", Op{'I',0x0F,0x0}},
  {"fence.i", Op{'I',0x0F,0x1}},
  {"ecall", Op{'I',0x73}},
  {"ebreak", Op{'I',0x73}},
  {"csrrw", Op{'I',0x73,0x1}},
  {"csrrs", Op{'I',0x73,0x2}},
  {"csrrc", Op{'I',0x73,0x3}},
  {"csrrwi", Op{'I',0x73,0x5}},
  {"csrrsi", Op{'I',0x73,0x6}},
  {"csrrci", Op{'I',0x73,0x7}},
  {"lb", Op{'I',0x03,0x0}},
  {"lh", Op{'I',0x03,0x1}},
  {"lw", Op{'I',0x03,0x2}},
  {"lbu", Op{'I',0x03,0x4}},
  {"lhu", Op{'I',0x03,0x5}},
  {"sb", Op{'S',0x23,0x0}},
  {"sh", Op{'S',0x23,0x1}},
  {"sw", Op{'S',0x23,0x2}},
};

map<string, int> rMap {
  // 暫存器 x0-x31
  {"x0",0},{"x1",1},{"x2",2},{"x3",3},{"x4",4},{"x5",5},{"x6",6},{"x7",7},
  {"x8",8}, {"x9",9}, {"x10",10}, {"x11",11},{"x12",12}, {"x13",13}, {"x14",14}, {"x15",15},
  {"x16",16}, {"x17",17}, {"x18",18}, {"x19",19},{"x20",20}, {"x21",21}, {"x22",22}, {"x23",23},
  {"x24",24}, {"x25",25}, {"x26",26}, {"x27",27},{"x28",28}, {"x29",29}, {"x30",30}, {"x31",31},
  // 暫存器別名
  {"zero",0},{"ra",1},{"sp",2},{"gp",3},{"tp",4},{"t0",5},{"t1",6},{"t2",7},
  {"s0",8}, {"fp",8}, {"s1",9}, {"a0",10}, {"a1",11},{"a2",12}, {"a3",13}, {"a4",14}, {"a5",15},
  {"a6",16}, {"a7",17}, {"s2",18}, {"s3",19},{"s4",20}, {"s5",21}, {"s6",22}, {"s7",23},
  {"s8",24}, {"s9",25}, {"s10",26}, {"s11",27},{"t3",28}, {"t4",29}, {"t5",30}, {"t6",31}
};

map<string, int> symMap {};

Op opFind(char *op) {
  string op0(op);
  map<string, Op>::const_iterator pos = opMap.find(op0);
  if (pos == opMap.end()) return opNull;
  return pos->second;
}

int rFind(char *name) {
  string name0(name);
  map<string, int>::const_iterator pos = rMap.find(name0);
  if (pos == rMap.end()) return -1;
  return pos->second;
}

int symFind(char *name) {
  string name0(name);
  map<string, int>::const_iterator pos = symMap.find(name0);
  if (pos == symMap.end()) return -1;
  printf("symFind(%s)=%d\n", name, pos->second);
  return pos->second;
}

void symDump(map<string, int> *symMap) {
  printf("=================symMap===================\n");
  for(auto it = symMap->cbegin(); it != symMap->cend(); ++it) {
      cout << it->first << " " << it->second << "\n";
  }
}

int tokens;
char op[SMAX], p1[SMAX], p2[SMAX], p3[SMAX];
uint32_t rd, rs1, rs2;
int32_t imm;

char parse(char *pline) {
  printf("// %s", pline);
  char line[SMAX], *s = line;
  strcpy(line, pline);
  while(*s != '\0') {
    if (strchr(",()\t\n", *s)!=NULL) *s=' ';
    if (*s == '#') break;
    s++;
  }
  *s = '\0';
  op[0]='\0'; p1[0]='\0'; p2[0]='\0'; p3[0]='\0';
  sscanf(line, "%s", op);
  if (strchr(op, ':') > 0) return ':';
  if (op[0] == '.') return '.';
  Op o = opFind(op);
  sscanf(line, "%s %s %s %s", op, p1, p2, p3);
  printf("// type=%c op=%s p1=%s p2=%s p3=%s\n", o.type, op, p1, p2, p3);
  return o.type;
}

void pass1(char *inFile) {
  printf("============= PASS1 ================\n");
  char line[100]="";
  FILE *fp = fopen(inFile, "r");
  int address = 0;
  while (fgets(line, sizeof(line), fp)) {
    char type = parse(line);
    if (type==':') {
      char label[SMAX];
      sscanf(op, "%[^:]", label);
      printf("label=%s\n", label);
      string slabel(label);
      symMap[slabel] = address;
    } else if (strchr("RISBUJ", type)!=NULL) {
      address += 4;
    }
  }
  fclose(fp);
}

uint32_t format(Op *o) {
  uint32_t f7 = o->f7, f3=o->f3, op=o->op;
  switch (o->type) {
    case 'R':
      return (f7<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (rd<<7) | (op);
    case 'I':
      return (imm<<20) | (rs1<<15) | (f3<<12) | (rd<<7) | (op);
    case 'S':
      return (BITS(imm, 5, 11)<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (BITS(imm, 0, 4)<<7) | (op);
    case 'B':
      return (BITS(imm, 12, 12)<<31) | (BITS(imm, 5, 10)<<25) | (rs2<<20) | (rs1<<15) | (f3<<12) | (BITS(imm, 1, 4)<<8) | (BITS(imm, 11, 11)<<7) | (op);
    case 'U':
      return (BITS(imm, 12, 31)<<12) | (rd<<7) | (op);
    case 'J':
      return (BITS(imm, 20, 20)<<31) | (BITS(imm, 1, 10)<<21) | (BITS(imm, 11, 11)<<20) | (BITS(imm, 12,19)<<12) | (rd<<7) | (op);
    default:
      printf("format: error! type=%c\n", o->type);
  }
}

int arg2i(char *arg) {
  printf("arg2i: arg=%s\n", arg);
  if (strchr("0123456789-", arg[0])!=NULL) {
    return atoi(arg);
  } else {
    return symFind(arg);
  }
}

uint32_t gen() {
  Op o = opFind(op); // opMap[op0];
  rd = -1; rs1 = -1; rs2 = -1; imm = -1;
  switch (o.type) {
    case 'R': // ex: add rd, rs1, rs2
      rd=rFind(p1); rs1=rFind(p2); rs2=rFind(p3);
      break;
    case 'I':
      if (isalpha(p2[0])) { // ex: addi rd, rs1, imm; 
        rd=rFind(p1); rs1=rFind(p2); imm=arg2i(p3);
      } else { // ex: lw	a5,-40(s0)
        rd=rFind(p1); imm=arg2i(p2); rs1=rFind(p3);
      }
      break;
    case 'S': // ex: SB rs1, imm(rs2)
      rs1=rFind(p1); imm=arg2i(p2); rs2=rFind(p3);
      break;
    case 'B': // ex: BEQ rs1, rs2, imm
      rs1=rFind(p1); rs2=rFind(p2); imm=arg2i(p3);
      break;
    case 'U': // ex: LUI rd, imm
      rd=rFind(p1); imm=arg2i(p2);
      break;
    case 'J': // ex: JAL rd, imm
      rd=rFind(p1); imm=arg2i(p2);
      break;
    default:
      printf("format: error! type=%c\n", o.type);
  }
  printf("gen %c:op=%d rd=%d rs1=%d rs2=%d imm=%d\n", o.type, o.op, rd, rs1, rs2, imm);
  // printf("gen: %s:op=%02X f3=%01X f7=%02X\n", op, o.op, o.f3, o.f7);
  return format(&o);
}

void pass2(char* inFile, char* binFile) {
  printf("============= PASS2 ================\n");
  char line[SMAX], binary[17];
  FILE *fp = fopen(inFile, "r"); // 開啟組合語言檔
  FILE *bfp = fopen(binFile, "wb"); // 開啟輸出的 .bin 二進位檔
  int address = 0;
  while (fgets(line, sizeof(line), fp)) { // 一行一行讀
    char type = parse(line);
    if (strchr("RISBUJ", type)!=NULL) {
      // printf("%02d:op=%s p1=%s p2=%s p3=%s\n", address, op, p1, p2, p3);
      uint32_t code = gen();
      printf("%02X:%08X\n", address, code);
      address += 4;
    }
  }
  fclose(fp);
  fclose(bfp);
}

void assemble(char *file) {
  char inFile[100], hackFile[100], binFile[100];
  sprintf(inFile, "%s.s", file);
  sprintf(binFile, "%s.bin", file);
  // symDump(&symMap);
  pass1(inFile);
  // symDump(&symMap);
  pass2(inFile, binFile);
}

// run: ./asm <file>
// notice : <file> with no extension.
int main(int argc, char *argv[]) {
  assemble(argv[1]);
}


/* 指令型態
  | R, I, S, B, U, J
--|--------------------------------------------
R | f7 rs2 rs1 f3 rd op
I |   i12  rs1 f3 rd op
S | i7 rs2 rs1 f3 i5 op  i7=i[11:5] i5=i[4:0]
B | i7 rs2 rs1 f3 i5 op  i7=i[12|10:5] i5=i[4:1|11]
U |   i20         rd op  i20=i[31:12]
J |   i20         rd op  i20=i[20|10:1|11|19:12]
*/

/* 指令表
U: lui rd,imm`: `rd = imm * 2^12; pc = pc + 4` with `-2^19 <= imm < 2^19`
I: addi rd,rs1,imm`: `rd = rs1 + imm; pc = pc + 4` with `-2^11 <= imm < 2^11`
I: lw?? ld rd,imm(rs1)`: `rd = memory[rs1 + imm]; pc = pc + 4` with `-2^11 <= imm < 2^11`
I: sw?? sd rs2,imm(rs1)`: `memory[rs1 + imm] = rs2; pc = pc + 4` with `-2^11 <= imm < 2^11`
R: add rd,rs1,rs2`: `rd = rs1 + rs2; pc = pc + 4`   op:0110011 f7:0000000 
R: sub rd,rs1,rs2`: `rd = rs1 - rs2; pc = pc + 4`   op:0110011 f7:0100000
R: mul rd,rs1,rs2`: `rd = rs1 * rs2; pc = pc + 4`
R: divu rd,rs1,rs2`: `rd = rs1 / rs2; pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
R: remu rd,rs1,rs2`: `rd = rs1 % rs2; pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
R: sltu rd,rs1,rs2`: `if (rs1 < rs2) { rd = 1 } else { rd = 0 } pc = pc + 4` where `rs1` and `rs2` are unsigned integers.
B: beq rs1,rs2,imm`: `if (rs1 == rs2) { pc = pc + imm } else { pc = pc + 4 }` with `-2^12 <= imm < 2^12` and `imm % 2 == 0`
J: jal rd,imm`: `rd = pc + 4; pc = pc + imm` with `-2^20 <= imm < 2^20` and `imm % 2 == 0`
I: jalr rd,imm(rs1)`: `tmp = ((rs1 + imm) / 2) * 2; rd = pc + 4; pc = tmp` with `-2^11 <= imm < 2^11`
I: ecall`: system call number is in `a7`, parameters are in `a0-a2`, return value is in `a0`.
*/
