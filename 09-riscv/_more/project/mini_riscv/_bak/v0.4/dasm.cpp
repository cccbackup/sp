#include "riscv.hpp"

#define MMAX 32768
uint8_t m[MMAX];
uint32_t PC, I, f3, f7, op, rd, rs1, rs2, mTop;
int32_t imm;
char type;
const char *name;

Op *decode(uint32_t I) {
  Op *o = NULL;
  op=BITS(I, 0, 6); 
  f3=BITS(I, 12, 14);
  f7=BITS(I, 25, 31);
  rd=BITS(I, 7, 11);
  rs1=BITS(I, 15, 19);
  rs2=BITS(I, 20, 24);
  switch (op) {
    case 0x33:
      switch (f3) {
        case 0: o = (f7==0)? &oAdd : &oSub; break;
        case 1: o = &oSll; break;
        case 2: o = &oSlt; break;
        case 3: o = &oSltu; break;
        case 4: o = &oXor; break;
        case 5: o = (f7==0)? &oSrl : &oSra; break;
        case 6: o = &oOr; break;
        case 7: o = &oAnd; break;
      }
      break;
    case 0x13:
      switch (f3) {
        case 0: o = &oAddi; break;
        case 1: o = &oSlli; break;
        case 2: o = &oSlti; break;
        case 3: o = &oSltiu; break;
        case 4: o = &oXori; break;
        case 5: o = (f7==0)? &oSrli : &oSrai; break;
        case 6: o = &oOri; break;
        case 7: o = &oAndi; break;
      }
      break;
    case 0x23:
      switch (f3) {
        case 0: o = &oSb; break;
        case 1: o = &oSh; break;
        case 2: o = &oSw; break;
      }
      break;
    case 0x03:
      switch (f3) {
        case 0: o = &oLb; break;
        case 1: o = &oLh; break;
        case 2: o = &oLw; break;
        case 4: o = &oLbu; break;
        case 5: o = &oLhu; break;
      }
      break;
    case 0x0F:
      switch (f3) {
        case 0: o = &oFence; break;
        case 1: o = &oFenceI; break;
      }
      break;
    case 0x63:
      switch (f3) {
        case 0: o = &oBeq; break;
        case 1: o = &oBne; break;
        case 4: o = &oBlt; break;
        case 5: o = &oBge; break;
        case 6: o = &oBltu; break;
        case 7: o = &oBgeu; break;
      }
      break;
    case 0x67:
      o = &oJalr; break;
    case 0x6F:
      o = &oJal; break;
    case 0x74:
      switch (f3) {
        case 1: o = &oCsrrw; break;
        case 2: o = &oCsrrs; break;
        case 3: o = &oCsrrc; break;
        case 5: o = &oCsrrwi; break;
        case 6: o = &oCsrrsi; break;
        case 7: o = &oCsrrci; break;
      }
      break;
  }
  if (o) {
    name = o->name.c_str();
    type = o->type;
  } else {
    type = '?';
  }
  return o;
}

void disasm(uint8_t *m, uint32_t mTop) {
  printf("asm                  addr:code     T op f3 f7 rd rs1 rs2\n---------------------------------------------------------\n");
  for (PC = 0; PC<mTop; PC+=4) {
    I = *(uint32_t*)&m[PC];
    Op *o = decode(I);
    char sasm[SMAX];
    imm = 0;
    uint32_t t;
    switch (type) {
      case 'R': sprintf(sasm, "%s x%d,x%d,x%d", name, rd, rs1, rs2); break;
      case 'I': t=BITS(I, 20, 31); imm = SGN(t, 11); sprintf(sasm, "%s x%d,x%d,%d", name, rd, rs1, imm); break;
      case 'S': t=BITS(I, 25, 31)<<5|BITS(I, 7, 11); imm=SGN(t,11); sprintf(sasm, "%s x%d,%d(x%d)", name, rs1, imm, rs2); break;
      case 'B': t=BITS(I, 31, 31)<<12|BITS(I,7,7)<<11|BITS(I,25,30)<<5|BITS(I,8,11)<<1; imm=SGN(t, 12); sprintf(sasm, "%s x%d,x%d,%d", name, rs1, rs2, imm); break;
      case 'U': imm=(int32_t)(BITS(I, 12, 31)<<12); sprintf(sasm, "%s x%d,%d", name, rd, imm); break;
      case 'J': t=BITS(I, 31, 31)<<20|BITS(I,21,30)<<1|BITS(I,20,20)<<11|BITS(I,12,19)<<12; imm=SGN(t,20); sprintf(sasm, "%s %d(x%d)", name, imm, rd); break;
      default: sprintf(sasm, "Instruction error !");
    }
    printf("%-20s %04X:%08X %c %02X  %01X %02X %02X  %02X  %02X\n", sasm, PC, I, type, op, f3, f7, rd, rs1, rs2);
  }
}

// run: ./disasm <file.bin>
int main(int argc, char *argv[]) {
  char *binFileName = argv[1];
  FILE *binFile = fopen(binFileName, "rb");
  mTop = fread(m, sizeof(uint8_t), MMAX, binFile);
  fclose(binFile);
  disasm(m, mTop);
  return 0;
}
