	.file	"gcd.c"
	.text
	.align	2
op2:
	add t1, t2, t3
	sub t2, t1, t4
	addi	sp,sp,-48
	sw	ra,44(sp)
.L2:
	sw	s0,40(sp)
	lw	a5,-40(s0)
	bne	a4,a5,.L2
	lw	a5,-36(s0)
	sw	a5,-20(s0)
	j	.L3
	beq	a4,a5,.L4
	ble	a4,a5,.-5
.L4:
	call	gcd
	jr	ra
	.size	main, .-main
	.ident	"GCC: (GNU) 7.2.0"
