#include "riscv.hpp"

uint32_t x[32];
float f[32];
uint8_t m[MMAX];
uint32_t pc, ir, f3, f7, op, rd, rs1, rs2, mTop;
int32_t imm;
char type;
const char *name;

Op opFind(char *name) {
  string op0(name);
  map<string, Op>::const_iterator pos = opMap.find(op0);
  if (pos == opMap.end()) return oNull;
  return pos->second;
}

int rFind(char *name) { return mapFind(&rMap, name); }

Op *decode(uint32_t ir) {
  Op *o = NULL;
  op=BITS(ir, 0, 6); 
  f3=BITS(ir, 12, 14);
  f7=BITS(ir, 25, 31);
  rd=BITS(ir, 7, 11);
  rs1=BITS(ir, 15, 19);
  rs2=BITS(ir, 20, 24);
  switch (op) {
    case 0x33:
      switch (f3) {
        case 0: o = (f7==0)? &oAdd : &oSub; break;
        case 1: o = &oSll; break;
        case 2: o = &oSlt; break;
        case 3: o = &oSltu; break;
        case 4: o = &oXor; break;
        case 5: o = (f7==0)? &oSrl : &oSra; break;
        case 6: o = &oOr; break;
        case 7: o = &oAnd; break;
      }
      break;
    case 0x13:
      switch (f3) {
        case 0: o = &oAddi; break;
        case 1: o = &oSlli; break;
        case 2: o = &oSlti; break;
        case 3: o = &oSltiu; break;
        case 4: o = &oXori; break;
        case 5: o = (f7==0)? &oSrli : &oSrai; break;
        case 6: o = &oOri; break;
        case 7: o = &oAndi; break;
      }
      break;
    case 0x23:
      switch (f3) {
        case 0: o = &oSb; break;
        case 1: o = &oSh; break;
        case 2: o = &oSw; break;
      }
      break;
    case 0x03:
      switch (f3) {
        case 0: o = &oLb; break;
        case 1: o = &oLh; break;
        case 2: o = &oLw; break;
        case 4: o = &oLbu; break;
        case 5: o = &oLhu; break;
      }
      break;
    case 0x0F:
      switch (f3) {
        case 0: o = &oFence; break;
        case 1: o = &oFenceI; break;
      }
      break;
    case 0x63:
      switch (f3) {
        case 0: o = &oBeq; break;
        case 1: o = &oBne; break;
        case 4: o = &oBlt; break;
        case 5: o = &oBge; break;
        case 6: o = &oBltu; break;
        case 7: o = &oBgeu; break;
      }
      break;
    case 0x67:
      o = &oJalr; break;
    case 0x6F:
      o = &oJal; break;
    case 0x74:
      switch (f3) {
        case 1: o = &oCsrrw; break;
        case 2: o = &oCsrrs; break;
        case 3: o = &oCsrrc; break;
        case 5: o = &oCsrrwi; break;
        case 6: o = &oCsrrsi; break;
        case 7: o = &oCsrrci; break;
      }
      break;
  }
  if (o) {
    name = o->name.c_str();
    type = o->type;
  } else {
    type = '?';
  }
  return o;
}

void dumpInstr(uint32_t ir) {
  char sasm[SMAX];
  imm = 0;
  uint32_t t;
  switch (type) {
    case 'R': sprintf(sasm, "%s x%d,x%d,x%d", name, rd, rs1, rs2); break;
    case 'I': t=BITS(ir, 20, 31); imm = SGN(t, 11); sprintf(sasm, "%s x%d,x%d,%d", name, rd, rs1, imm); break;
    case 'S': t=BITS(ir, 25, 31)<<5|BITS(ir, 7, 11); imm=SGN(t,11); sprintf(sasm, "%s x%d,%d(x%d)", name, rs1, imm, rs2); break;
    case 'B': t=BITS(ir, 31, 31)<<12|BITS(ir,7,7)<<11|BITS(ir,25,30)<<5|BITS(ir,8,11)<<1; imm=SGN(t, 12); sprintf(sasm, "%s x%d,x%d,%d", name, rs1, rs2, imm); break;
    case 'U': imm=(int32_t)(BITS(ir, 12, 31)<<12); sprintf(sasm, "%s x%d,%d", name, rd, imm); break;
    case 'J': t=BITS(ir, 31, 31)<<20|BITS(ir,21,30)<<1|BITS(ir,20,20)<<11|BITS(ir,12,19)<<12; imm=SGN(t,20); sprintf(sasm, "%s %d(x%d)", name, imm, rd); break;
    default: sprintf(sasm, "Instruction error !");
  }
  printf("%04X:%-20s %08X %c %02X  %01X %02X %02X  %02X  %02X\n", pc, sasm, ir, type, op, f3, f7, rd, rs1, rs2);
}

// Convert i32 to Little Endian
void wordToBytes(uint32_t word, uint8_t bytes[]) {
  bytes[0] = BITS(word, 0, 7);
  bytes[1] = BITS(word, 8, 15);
  bytes[2] = BITS(word, 16, 23);
  bytes[3] = BITS(word, 24, 31);
  // printf("word=%08X bytes=%02X%02X%02X%02X\n", word, bytes[0], bytes[1], bytes[2], bytes[3]);
}

// Convert Little Endian to i32
uint32_t bytesToWord(uint8_t bytes[]) {
  return (uint32_t)bytes[0] | (uint32_t)bytes[1] << 8 | (uint32_t)bytes[2] << 16 | (uint32_t)bytes[3] << 24;
}
