#include "lib.hpp"

int mapFind(map<string, int> *iMap, char *name) {
  string name0(name);
  map<string, int>::const_iterator pos = iMap->find(name0);
  if (pos == iMap->end()) return -1;
  return pos->second;
}

void mapDump(map<string, int> *iMap) {
  printf("=================symMap===================\n");
  for(auto it = iMap->cbegin(); it != iMap->cend(); ++it) {
      cout << it->first << " " << it->second << "\n";
  }
}

