# xv6 的底層輸出入

## 閱讀

* [Chapter 5 -- Interrupts and device drivers](../book/05n-InterruptDriver.md)
* [Chapter 8 -- File system](../book/08n-FileSystem.md)

## 實作

* [xv6 的 uart 輸出](./01-output)
* [xv6 的 uart 輸入](./02-input)
* [xv6 的 disk 磁碟存取](./03-disk)
* [xv6 的 net 網路存取](./04-net)

## 心得

* [RISC-V 上的底層輸出入](./xv6io.md)

