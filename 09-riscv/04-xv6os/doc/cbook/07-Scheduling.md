# Chapter 7 -- Scheduling

Any operating system is likely to run with more processes than the computer has CPUs, so a plan is needed to time-share the CPUs among the processes. Ideally the sharing would be transparent to user processes. A common approach is to provide each process with the illusion that it has its own virtual CPU by multiplexing the processes onto the hardware CPUs. This chapter explains how xv6 achieves this multiplexing.

任何操作系统运行的进程数量都可能超过计算机的CPU数量，因此需要制定一个计划，在各进程之间分时共享CPU。理想情况下，这种共享对用户进程是透明的。一种常见的方法是通过将进程复用到硬件CPU上，给每个进程提供它有自己的虚拟CPU的假象。本章解释xv6如何实现这种复用。

## 7.1 Multiplexing

Xv6 multiplexes by switching each CPU from one process to another in two situations. First, xv6’s sleep and wakeup mechanism switches when a process waits for device or pipe I/O to complete, or waits for a child to exit, or waits in the sleep system call. Second, xv6 periodically forces a switch to cope with processes that compute for long periods without sleeping. This multiplexing creates the illusion that each process has its own CPU, just as xv6 uses the memory allocator and hardware page tables to create the illusion that each process has its own memory.

Implementing multiplexing poses a few challenges. First, how to switch from one process to another? Although the idea of context switching is simple, the implementation is some of the most opaque code in xv6. Second, how to force switches in a way that is transparent to user processes? Xv6 uses the standard technique of driving context switches with timer interrupts. Third, many CPUs may be switching among processes concurrently, and a locking plan is necessary to avoid races. Fourth, a process’s memory and other resources must be freed when the process exits, but it cannot do all of this itself because (for example) it can’t free its own kernel stack while still using it. Fifth, each core of a multi-core machine must remember which process it is executing so that system calls affect the correct process’s kernel state. Finally, sleep and wakeup allow a process to give up the CPU and sleep waiting for an event, and allows another process to wake the first process up. Care is needed to avoid races that result in the loss of wakeup notifications. Xv6 tries to solve these problems as simply as possible, but nevertheless the resulting code is tricky

xv6通过在两种情况下将每个CPU从一个进程切换到另一个进程，从而实现复用。首先，xv6的睡眠和唤醒机制在进程等待设备或管道I/O完成时，或等待子进程退出时，或在睡眠系统调用中等待时，会进行切换。其次，xv6会周期性地强制切换，以应对长时间不睡觉的计算进程。这种多路复用造成了每个进程都有自己的CPU的假象，就像xv6使用内存分配器和硬件页表造成每个进程都有自己的内存的假象一样。

实现多路复用带来了一些挑战。首先，如何从一个进程切换到另一个进程？虽然上下文切换的想法很简单，但实现起来却是xv6中最不透明的一些代码。第二，如何以对用户进程透明的方式强制切换？xv6采用标准的技术，用定时器中断来驱动上下文切换。第三，许多CPU可能会在进程间并发切换，为了避免竞赛，需要制定锁定计划。第四，当进程退出时，必须释放进程的内存和其他资源，但它自己不能做到这一切，因为（例如）它不能在还在使用内核栈时释放自己的内核栈。第五，多核机的每个内核必须记住它正在执行的进程，这样系统调用就会影响正确的进程的内核状态。最后，睡眠和唤醒允许一个进程放弃CPU和睡眠等待事件，并允许另一个进程唤醒第一个进程。需要注意避免竞赛，导致唤醒通知的丢失。Xv6试图尽可能简单地解决这些问题，但尽管如此，产生的代码还是很棘手的

![](../img/Figure7.1.png)

## 7.2 Code: Context switching

Figure 7.1 outlines the steps involved in switching from one user process to another: a user-kernel transition (system call or interrupt) to the old process’s kernel thread, a context switch to the current CPU’s scheduler thread, a context switch to a new process’s kernel thread, and a trap return to the user-level process. The xv6 scheduler has a dedicated thread (saved registers and stack) per CPU because it is not safe for the scheduler execute on the old process’s kernel stack: some other core might wake the process up and run it, and it would be a disaster to use the same stack on two different cores. In this section we’ll examine the mechanics of switching between a kernel thread and a scheduler thread.

Switching from one thread to another involves saving the old thread’s CPU registers, and restoring the previously-saved registers of the new thread; the fact that the stack pointer and program counter are saved and restored means that the CPU will switch stacks and switch what code it is executing.

图7.1概述了从一个用户进程切换到另一个用户进程的步骤：用户内核转换（系统调用或中断）到旧进程的内核线程，上下文切换到当前CPU的调度器线程，上下文切换到新进程的内核线程，陷阱返回到用户级进程。xv6调度器每个CPU有一个专门的线程（保存的寄存器和堆栈），因为调度器在旧进程的内核堆栈上执行是不安全的：其他一些核心可能会唤醒进程并运行它，而在两个不同的核心上使用相同的堆栈将是一场灾难。在这一节中，我们将研究内核线程和调度器线程之间的切换机制。

从一个线程切换到另一个线程涉及到保存旧线程的CPU寄存器，并恢复新线程之前保存的寄存器；保存和恢复堆栈指针和程序计数器意味着CPU将切换堆栈，切换它正在执行的代码。

The function swtch performs the saves and restores for a kernel thread switch. swtch doesn’t directly know about threads; it just saves and restores register sets, called contexts. When it is time for a process to give up the CPU, the process’s kernel thread calls swtch to save its own context and return to the scheduler context. Each context is contained in a struct context (kernel/proc.h:2), itself contained in a process’s struct proc or a CPU’s struct cpu. Swtch takes two arguments: struct context *old and struct context *new. It saves the current registers in old, loads registers from new, and returns.

Let’s follow a process through swtch into the scheduler. We saw in Chapter 4 that one possibility at the end of an interrupt is that usertrap calls yield. Yield in turn calls sched, which calls swtch to save the current context in p->context and switch to the scheduler context previously saved in cpu->scheduler (kernel/proc.c:509).

函数swtch执行内核线程切换的保存和恢复，swtch并不直接知道线程的情况，它只是保存和恢复寄存器集，称为contexts。当一个进程要放弃CPU的时候，该进程的内核线程会调用swtch保存自己的上下文，并返回调度器上下文。每个上下文都包含在一个struct context（kernel/proc.h:2）中，它本身包含在进程的struct proc或CPU的struct cpu中。Swtch需要两个参数：struct context *old和struct context *new。它将当前寄存器保存在old中，从new中加载寄存器，然后返回。

让我们跟随一个进程通过swtch进入调度器。我们在第4章看到，在中断结束时，有一种可能是usertrap调用yield。yield又调用sched，sched调用swtch将当前上下文保存在p->context中，并切换到之前保存在cpu->scheduler中的调度器上下文（kernel/proc.c:509）。

Swtch (kernel/swtch.S:3) saves only callee-saved registers; caller-saved registers are saved on the stack (if needed) by the calling C code. Swtch knows the offset of each register’s field in struct context. It does not save the program counter. Instead, swtch saves the ra register, which holds the return address from which swtch was called. Now swtch restores registers from the new context, which holds register values saved by a previous swtch. When swtch returns, it returns to the instructions pointed to by the restored ra register, that is, the instruction from which the new thread previously called swtch. In addition, it returns on the new thread’s stack.

In our example, sched called swtch to switch to cpu->scheduler, the per-CPU scheduler context. That context had been saved by scheduler’s call to swtch (kernel/proc.c:475). When the swtch we have been tracing returns, it returns not to sched but to scheduler, and its stack pointer points at the current CPU’s scheduler stack.

Swtch(kernel/swtch.S:3)只保存调用者保存的寄存器，调用者保存的寄存器由调用的C代码保存在堆栈上（如果需要的话）。Swtch知道结构上下文中每个寄存器字段的偏移量。它不保存程序计数器。相反，swtch保存了ra寄存器，它保存了swtch被调用的返回地址。现在，swtch从新的上下文中恢复寄存器，新的上下文中保存着前一次swtch所保存的寄存器值。当swtch返回时，它返回到被恢复的ra寄存器所指向的指令，也就是新线程之前调用swtch的指令。此外，它还会在新线程的栈上返回。

在我们的例子中，sched调用swtch切换到cpu->scheduler，即每CPU调度器上下文。这个上下文已经被调度器调用swtch（kernel/proc.c:475）保存了下来。当我们一直追踪的swtch返回时，它不是返回到sched而是返回到调度器，它的栈指针指向当前CPU的调度器栈。

## 7.3 Code: Scheduling

The last section looked at the low-level details of swtch; now let’s take swtch as a given and examine switching from one process’s kernel thread through the scheduler to another process. The scheduler exists in the form of a special thread per CPU, each running the scheduler function.

This function is in charge of choosing which process to run next. A process that wants to give up the CPU must acquire its own process lock p->lock, release any other locks it is holding, update its own state (p->state), and then call sched. Yield (kernel/proc.c:515) follows this convention, as do sleep and exit, which we will examine later. Sched double-checks those conditions (kernel/proc.c:499-504) and then an implication of those conditions: since a lock is held, interrupts should be disabled. Finally, sched calls swtch to save the current context in p->context and switch to the scheduler context in cpu->scheduler. Swtch returns on the scheduler’s stack as though scheduler’s swtch had returned The scheduler continues the for loop, finds a process to run, switches to it, and the cycle repeats.

上一节研究了swtch的低级细节，现在我们把swtch作为一个给定的条件，研究从一个进程的内核线程通过调度器切换到另一个进程。调度器以每个CPU一个特殊线程的形式存在，每个线程都运行调度器函数。

这个函数负责选择下一步运行哪个进程。一个想要放弃CPU的进程必须获得自己的进程锁p->lock，释放它所持有的其他锁，更新自己的状态（p->state），然后调用sched。Yield (kernel/proc.c:515)遵循这个惯例，就像我们稍后要研究的sleep和exit一样。Sched对这些条件进行双重检查(kernel/proc.c:499-504)，然后对这些条件进行暗示：因为锁被持有，所以中断应该被禁止。最后，sched调用swtch保存p->context中的当前上下文，并在cpu->scheduler中切换到调度器上下文。swtch在调度器的堆栈上返回，就像调度器的swtch已经返回一样 调度器继续for循环，找到一个要运行的进程，切换到它，然后循环重复。

We just saw that xv6 holds p->lock across calls to swtch: the caller of swtch must already hold the lock, and control of the lock passes to the switched-to code. This convention is unusual with locks; usually the thread that acquires a lock is also responsible for releasing the lock, which makes it easier to reason about correctness. For context switching it is necessary to break this convention because p->lock protects invariants on the process’s state and context fields that are not true while executing in swtch. One example of a problem that could arise if p->lock were not held during swtch: a different CPU might decide to run the process after yield had set its state to RUNNABLE, but before swtch caused it to stop using its own kernel stack. The result would be two CPUs running on the same stack, which cannot be right.

我们刚刚看到xv6在调用swtch的过程中持有p->lock：swtch的调用者必须已经持有锁，而锁的控制权则传递给切换到的代码。这种约定对于锁来说是不寻常的，通常获得锁的线程也要负责释放锁，这样更容易推理出正确性。对于上下文切换来说，有必要打破这个约定，因为p->lock保护了进程的状态和上下文字段上的不变量，而这些不变量在swtch中执行时是不真实的。如果p->lock在swtch期间不被保持，可能会出现问题的一个例子：在yield将其状态设置为RUNNABLE之后，但在swtch使其停止使用自己的内核栈之前，不同的CPU可能会决定运行这个进程。结果就是两个CPU运行在同一个栈上，这是不可能的。

A kernel thread always gives up its CPU in sched and always switches to the same location in the scheduler, which (almost) always switches to some kernel thread that previously called sched. Thus, if one were to print out the line numbers where xv6 switches threads, one would observe the following simple pattern: (kernel/proc.c:475), (kernel/proc.c:509), (kernel/proc.c:475), (kernel/proc.c:509), and so on. The procedures in which this stylized switching between two threads happens are sometimes referred to as coroutines; in this example, sched and scheduler are co-routines of each other.

There is one case when the scheduler’s call to swtch does not end up in sched. When a new process is first scheduled, it begins at forkret (kernel/proc.c:527). Forkret exists to release the p->lock; otherwise, the new process could start at usertrapret.

Scheduler (kernel/proc.c:457) runs a simple loop: find a process to run, run it until it yields, repeat. The scheduler loops over the process table looking for a runnable process, one that has p->state == RUNNABLE. Once it finds a process, it sets the per-CPU current process variable c->proc, marks the process as RUNNING, and then calls swtch to start running it (kernel/proc.c:470-475).

一个内核线程总是在sched中放弃它的CPU，并且总是切换到调度器的同一个位置，而调度器（几乎）总是切换到之前调用sched的某个内核线程。因此，如果把xv6切换线程的行号打印出来，就会观察到下面的简单模式。(kernel/proc.c:475)，(kernel/proc.c:509)，(kernel/proc.c:475)，(kernel/proc.c:509)，等等。这种发生在两个线程之间的风格化切换的程序有时被称为coroutine；在这个例子中，sched和调度器是彼此的联合程序。

有一种情况是，调度器对swtch的调用最终没有在sched中完成。当一个新进程第一次被调度时，它开始于forkret(kernel/proc.c:527)。Forkret的存在是为了释放p->锁；否则，新进程可能从usertrapret开始。

调度器(kernel/proc.c:457)运行一个简单的循环：找到一个要运行的进程，运行它直到它屈服，重复。调度器在进程表上循环，寻找一个可运行的进程，一个有p->state == RUNNABLE的进程。一旦找到一个进程，它就会设置每个CPU当前进程变量c->proc，将该进程标记为RUNNING，然后调用swtch开始运行它(kernel/proc.c:470-475)。

One way to think about the structure of the scheduling code is that it enforces a set of invariants about each process, and holds p->lock whenever those invariants are not true. One invariant is that if a process is RUNNING, a timer interrupt’s yield must be able to safely switch away from the process; this means that the CPU registers must hold the process’s register values (i.e. swtch hasn’t moved them to a context), and c->proc must refer to the process. Another invariant is that if a process is RUNNABLE, it must be safe for an idle CPU’s scheduler to run it; this means that p->context must hold the process’s registers (i.e., they are not actually in the real registers), that no CPU is executing on the process’s kernel stack, and that no CPU’s c->proc refers to the process. Observe that these properties are often not true while p->lock is held.

Maintaining the above invariants is the reason why xv6 often acquires p->lock in one thread and releases it in other, for example acquiring in yield and releasing in scheduler. Once yield has started to modify a running process’s state to make it RUNNABLE, the lock must remain held until the invariants are restored: the earliest correct release point is after scheduler (running on its own stack) clears c->proc. Similarly, once scheduler starts to convert a RUNNABLE process to RUNNING, the lock cannot be released until the kernel thread is completely running (after the swtch, for example in yield).

p->lock protects other things as well: the interplay between exit and wait, the machinery to avoid lost wakeups (see Section 7.5), and avoidance of races between a process exiting and other processes reading or writing its state (e.g., the exit system call looking at p->pid and setting p->killed (kernel/proc.c:611)). It might be worth thinking about whether the different functions of p->lock could be split up, for clarity and perhaps for performance.

思考调度代码结构的一种方法是，它强制执行一组关于每个进程的不变性，并且每当这些不变性不为真时，就会持有p->lock。一个不变性是，如果一个进程在RUNNING，定时器中断的产率必须能够安全地从进程中切换开来；这意味着CPU寄存器必须持有进程的寄存器值（即swtch没有把它们移到上下文中），c->proc必须引用进程。另一个不变性是，如果一个进程是RUNNABLE的，那么对于一个空闲的CPU的调度器来说，运行它必须是安全的；这意味着p->context必须持有进程的寄存器（即它们实际上并不在真实的寄存器中），没有CPU在进程的内核堆栈上执行，没有CPU的c->proc引用进程。请注意，当p->lock被持有时，这些属性往往不是真的。

保持上述不变性是xv6经常在一个线程中获取p->lock，而在另一个线程中释放它的原因，例如在yield中获取，在调度器中释放。一旦yield开始修改一个正在运行的进程的状态，使其成为RUNNABLE，锁必须一直保持，直到不变式被恢复：最早正确的释放点是在调度器（运行在自己的堆栈上）清除c->proc之后。同样，一旦调度器开始将一个RUNNABLE进程转换为RUNNING进程，在内核线程完全运行之前（例如在yield中的swtch之后），锁不能被释放。

p->lock也保护其他的东西：退出和等待之间的相互作用，避免丢失唤醒的机制（见7.5节），以及避免进程退出和其他进程读写其状态之间的竞赛（例如，退出系统调用查看p->pid并设置p->killed (kernel/proc.c:611)）。也许值得思考的是，是否可以将p->lock的不同功能拆分开来，这样既清晰，也可能是为了性能。

## 7.4 Code: mycpu and myproc

Xv6 often needs a pointer to the current process’s proc structure. On a uniprocessor one could have a global variable pointing to the current proc. This doesn’t work on a multi-core machine, since each core executes a different process. The way to solve this problem is to exploit the fact that each core has its own set of registers; we can use one of those registers to help find per-core information.

Xv6 maintains a struct cpu for each CPU (kernel/proc.h:22), which records the process currently running on that CPU (if any), saved registers for the CPU’s scheduler thread, and the count of nested spinlocks needed to manage interrupt disabling. The function mycpu (kernel/proc.c:60) returns a pointer to the current CPU’s struct cpu. RISC-V numbers its CPUs, giving each a hartid. Xv6 ensures that each CPU’s hartid is stored in that CPU’s tp register while in the kernel. This allows mycpu to use tp to index an array of cpu structures to find the right one.

Xv6经常需要一个指向当前进程的proc结构的指针。在单核处理器上，可以有一个全局变量指向当前的proc。这在多核机器上是行不通的，因为每个核都执行不同的进程。解决这个问题的方法是利用每个核都有自己的一组寄存器的事实；我们可以使用其中的一个寄存器来帮助查找每个核的信息。

Xv6为每个CPU维护了一个结构cpu(kernel/proc.h:22)，它记录了当前在该CPU上运行的进程(如果有的话)，为CPU的调度线程保存的寄存器，以及管理中断禁用所需的嵌套自旋锁的计数。函数mycpu(kernel/proc.c:60)返回一个指向当前CPU的结构cpu的指针。RISC-V对其CPU进行了编号，给每个CPU一个hartid。Xv6确保每个CPU的hartid在内核中存储在该CPU的tp寄存器中。这使得mycpu可以使用tp来索引cpu结构的数组来找到正确的cpu。

Ensuring that a CPU’s tp always holds the CPU’s hartid is a little involved. mstart sets the tp register early in the CPU’s boot sequence, while still in machine mode (kernel/start.c:46). usertrapret saves tp in the trampoline page, because the user process might modify tp. Finally uservec restores that saved tp when entering the kernel from user space (kernel/trampoline.S:70).

The compiler guarantees never to use the tp register. It would be more convenient if RISC-V allowed xv6 to read the current hartid directly, but that is allowed only in machine mode, not in supervisor mode.

确保CPU的tp始终保持CPU的hartid是有一点难度的。mstart在CPU启动序列的早期设置tp寄存器，当时还处于机器模式(kernel/start.c:46)。最后，当从用户空间进入内核时，uservec会恢复保存的tp(kernel/trampoline.S:70)。

编译器保证永远不使用tp寄存器。如果RISC-V允许xv6直接读取当前的hartid会更方便，但这只允许在机器模式下，不允许在主管模式下使用。

The return values of cpuid and mycpu are fragile: if the timer were to interrupt and cause the thread to yield and then move to a different CPU, a previously returned value would no longer be correct. To avoid this problem, xv6 requires that callers disable interrupts, and only enable them after they finish using the returned struct cpu.

The function myproc (kernel/proc.c:68) returns the struct proc pointer for the process that is running on the current CPU. myproc disables interrupts, invokes mycpu, fetches the current process pointer (c->proc) out of the struct cpu, and then enables interrupts. The return value of myproc is safe to use even if interrupts are enabled: if a timer interrupt moves the calling process to a different CPU, its struct proc pointer will stay the same.

cpuid和mycpu的返回值是很脆弱的：如果定时器中断，导致线程屈服，然后转移到不同的CPU上，之前返回的值将不再正确。为了避免这个问题，xv6要求调用者禁用中断，只有在使用完返回的结构cpu后才启用。

函数myproc(kernel/proc.c:68)返回当前CPU上运行的进程的struct proc指针，myproc禁用中断，调用mycpu，从struct cpu中获取当前进程指针(c->proc)，然后启用中断。即使启用了中断，myproc的返回值也可以安全使用：如果定时器中断将调用进程移到了不同的CPU上，它的proc结构指针将保持不变。

## 7.5 Sleep and wakeup

Scheduling and locks help conceal the existence of one process from another, but so far we have no abstractions that help processes intentionally interact. Many mechanisms have been invented to solve this problem. Xv6 uses one called sleep and wakeup, which allow one process to sleep waiting for an event and another process to wake it up once the event has happened. Sleep and wakeup are often called sequence coordination or conditional synchronization mechanisms.

To illustrate, let’s consider a synchronization mechanism called a semaphore [4] that coordinates producers and consumers. A semaphore maintains a count and provides two operations. The “V” operation (for the producer) increments the count. The “P” operation (for the consumer) waits until the count is non-zero, and then decrements it and returns. If there were only one producer thread and one consumer thread, and they executed on different CPUs, and the compiler didn’t optimize too aggressively, this implementation would be correct:

调度和锁有助于隐藏一个进程与另一个进程的存在，但到目前为止，我们还没有任何抽象的方法来帮助进程有意地进行交互。人们发明了许多机制来解决这个问题。Xv6使用了一种叫做睡眠和唤醒的机制，它允许一个进程睡觉等待事件，而另一个进程在事件发生后将其唤醒。睡眠和唤醒通常被称为序列协调或条件同步机制。

为了说明这一点，让我们考虑一种叫做semaphore[4]的同步机制，它可以协调生产者和消费者。一个旗语体维护一个计数，并提供两个操作。"V "操作（针对生产者）递增计数。P "操作（针对消费者）等待，直到计数非零，然后递减并返回。如果只有一个生产者线程和一个消费者线程，而且它们在不同的CPU上执行，编译器也没有太过激进的优化，那么这个实现是正确的。

```cpp
100 struct semaphore {
101 struct spinlock lock;
102   int count;
103 };
104
105 void
106 V(struct semaphore *s)
107 {
108   acquire(&s->lock);
109   s->count += 1;
110   release(&s->lock);
111 }
112
113 void
114 P(struct semaphore *s)
115 {
116   while(s->count == 0)
117   ;
118   acquire(&s->lock);
119   s->count -= 1;
120   release(&s->lock);
121 }
```

The implementation above is expensive. If the producer acts rarely, the consumer will spend most of its time spinning in the while loop hoping for a non-zero count. The consumer’s CPU could find more productive work than with busy waiting by repeatedly polling s->count. Avoiding busy waiting requires a way for the consumer to yield the CPU and resume only after V increments the count.

Here’s a step in that direction, though as we will see it is not enough. Let’s imagine a pair of calls, sleep and wakeup, that work as follows. Sleep(chan) sleeps on the arbitrary value chan, called the wait channel. Sleep puts the calling process to sleep, releasing the CPU for other work. Wakeup(chan) wakes all processes sleeping on chan (if any), causing their sleep calls to return. If no processes are waiting on chan, wakeup does nothing. We can change the semaphore implementation to use sleep and wakeup (changes highlighted in yellow):

上面的实现是昂贵的。如果生产者很少行动，消费者就会花大部分时间在while循环中旋转，希望得到一个非零的计数。消费者的CPU可以通过反复轮询s->count来找到比忙等更有成效的工作。避免忙碌的等待需要有一种方法让消费者让出CPU，并在V增量计数后才继续工作。

这里是朝这个方向迈出的一步，不过我们会看到，这还不够。让我们想象一对调用，睡眠和唤醒，其工作原理如下。Sleep(chan)对任意值chan进行睡眠，称为等待通道。Sleep使调用进程进入睡眠状态，释放CPU进行其他工作。Wakeup(chan)唤醒所有在chan上睡觉的进程（如果有的话），使它们的睡眠调用返回。如果没有进程在chan上等待，则wakeup不做任何事情。我们可以更改旗语实现，使用sleep和wakeup（黄色高亮显示的更改）。

```cpp
200 void
201 V(struct semaphore *s)
202 {
203   acquire(&s->lock);
204   s->count += 1;
205   wakeup(s);
206   release(&s->lock);
207 }
208
209 void
210 P(struct semaphore *s)
211 {
212   while(s->count == 0)
213     sleep(s);
214   acquire(&s->lock);
215   s->count -= 1;
216   release(&s->lock);
217 }
```

P now gives up the CPU instead of spinning, which is nice. However, it turns out not to be straightforward to design sleep and wakeup with this interface without suffering from what is known as the lost wake-up problem. Suppose that P finds that s->count == 0 on line 212. While P is between lines 212 and 213, V runs on another CPU: it changes s->count to be nonzero and calls wakeup, which finds no processes sleeping and thus does nothing. Now P continues executing at line 213: it calls sleep and goes to sleep. This causes a problem: P is asleep waiting for a V call that has already happened. Unless we get lucky and the producer calls V again, the consumer will wait forever even though the count is non-zero.

The root of this problem is that the invariant that P only sleeps when s->count == 0 is violated by V running at just the wrong moment. An incorrect way to protect the invariant would be to move the lock acquisition (highlighted in yellow below) in P so that its check of the count and its call to sleep are atomic:

P现在放弃了CPU而不是旋转，这很好。然而，事实证明，用这个接口设计睡眠和唤醒并不直接，而不会遭受所谓的丢失唤醒问题。假设P在第212行发现s->count == 0。当P在第212行和213行之间时，V在另一个CPU上运行：它将s->count改为非零，并调用wakeup，wakeup发现没有进程在睡觉，因此什么也不做。现在P在第213行继续执行：它调用sleep并进入睡眠状态。这就造成了一个问题：P正在睡觉，等待一个已经发生的V调用。除非我们运气好，生产者再次调用V，否则消费者将永远等待，即使计数是非零。

这个问题的根源在于，P只有在s->count ==0时才会睡眠的不变性被V在恰好错误的时刻运行所违反。保护这个不变性的一个不正确的方法是移动P中的锁的获取（在下面的黄色中高亮显示），使其对计数的检查和对睡眠的调用是原子的。

```cpp
300 void
301 V(struct semaphore *s)
302 {
303   acquire(&s->lock);
304   s->count += 1;
305   wakeup(s);
306   release(&s->lock);
307 }
308
309 void
310 P(struct semaphore *s)
311 {
312   acquire(&s->lock);
313   while(s->count == 0)
314     sleep(s);
315   s->count -= 1;
316   release(&s->lock);
317 }
```

One might hope that this version of P would avoid the lost wakeup because the lock prevents V from executing between lines 313 and 314. It does that, but it also deadlocks: P holds the lock while it sleeps, so V will block forever waiting for the lock.

We’ll fix the preceding scheme by changing sleep’s interface: the caller must pass the condition lock to sleep so it can release the lock after the calling process is marked as asleep and waiting on the sleep channel. The lock will force a concurrent V to wait until P has finished putting itself to sleep, so that the wakeup will find the sleeping consumer and wake it up. Once the consumer is awake again sleep reacquires the lock before returning. Our new correct sleep/wakeup scheme is usable as follows (change highlighted in yellow):

人们可能希望这个版本的P能够避免丢失的唤醒，因为锁会阻止V在第313和314行之间执行。它做到了这一点，但它也会死锁。P在睡眠时保持着锁, 所以V将永远阻塞在等待锁的过程中.

我们将通过改变sleep的接口来解决前面的方案：调用者必须将条件锁传递给sleep，这样它就可以在调用进程被标记为睡眠并在睡眠通道上等待后释放锁。这个锁会强制并发的V等到P完成让自己进入睡眠状态，这样唤醒就会发现睡眠中的消费者，并将其唤醒。一旦消费者再次被唤醒，睡眠就会重新获得锁，然后再返回。我们新的正确的睡眠/唤醒方案可用如下（变化用黄色突出显示）。

```cpp
400 void
401 V(struct semaphore *s)
402 {
403   acquire(&s->lock);
404   s->count += 1;
405   wakeup(s);
406   release(&s->lock);
407 }
408
409 void
410 P(struct semaphore *s)
411 {
412   acquire(&s->lock);
413   while(s->count == 0)
414     sleep(s, &s->lock);
415   s->count -= 1;
416   release(&s->lock);
417 }
```

The fact that P holds s->lock prevents V from trying to wake it up between P’s check of c->count and its call to sleep. Note, however, that we need sleep to atomically release s->lock and put the consuming process to sleep.

P持有s->锁的事实阻止了V在P检查c->count和调用睡眠之间试图唤醒它。但是，请注意，我们需要睡眠来原子地释放s->锁并使消耗进程进入睡眠状态。

## 7.6 Code: Sleep and wakeup

Let’s look at the implementation of sleep (kernel/proc.c:548) and wakeup (kernel/proc.c:582). The basic idea is to have sleep mark the current process as SLEEPING and then call sched to release the CPU; wakeup looks for a process sleeping on the given wait channel and marks it as RUNNABLE. Callers of sleep and wakeup can use any mutually convenient number as the channel. Xv6 often uses the address of a kernel data structure involved in the waiting.

Sleep acquires p->lock (kernel/proc.c:559). Now the process going to sleep holds both p->lock and lk. Holding lk was necessary in the caller (in the example, P): it ensured that no other process (in the example, one running V) could start a call to wakeup(chan). Now that sleep holds p->lock, it is safe to release lk: some other process may start a call to wakeup(chan), but wakeup will wait to acquire p->lock, and thus will wait until sleep has finished putting the process to sleep, keeping the wakeup from missing the sleep.

There is a minor complication: if lk is the same lock as p->lock, then sleep would deadlock with itself if it tried to acquire p->lock. But if the process calling sleep already holds p->lock, it doesn’t need to do anything more in order to avoiding missing a concurrent wakeup. This case arises when wait (kernel/proc.c:582) calls sleep with p->lock.

让我们看看sleep (kernel/proc.c:548) 和 wakeup (kernel/proc.c:582) 的实现。其基本思想是让sleep将当前进程标记为SLEEPING，然后调用sched释放CPU；wakeup寻找一个在给定的等待通道上睡觉的进程，并将其标记为RUNNABLE。sleep和wakeup的调用者可以使用任何相互方便的数字作为通道。Xv6经常使用参与等待的内核数据结构的地址。

Sleep获取p->lock（kernel/proc.c:559）。现在进入睡眠的进程同时持有p->lock和lk。在调用者（在本例中，P）中，持有lk是必要的：它保证了没有其他进程（在本例中，运行V的进程）可以启动对wakeup(chan)的调用。现在，sleep持有p->锁，释放lk是安全的：其他进程可能会启动对wakeup(chan)的调用，但wakeup会等待获取p->锁，因此会等到sleep完成将进程放入睡眠状态，使wakeup不会错过睡眠。

有一个小的复杂情况：如果lk和p->lock是同一个锁，那么sleep如果试图获取p->lock，就会和自己死锁。但是如果调用sleep的进程已经持有p->lock，那么为了避免错过并发唤醒，它不需要再做任何事情。当wait (kernel/proc.c:582)调用sleep并持有p->lock时，就会出现这种情况。

Now that sleep holds p->lock and no others, it can put the process to sleep by recording the sleep channel, changing the process state to SLEEPING, and calling sched (kernel/proc.c:564-567).

In a moment it will be clear why it’s critical that p->lock is not released (by scheduler) until after the process is marked SLEEPING.

At some point, a process will acquire the condition lock, set the condition that the sleeper is waiting for, and call wakeup(chan). It’s important that wakeup is called while holding the condition lock1. Wakeup loops over the process table (kernel/proc.c:582). It acquires the p->lock of each process it inspects, both because it may manipulate that process’s state and because p->lock ensures that sleep and wakeup do not miss each other. When wakeup finds a process in state SLEEPING with a matching chan, it changes that process’s state to RUNNABLE. The next time the scheduler runs, it will see that the process is ready to be run.

Why do the locking rules for sleep and wakeup ensure a sleeping process won’t miss a wakeup? The sleeping process holds either the condition lock or its own p->lock or both from a point before it checks the condition to a point after it is marked SLEEPING. The process calling wakeup holds both of those locks in wakeup’s loop. Thus the waker either makes the condition true before the consuming thread checks the condition; or the waker’s wakeup examines the sleeping thread strictly after it has been marked SLEEPING. Then wakeup will see the sleeping process and wake it up (unless something else wakes it up first).

现在sleep持有p->锁，而没有其他的锁，它可以通过记录睡眠通道，将进程状态改为SLEEPING，并调用sched(kernel/proc.c:564-567)来使进程进入睡眠状态。

一会儿就会明白为什么在进程被标记为SLEEPING之后才释放p->lock（由调度器）是至关重要的。

在某些时候，进程会获取条件锁，设置睡眠者正在等待的条件，并调用wakeup(chan)。重要的是，wakeup是在持有条件锁1的情况下调用的。Wakeup循环浏览进程表（kernel/proc.c:582）。它获取它所检查的每一个进程的p->锁，一方面是因为它可能会操纵该进程的状态，另一方面是因为p->锁确保睡眠和唤醒不会相互错过。当wakeup发现一个进程处于状态为SLEEPING并有一个匹配的chan时，它就会将该进程的状态改为RUNNABLE。下一次调度器运行时，就会看到这个进程已经准备好运行了。

为什么睡眠和唤醒的锁定规则能保证睡眠进程不会错过唤醒？睡眠进程从检查条件之前的一点到标记为sleeping之后的一点，都持有条件锁或自己的p->锁或两者都持有。调用唤醒的进程在唤醒的循环中持有这两个锁。因此，waker要么在消耗线程检查条件之前使条件为真；要么waker的wakeup严格地在睡眠线程被标记为SLEEPING之后检查它。那么wakeup就会看到睡眠进程，并将其唤醒（除非有其他事情先将其唤醒）。

It is sometimes the case that multiple processes are sleeping on the same channel; for example, more than one process reading from a pipe. A single call to wakeup will wake them all up. One of them will run first and acquire the lock that sleep was called with, and (in the case of pipes) read whatever data is waiting in the pipe. The other processes will find that, despite being woken up, there is no data to be read. From their point of view the wakeup was “spurious,” and they must sleep again. For this reason sleep is always called inside a loop that checks the condition.

No harm is done if two uses of sleep/wakeup accidentally choose the same channel: they will see spurious wakeups, but looping as described above will tolerate this problem. Much of the charm of sleep/wakeup is that it is both lightweight (no need to create special data structures to act as sleep channels) and provides a layer of indirection (callers need not know which specific process they are interacting with).

有时会出现多个进程在同一个通道上睡觉的情况；例如，有多个进程从管道中读取数据。调用一次唤醒就会把它们全部唤醒。其中一个进程将首先运行，并获得睡眠被调用的锁，（在管道的情况下）读取管道中等待的任何数据。其他进程会发现，尽管被唤醒了，但没有数据可读。从他们的角度来看，唤醒是 "虚假的"，他们必须再次睡眠。出于这个原因，sleep总是在一个检查条件的循环里面被调用。

如果sleep/wakeup的两个用法不小心选择了同一个通道，也不会造成伤害：它们会看到虚假的唤醒，但如上所述的循环会容忍这个问题。sleep/wakeup的魅力很大程度上在于它既轻量级（不需要创建特殊的数据结构来充当睡眠通道），又提供了一层间接性（调用者不需要知道他们正在与哪个具体进程交互）。

## 7.7 Code: Pipes

A more complex example that uses sleep and wakeup to synchronize producers and consumers is xv6’s implementation of pipes. We saw the interface for pipes in Chapter 1: bytes written to one end of a pipe are copied to an in-kernel buffer and then can be read from the other end of the pipe.

Future chapters will examine the file descriptor support surrounding pipes, but let’s look now at the implementations of pipewrite and piperead.

Each pipe is represented by a struct pipe, which contains a lock and a data buffer.

The fields nread and nwrite count the total number of bytes read from and written to the buffer. The buffer wraps around: the next byte written after buf[PIPESIZE-1] is buf[0]. The counts do not wrap. This convention lets the implementation distinguish a full buffer (nwrite ==nread+PIPESIZE) from an empty buffer (nwrite == nread), but it means that indexing into the buffer must use buf[nread % PIPESIZE] instead of just buf[nread] (and similarly for nwrite).

一个使用睡眠和唤醒来同步生产者和消费者的更复杂的例子是xv6对管道的实现。我们在第1章看到了管道的接口：写入管道一端的字节被复制到内核缓冲区，然后可以从管道的另一端读取。

未来的章节将研究围绕管道的文件描述符支持，但我们现在来看pipewrite和piperead的实现。

每个管道由一个结构体管道表示，它包含一个锁和一个数据缓冲区。

字段nread和nwrite统计从缓冲区读取和写入的字节总数。缓冲区环绕：在buf[PIPESIZE-1]之后写入的下一个字节是buf[0]。计数不环绕。这个约定使得实现可以区分满缓冲区(nwrite ==nread+PIPESIZE)和空缓冲区(nwrite ==nread)，但这意味着对缓冲区的索引必须使用buf[nread % PIPESIZE]，而不是只使用buf[nread](对nwrite也是如此)。

Let’s suppose that calls to piperead and pipewrite happen simultaneously on two different CPUs. Pipewrite (kernel/pipe.c:77) begins by acquiring the pipe’s lock, which protects the counts, the data, and their associated invariants. Piperead (kernel/pipe.c:103) then tries to acquire the lock too, but cannot. It spins in acquire (kernel/spinlock.c:22) waiting for the lock. While piperead waits, pipewrite loops over the bytes being written (addr[0..n-1]), adding each to the pipe in turn (kernel/pipe.c:95). During this loop, it could happen that the buffer fills (kernel/pipe.c:85). In this case, pipewrite calls wakeup to alert any sleeping readers to the fact that there is data waiting in the buffer and then sleeps on &pi->nwrite to wait for a reader to take some bytes out of the buffer. Sleep releases pi->lock as part of putting pipewrite’s process to sleep.

Now that pi->lock is available, piperead manages to acquire it and enters its critical section: it finds that pi->nread != pi->nwrite (kernel/pipe.c:110) (pipewrite went to sleep be cause pi->nwrite == pi->nread+PIPESIZE (kernel/pipe.c:85)), so it falls through to the for
loop, copies data out of the pipe (kernel/pipe.c:117), and increments nread by the number of bytes copied. That many bytes are now available for writing, so piperead calls wakeup (kernel/pipe.c:124) to wake any sleeping writers before it returns. Wakeup finds a process sleeping on &pi->nwrite, the process that was running pipewrite but stopped when the buffer filled. It marks that process as RUNNABLE.

The pipe code uses separate sleep channels for reader and writer (pi->nread and pi->nwrite); this might make the system more efficient in the unlikely event that there are lots of readers and writers waiting for the same pipe. The pipe code sleeps inside a loop checking the sleep condition; if there are multiple readers or writers, all but the first process to wake up will see the condition is still false and sleep again.

假设对piperead和pipewrite的调用同时发生在两个不同的CPU上。Pipewrite (kernel/pipe.c:77)首先获取管道的锁，它保护了计数、数据和相关的不变量。然后，Piperead (kernel/pipe.c:103) 也试图获取管子的锁，但不能。它在acquisition(kernel/spinlock.c:22)中旋转，等待锁的到来。当piperead等待时，pipewrite会在被写入的字节(addr[0...n-1])上循环，依次将每个字节添加到管道中(kernel/pipe.c:95)。在这个循环中，可能会发生缓冲区被填满的情况(kernel/pipe.c:85)。在这种情况下，pipewrite调用wakeup来提醒所有睡眠中的读者有数据在缓冲区中等待，然后在&pi->nwrite上睡眠，等待读者从缓冲区中取出一些字节。Sleep释放pi->lock作为将pipewrite的进程进入睡眠状态的一部分。

现在pi->lock是可用的，piperead设法获取它并进入它的关键部分：它发现pi->nread != pi->nwrite (kernel/pipe.c:110) (pipewrite进入睡眠状态的原因是pi->nwrite == pi->nread+PIPESIZE (kernel/pipe.c:85))，所以它落到了for
循环，将数据从管道(kernel/pipe.c:117)中复制出来，并按复制的字节数增加nread。现在有这么多字节可以用来写，所以 piperead 在返回之前调用 wakeup (kernel/pipe.c:124) 来唤醒所有沉睡的写入者。Wakeup发现一个在&pi->nwrite上睡觉的进程，这个进程正在运行pipewrite，但在缓冲区填满时停止了。它将该进程标记为RUNNABLE。

管道代码为读取器和写入器使用了单独的睡眠通道（pi->nread和pi->nwrite）；这可能会使系统在万一有很多读取器和写入器在等待同一个管道的情况下更加高效。管道代码在循环内睡眠，检查睡眠条件；如果有多个读写器，除了第一个被唤醒的进程外，其他进程都会看到条件仍然是假的，然后再次睡眠。

## 7.8 Code: Wait, exit, and kill

Sleep and wakeup can be used for many kinds of waiting. An interesting example, introduced in Chapter 1, is the interaction between a child’s exit and its parent’s wait. At the time of the child’s death, the parent may already be sleeping in wait, or may be doing something else; in the latter case, a subsequent call to wait must observe the child’s death, perhaps long after it calls exit. The way that xv6 records the child’s demise until wait observes it is for exit to put the caller into the ZOMBIE state, where it stays until the parent’s wait notices it, changes the child’s state to UNUSED, copies the child’s exit status, and returns the child’s process ID to the parent. If the parent exits before the child, the parent gives the child to the init process, which perpetually calls wait; thus every child has a parent to clean up after it. The main implementation challenge is the possibility of races and deadlock between parent and child wait and exit, as well as exit and exit.

睡眠和唤醒可以用于许多种等待。第1章中介绍的一个有趣的例子是，孩子的退出和父母的等待之间的互动。在孩子死亡的时候，父母可能已经在等待中睡觉了，也可能在做别的事情；在后一种情况下，后续的等待调用必须观察到孩子的死亡，也许是在它调用退出之后很久。xv6在wait观察到孩子的死亡之前，记录孩子死亡的方式是让exit将调用者放入ZOMBIE状态，在这个状态下停留，直到父体的wait注意到它，将孩子的状态改为UNUSED，复制孩子的退出状态，并将孩子的进程ID返回给父体。如果父进程比子进程先退出，父进程就会把子进程交给init进程，而init进程则永久地调用wait；这样每个子进程都有父进程来清理。主要的实现难点是父进程和子进程的wait和退出，以及退出和退出之间可能会出现竞赛和死锁。

Wait uses the calling process’s p->lock as the condition lock to avoid lost wakeups, and it acquires that lock at the start (kernel/proc.c:398). Then it scans the process table. If it finds a child in ZOMBIE state, it frees that child’s resources and its proc structure, copies the child’s exit status to the address supplied to wait (if it is not 0), and returns the child’s process ID. If wait finds children but none have exited, it calls sleep to wait for one of them to exit (kernel/proc.c:445), then scans again. Here, the condition lock being released in sleep is the waiting process’s p->lock, the special case mentioned above. Note that wait often holds two locks; that it acquires its own lock before trying to acquire any child’s lock; and that thus all of xv6 must obey the same locking order (parent, then child) in order to avoid deadlock.

Wait使用调用进程的p->锁作为条件锁，以避免唤醒丢失，它在启动时获取该锁（kernel/proc.c:398）。然后它扫描进程表。如果它发现一个处于ZOMBIE状态的子进程，它释放这个子进程的资源和它的proc结构，将子进程的退出状态复制到提供给wait的地址（如果它不是0），并返回子进程的ID。如果wait找到了子进程，但没有一个退出，它就调用sleep等待其中一个子进程退出(kernel/proc.c:445)，然后再次扫描。这里，在sleep中释放的条件锁是等待进程的p->锁，也就是上面提到的特殊情况。请注意，wait经常拥有两个锁；它在试图获取任何子锁之前，先获取自己的锁；因此，xv6的所有锁都必须遵守相同的锁定顺序（父锁，然后是子锁），以避免死锁。

Wait looks at every process’s np->parent to find its children. It uses np->parent without holding np->lock, which is a violation of the usual rule that shared variables must be protected by locks. It is possible that np is an ancestor of the current process, in which case acquiring np->lock could cause a deadlock since that would violate the order mentioned above. Examining np->parent without a lock seems safe in this case; a process’s parent field is only changed by its parent, so if np->parent==p is true, the value can’t change unless the current process changes it.

Wait会查看每个进程的np->parent来寻找它的子进程。它使用 np->parent 而不持有 np->lock，这违反了共享变量必须受锁保护的通常规则。np有可能是当前进程的祖先，在这种情况下，获取np->lock可能会导致死锁，因为这违反了上面提到的顺序。在这种情况下，在没有锁的情况下检查np->parent似乎是安全的；一个进程的父字段只有它的父字段才会被改变，所以如果np->parent==p为真，除非当前进程改变它，否则该值就不能改变。

Exit (kernel/proc.c:333) records the exit status, frees some resources, gives any children to the init process, wakes up the parent in case it is in wait, marks the caller as a zombie, and permanently yields the CPU. The final sequence is a little tricky. The exiting process must hold its parent’s lock while it sets its state to ZOMBIE and wakes the parent up, since the parent’s lock is the condition lock that guards against lost wakeups in wait. The child must also hold its own p->lock, since otherwise the parent might see it in state ZOMBIE and free it while it is still running.

Exit (kernel/proc.c:333)记录退出状态，释放一些资源，将任何子进程交给init进程，在父进程处于等待状态时唤醒它，将调用者标记为僵尸，并永久放弃CPU。最后的顺序有点棘手。退出的进程必须持有父进程的锁，同时将其状态设置为ZOMBIE并唤醒父进程，因为父进程的锁是条件锁，可以防止在等待中失去唤醒功能。子进程也必须持有它自己的p->锁，否则父进程可能会看到它处于ZOMBIE状态，并在它还在运行时释放它。

The lock acquisition order is important to avoid deadlock: since wait acquires the parent’s lock before the child’s lock, exit must use the same order.

Exit calls a specialized wakeup function, wakeup1, that wakes up only the parent, and only if it is sleeping in wait (kernel/proc.c:598). It may look incorrect for the child to wake up the parent before setting its state to ZOMBIE, but that is safe: although wakeup1 may cause the parent to run, the loop in wait cannot examine the child until the child’s p->lock is released by scheduler, so wait can’t look at the exiting process until well after exit has set its state to ZOMBIE (kernel/proc.c:386).

锁的获取顺序对于避免死锁很重要：因为wait在子锁之前获取父锁，所以exit必须使用同样的顺序。

exit调用了一个专门的唤醒函数wakeup1，这个函数只唤醒父函数，而且只在父函数在wait中睡觉的情况下才唤醒(kernel/proc.c:598)。子程序在将状态设置为ZOMBIE之前唤醒父程序可能看起来不正确，但这是安全的：尽管wakeup1可能会导致父程序运行，但在wait中的循环不能检查子程序，直到子程序的p->锁被调度器释放，所以wait在退出将状态设置为ZOMBIE之后才能查看退出的进程(kernel/proc.c:386)。

While exit allows a process to terminate itself, kill (kernel/proc.c:611) lets one process request that another terminate. It would be too complex for kill to directly destroy the victim process, since the victim might be executing on another CPU, perhaps in the middle of a sensitive sequence of updates to kernel data structures. Thus kill does very little: it just sets the victim’s p->killed and, if it is sleeping, wakes it up. Eventually the victim will enter or leave the kernel, at which point code in usertrap will call exit if p->killed is set. If the victim is running in user space, it will soon enter the kernel by making a system call or because the timer (or some other device) interrupts.

exit允许一个进程自行终止，而kernel/proc.c:611则允许一个进程请求另一个进程终止。如果让kill直接摧毁受害者进程，那就太复杂了，因为受害者可能正在另一个CPU上执行，也许正处于对内核数据结构进行敏感更新的过程中。因此，kill的作用很小：它只是设置受害者的p->killed，如果它在睡觉，则唤醒它。最终，受害者会进入或离开内核，这时如果p->killed被设置，usertrap中的代码会调用exit。如果受害者在用户空间运行，它将很快通过进行系统调用或因为定时器（或其他设备）中断而进入内核。

If the victim process is in sleep, kill’s call to wakeup will cause the victim to return from sleep. This is potentially dangerous because the condition being waiting for may not be true.

However, xv6 calls to sleep are always wrapped in a while loop that re-tests the condition after sleep returns. Some calls to sleep also test p->killed in the loop, and abandon the current activity if it is set. This is only done when such abandonment would be correct. For example, the pipe read and write code returns if the killed flag is set; eventually the code will return back to trap, which will again check the flag and exit.

Some xv6 sleep loops do not check p->killed because the code is in the middle of a multistep system call that should be atomic. The virtio driver (kernel/virtio_disk.c:242) is an example: it does not check p->killed because a disk operation may be one of a set of writes that are all needed in order for the file system to be left in a correct state. A process that is killed while waiting for disk I/O won’t exit until it completes the current system call and usertrap sees the killed flag.

如果受害者进程处于睡眠状态，kill的唤醒会使受害者从睡眠中返回。这是潜在的危险，因为正在等待的条件可能不是真的。

然而，xv6对sleep的调用总是被包裹在一个while循环中，在sleep返回后重新测试条件。一些对sleep的调用也会在循环中测试p->killed，如果设置了p->killed，则放弃当前活动。只有当这种放弃是正确的时候才会这样做。例如，管道读写代码如果设置了killed标志就会返回；最终代码会返回陷阱，陷阱会再次检查标志并退出。

一些xv6睡眠循环没有检查p->killed，因为代码处于一个多步骤的系统调用中间，应该是原子的。virtio驱动(kernel/virtio_disk.c:242)就是一个例子：它没有检查p->killed，因为磁盘操作可能是一组写操作中的一个，而这些写操作都是为了让文件系统处于一个正确的状态而需要的。一个在等待磁盘I/O时被杀死的进程不会退出，直到它完成当前的系统调用和usertrap看到被杀死的标志。

## 7.9 Real world

The xv6 scheduler implements a simple scheduling policy, which runs each process in turn. This policy is called round robin. Real operating systems implement more sophisticated policies that, for example, allow processes to have priorities. The idea is that a runnable high-priority process will be preferred by the scheduler over a runnable low-priority process. These policies can become complex quickly because there are often competing goals: for example, the operating might also want to guarantee fairness and high throughput. In addition, complex policies may lead to unintended interactions such as priority inversion and convoys. Priority inversion can happen when a low-priority and high-priority process share a lock, which when acquired by the low-priority process can prevent the high-priority process from making progress. A long convoy of waiting processes can form when many high-priority processes are waiting for a low-priority process that acquires a shared lock; once a convoy has formed it can persist for long time. To avoid these kinds of problems additional mechanisms are necessary in sophisticated schedulers.

xv6 调度器实现了一个简单的调度策略，它依次运行每个进程。这种策略被称为round robin。真正的操作系统实现了更复杂的策略，例如，允许进程有优先级。这个想法是，一个可运行的高优先级进程将被调度器优先于一个可运行的低优先级进程。这些策略可能会很快变得复杂，因为经常有相互竞争的目标：例如，操作者可能还想保证公平性和高吞吐量。此外，复杂的策略可能会导致非预期的交互，如优先级倒置和车队。当低优先级和高优先级进程共享一个锁时，就会发生优先级倒置，当低优先级进程获得锁时，就会阻止高优先级进程的进展。当许多高优先级进程都在等待一个获得共享锁的低优先级进程时，就会形成一个长长的等待进程的车队；一旦车队形成，就会持续很长时间。为了避免这类问题，在复杂的调度器中需要额外的机制。

Sleep and wakeup are a simple and effective synchronization method, but there are many others. The first challenge in all of them is to avoid the “lost wakeups” problem we saw at the beginning of the chapter. The original Unix kernel’s sleep simply disabled interrupts, which sufficed because Unix ran on a single-CPU system. Because xv6 runs on multiprocessors, it adds an explicit lock to sleep. FreeBSD’s msleep takes the same approach. Plan 9’s sleep uses a callback function that runs with the scheduling lock held just before going to sleep; the function serves as a last-minute check of the sleep condition, to avoid lost wakeups. The Linux kernel’s sleep uses an explicit process queue, called a wait queue, instead of a wait channel; the queue has its own internal lock.

睡眠和唤醒是一种简单有效的同步方法，但还有很多其他方法。所有这些方法的第一个挑战是避免我们在本章开始时看到的 "丢失唤醒 "问题。最初的Unix内核的睡眠只是禁用了中断，这已经足够了，因为Unix运行在单CPU系统上。因为xv6运行在多处理器上，所以它给睡眠增加了一个显式锁。FreeBSD 的 msleep 采用了同样的方法。Plan 9的sleep使用了一个回调函数，在进入睡眠之前，在持有调度锁的情况下运行；该函数的作用是在最后一分钟检查睡眠情况，以避免丢失唤醒。Linux内核的睡眠使用一个显式的进程队列，称为等待队列，而不是等待通道；队列有自己的内部锁。

Scanning the entire process list in wakeup for processes with a matching chan is inefficient. A better solution is to replace the chan in both sleep and wakeup with a data structure that holds a list of processes sleeping on that structure, such as Linux’s wait queue. Plan 9’s sleep and wakeup call that structure a rendezvous point or Rendez. Many thread libraries refer to the same structure as a condition variable; in that context, the operations sleep and wakeup are called wait and signal. All of these mechanisms share the same flavor: the sleep condition is protected by some kind of lock dropped atomically during sleep.

在唤醒时扫描整个进程列表，寻找有匹配的chan的进程，效率很低。一个更好的解决方案是用一个数据结构代替睡眠和唤醒中的chan，该结构上存放着睡眠的进程列表，比如Linux的等待队列。Plan 9的sleep和wakeup将该结构称为rendezvous point或Rendez。许多线程库将同一个结构称为条件变量；在这种情况下，睡眠和唤醒的操作被称为等待和信号。所有这些机制都有相同的味道：睡眠条件在睡眠过程中被某种原子掉落的锁保护。

The implementation of wakeup wakes up all processes that are waiting on a particular channel, and it might be the case that many processes are waiting for that particular channel. The operating system will schedule all these processes and they will race to check the sleep condition.

Processes that behave in this way are sometimes called a thundering herd, and it is best avoided.

Most condition variables have two primitives for wakeup: signal, which wakes up one process, and broadcast, which wakes up all waiting processes.

唤醒的实现唤醒了所有在某个特定通道上等待的进程，可能很多进程都在等待这个特定通道。操作系统会调度所有这些进程，它们会竞相检查睡眠状况。

以这种方式行事的进程有时被称为雷霆万钧，最好避免这种情况。

大多数条件变量都有两个唤醒的基元：信号（signal），唤醒一个进程；广播（broadcast），唤醒所有等待的进程。

Semaphores are often used for synchronization. The count typically corresponds to something like the number of bytes available in a pipe buffer or the number of zombie children that a process has. Using an explicit count as part of the abstraction avoids the “lost wakeup” problem: there is an explicit count of the number of wakeups that have occurred. The count also avoids the spurious wakeup and thundering herd problems.

Terminating processes and cleaning them up introduces much complexity in xv6. In most operating systems it is even more complex, because, for example, the victim process may be deep inside the kernel sleeping, and unwinding its stack requires much careful programming. Many operating systems unwind the stack using explicit mechanisms for exception handling, such as longjmp.

Semaphores通常用于同步。计数通常对应于类似于管道缓冲区中可用的字节数或一个进程拥有的僵尸子代数。使用一个显式计数作为抽象的一部分，可以避免 "丢失唤醒 "的问题：有一个显式的计数，说明已经发生的唤醒次数。这个计数也避免了虚假唤醒和雷鸣般的群组问题。

终止进程和清理进程在xv6中引入了很多复杂性。在大多数操作系统中，它甚至更加复杂，因为，例如，受害进程可能深陷在内核中睡觉，而解开它的堆栈需要很多仔细的编程。许多操作系统使用显式的异常处理机制来解除堆栈，比如longjmp。

Furthermore, there are other events that can cause a sleeping process to be woken up, even though the event it is waiting for has not happened yet. For example, when a Unix process is sleeping, another process may send a signal to it. In this case, the process will return from the interrupted system call with the value -1 and with the error code set to EINTR. The application can check for these values and decide what to do. Xv6 doesn’t support signals and this complexity doesn’t arise Xv6’s support for kill is not entirely satisfactory: there are sleep loops which probably should check for p->killed. A related problem is that, even for sleep loops that check p->killed, there is a race between sleep and kill; the latter may set p->killed and try to wake up the victim just after the victim’s loop checks p->killed but before it calls sleep. If this problem occurs, the victim won’t notice the p->killed until the condition it is waiting for occurs. This may be quite a bit later (e.g., when the virtio driver returns a disk block that the victim is waiting for) or never (e.g., if the victim is waiting from input from the console, but the user doesn’t type any input).

A real operating system would find free proc structures with an explicit free list in constant time instead of the linear-time search in allocproc; xv6 uses the linear scan for simplicity.

此外，还有其他一些事件可以导致睡眠进程被唤醒，即使它正在等待的事件还没有发生。例如，当一个Unix进程处于睡眠状态时，另一个进程可能会向它发送一个信号。在这种情况下，该进程将从中断的系统调用中返回，值为-1，错误代码设置为EINTR。应用程序可以检查这些值，并决定该怎么做。Xv6不支持信号，这种复杂性不会出现 Xv6对kill的支持并不完全令人满意：有睡眠循环可能应该检查p->killed。一个相关的问题是，即使对于检查p->killed的睡眠循环，睡眠和kill之间也存在竞赛；后者可能会设置p->killed，并试图在受害者的循环检查p->killed之后但在它调用睡眠之前唤醒受害者。如果发生这个问题，受害者不会注意到p->killed，直到它所等待的条件发生。这可能会晚很多（例如，当virtio驱动返回一个受害者正在等待的磁盘块时），也可能永远不会发生（例如，如果受害者正在等待来自控制台的输入，但用户没有键入任何输入）。

真正的操作系统会用一个显式的空闲列表在恒定时间内找到空闲的proc结构，而不是在allocproc中进行线性时间搜索；xv6为了简单起见，使用线性扫描。

## 7.10 Exercises

1- Sleep has to check lk != &p->lock to avoid a deadlock (kernel/proc.c:558-561). Suppose the special case were eliminated by replacing

```cpp
if(lk != &p->lock){
  acquire(&p->lock);
  release(lk);
}
```

with

```cpp
release(lk);
acquire(&p->lock);
```

Doing this would break sleep. How?

2- Most process cleanup could be done by either exit or wait. It turns out that exit must be the one to close the open files. Why? The answer involves pipes.

3- Implement semaphores in xv6 without using sleep and wakeup (but it is OK to use spin locks). Replace the uses of sleep and wakeup in xv6 with semaphores. Judge the result.

4- Fix the race mentioned above between kill and sleep, so that a kill that occurs after the victim’s sleep loop checks p->killed but before it calls sleep results in the victim abandoning the current system call.

5- Design a plan so that every sleep loop checks p->killed so that, for example, a process that is in the virtio driver can return quickly from the while loop if it is killed by another process.

6- Modify xv6 to use only one context switch when switching from one process’s kernel thread to another, rather than switching through the scheduler thread. The yielding thread will need to select the next thread itself and call swtch. The challenges will be to prevent multiple cores from executing the same thread accidentally; to get the locking right; and to avoid deadlocks.

7- Modify xv6’s scheduler to use the RISC-V WFI (wait for interrupt) instruction when no processes are runnable. Try to ensure that, any time there are runnable processes waiting to run, no cores are pausing in WFI.

8- The lock p->lock protects many invariants, and when looking at a particular piece of xv6 code that is protected by p->lock, it can be difficult to figure out which invariant is being enforced. Design a plan that is more clean by splitting p->lock into several locks.

1- Sleep必须检查lk != &p->lock以避免死锁(kernel/proc.c:558-561)。假设特殊情况被消除了，将其替换为

```cpp
if(lk !!!= &p->lock){ ．
  acquire(&p->lock);
  release(lk);
}
```

与

```cpp
release(lk);
acquire(&p->lock);
```

这样做会破坏睡眠。怎么做？

2-大部分进程清理可以通过exit或wait来完成。事实证明，exit必须是关闭打开的文件。为什么要这样做呢？答案涉及到管道。

3- 在xv6中实现semaphores而不使用sleep和wakeup（但使用spin locks也可以）。在xv6中用semaphores替换睡眠和唤醒的用法。判断结果。

4-修正上面提到的kill和sleep之间的竞赛，使在受害者的sleep循环检查p->killed之后但在它调用sleep之前发生的kill，导致受害者放弃当前的系统调用。

5- 设计一个计划，使每个睡眠循环都检查p->killed，这样，例如，在virtio驱动中的进程如果被其他进程杀死，就可以从while循环中快速返回。

6-修改xv6，当从一个进程的内核线程切换到另一个进程时，只使用一个上下文切换，而不是通过调度器线程切换。屈服线程需要自己选择下一个线程并调用swtch。面临的挑战将是如何防止多个内核意外执行同一个线程；如何正确地进行锁定；如何避免死锁。

7-修改xv6的调度器，当没有进程可运行时，使用RISC-V的WFI（wait for interrupt）指令。尽量保证，在任何有可运行进程等待运行的时候，没有核心在WFI中暂停。

8-锁p->锁保护了很多不变性，当看某段被p->锁保护的xv6代码时，可能很难弄清楚是执行了哪个不变性。设计一个计划，把p->锁拆成几个锁，更干净。
