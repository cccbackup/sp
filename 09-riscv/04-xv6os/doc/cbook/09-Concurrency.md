# Chapter 9 -- Concurrency revisited

Simultaneously obtaining good parallel performance, correctness despite concurrency, and understandable code is a big challenge in kernel design. Straightforward use of locks is the best path to correctness, but is not always possible. This chapter highlights examples in which xv6 is forced to use locks in an involved way, and examples where xv6 uses lock-like techniques but not locks.

同时获得良好的并行性能、并发情况下的正确性和可理解的代码是内核设计中的一大挑战。直接使用锁是实现正确性的最佳途径，但并不总是可能的。本章重点介绍了xv6被迫以涉及的方式使用锁的例子，以及xv6使用类似锁的技术但不使用锁的例子。

## 9.1 Locking patterns

Cached items are often a challenge to lock. For example, the filesystem’s block cache (kernel/bio.c:26) stores copies of up to NBUF disk blocks. It’s vital that a given disk block have at most one copy in the cache; otherwise, different processes might make conflicting changes to different copies of what ought to be the same block. Each cached block is stored in a struct buf (kernel/buf.h:1).

A struct buf has a lock field which helps ensure that only one process uses a given disk block at a time. However, that lock is not enough: what if a block is not present in the cache at all, and two processes want to use it at the same time? There is no struct buf (since the block isn’t yet cached), and thus there is nothing to lock. Xv6 deals with this situation by associating an additional lock (bcache.lock) with the set of identities of cached blocks. Code that needs to check if a block is cached (e.g., bget (kernel/bio.c:59)), or change the set of cached blocks, must hold bcache.lock; after that code has found the block and struct buf it needs, it can release bcache.lock and lock just the specific block. This is a common pattern: one lock for the set of items, plus one lock per item.

Ordinarily the same function that acquires a lock will release it. But a more precise way to view things is that a lock is acquired at the start of a sequence that must appear atomic, and released when that sequence ends. If the sequence starts and ends in different functions, or different threads, or on different CPUs, then the lock acquire and release must do the same. The function of the lock is to force other uses to wait, not to pin a piece of data to a particular agent. One example is the acquire in yield (kernel/proc.c:515), which is released in the scheduler thread rather than in the acquiring process. Another example is the acquiresleep in ilock (kernel/fs.c:289); this code often sleeps while reading the disk; it may wake up on a different CPU, which means the lock may be acquired and released on different CPUs.

Freeing an object that is protected by a lock embedded in the object is a delicate business, since owning the lock is not enough to guarantee that freeing would be correct. The problem case arises when some other thread is waiting in acquire to use the object; freeing the object implicitly frees the embedded lock, which will cause the waiting thread to malfunction. One solution is to track how many references to the object exist, so that it is only freed when the last reference disappears. See pipeclose (kernel/pipe.c:59) for an example; pi->readopen and pi->writeopen track whether the pipe has file descriptors referring to it.

缓存项目通常是锁定的一个挑战。例如，文件系统的块缓存(kernel/bio.c:26)存储了多达NBUF磁盘块的副本。一个给定的磁盘块在缓存中最多只有一个副本，这一点非常重要；否则，不同的进程可能会对本应是同一个块的不同副本进行冲突的修改。每一个缓存块都被存储在一个buf结构中(kernel/buf.h:1)。

buf结构有一个锁字段，它有助于确保每次只有一个进程使用一个给定的磁盘块。然而，这个锁是不够的：如果一个块根本不存在于缓存中，而两个进程想同时使用它怎么办？没有struct buf（因为块还没有被缓存），因此没有什么可锁定的。Xv6处理这种情况的方法是将缓存块的身份集关联一个额外的锁（bcache.lock）。需要检查某个块是否被缓存的代码（例如，bget(kernel/bio.c:59)），或者改变缓存块的集合，必须持有bcache.lock；当该代码找到它所需要的块和struct buf之后，它可以释放bcache.lock，只锁定特定的块。这是一种常见的模式：一组项的一个锁，加上每个项的一个锁。

通常情况下，获取锁的同一个函数会释放它。但更精确的看法是，在一个序列开始时获取一个锁，这个序列必须显得原子化，当这个序列结束时释放。如果序列的开始和结束在不同的函数中，或者不同的线程中，或者在不同的CPU上，那么锁的获取和释放也必须是一样的。锁的功能是强制其他使用等待，而不是将一段数据钉在某个代理上。一个例子是yield中的acquisition(kernel/proc.c:515)，它是在调度线程中释放的，而不是在获取过程中释放的。另一个例子是ilock(kernel/fs.c:289)中的acquiresleep；这段代码经常在读取磁盘时睡觉；它可能会在不同的CPU上醒来，这意味着锁可能会在不同的CPU上被获取和释放。

释放一个被嵌入锁保护的对象是一件微妙的事情，因为拥有锁并不足以保证释放会正确。当有其他线程在获取中等待使用对象时，问题就会出现；释放对象隐含地释放了嵌入的锁，这将导致等待的线程发生故障。一个解决方案是跟踪对象存在多少个引用，因此只有当最后一个引用消失时才会释放对象。参见pipeclose(kernel/pipe.c:59)的例子；pi->readopen和pi->writeopen跟踪管道是否有文件描述符引用它。

## 9.2 Lock-like patterns

In many places xv6 uses a reference count or a flag as a kind of soft lock to indicate that an object is allocated and should not be freed or re-used. A process’s p->state acts in this way, as do the reference counts in file, inode, and buf structures. While in each case a lock protects the flag or reference count, it is the latter that prevents the object from being prematurely freed.

The file system uses struct inode reference counts as a kind of shared lock that can be held by multiple processes, in order to avoid deadlocks that would occur if the code used ordinary locks. For example, the loop in namex (kernel/fs.c:626) locks the directory named by each pathname component in turn. However, namex must release each lock at the end of the loop, since if it held multiple locks it could deadlock with itself if the pathname included a dot (e.g., a/./b). It might also deadlock with a concurrent lookup involving the directory and ... 

As Chapter 8 explains, the solution is for the loop to carry the directory inode over to the next iteration with its reference count incremented, but not locked.

Some data items are protected by different mechanisms at different times, and may at times be protected from concurrent access implicitly by the structure of the xv6 code rather than by explicit locks. For example, when a physical page is free, it is protected by kmem.lock (kernel/kalloc.c:24). If the page is then allocated as a pipe (kernel/pipe.c:23), it is protected by a different lock (the embedded pi->lock). If the page is re-allocated for a new process’s user memory, it is not protected by a lock at all. Instead, the fact that the allocator won’t give that page to any other process (until it is freed) protects it from concurrent access. The ownership of a new process’s memory is complex: first the parent allocates and manipulates it in fork, then the child uses it, and (after the child exits) the parent again owns the memory and passes it to kfree. There are two lessons here: a data object may be protected from concurrency in different ways at different points in its lifetime, and the protection may take the form of implicit structure rather than explicit locks.

A final lock-like example is the need to disable interrupts around calls to mycpu() (kernel/proc.c:68). Disabling interrupts causes the calling code to be atomic with respect to timer interrupts that could force a context switch, and thus move the process to a different CPU.

在许多地方，xv6使用引用计数或标志作为一种软锁，以表明一个对象已被分配，不应该被释放或重新使用。进程的p->状态以这种方式起作用，文件、inode和buf结构中的引用计数也是如此。虽然在每种情况下，锁都会保护标志或引用计数，但正是后者防止了对象被过早释放。

文件系统将inode结构的引用计数作为一种共享锁，可以由多个进程持有，以避免代码使用普通锁时出现死锁。例如，namex(kernel/fs.c:626)中的循环依次锁住每个路径名组件命名的目录。然而，namex必须在循环结束时释放每一个锁，因为如果它持有多个锁，那么如果路径名中包含一个点(例如，a/./b)，它可能会与自己发生死锁。它也可能因为涉及目录和...的并发查找而死锁。

正如第8章所解释的那样，解决方案是让循环将目录inode带入下一次迭代，并增加其引用计数，但不锁定。

有些数据项在不同的时候受到不同机制的保护，有时可能会被xv6代码的结构隐式保护，而不是通过显式锁来防止并发访问。例如，当一个物理页是空闲的时候，它被kmem.lock（kernel/kalloc.c:24）保护。如果页面被分配为管道(kernel/pipe.c:23)，它将受到不同的锁(嵌入式pi->锁)保护。如果该页被重新分配给一个新进程的用户内存，它就不会受到锁的保护。相反，分配器不会将该页交给任何其他进程（直到它被释放），这一事实保护了它不被并发访问。一个新进程的内存的所有权是很复杂的：首先父进程在fork中分配和操作它，然后子进程使用它，（在子进程退出后）父进程再次拥有内存并将其传递给kfree。这里有两个教训：一个数据对象在其生命周期中的不同阶段可以用不同的方式保护其免受并发，保护的形式可以是隐式结构而不是显式锁。

最后一个类似锁的例子是在调用mycpu()(kernel/proc.c:68)时需要禁用中断。禁用中断会导致调用代码对定时器中断是原子性的，可能会强制上下文切换，从而将进程移到不同的CPU上。

## 9.3 No locks at all

There are a few places where xv6 shares mutable data with no locks at all. One is in the implementation of spinlocks, although one could view the RISC-V atomic instructions as relying on locks implemented in hardware. Another is the started variable in main.c (kernel/main.c:7), used to prevent other CPUs from running until CPU zero has finished initializing xv6; the volatile ensures that the compiler actually generates load and store instructions. A third are some uses of p->parent in proc.c (kernel/proc.c:398) (kernel/proc.c:306) where proper locking could deadlock, but it seems clear that no other process could be simultaneously modifying p->parent.

A fourth example is p->killed, which is set while holding p->lock (kernel/proc.c:611), but checked without a holding lock (kernel/trap.c:56).

Xv6 contains cases in which one CPU or thread writes some data, and another CPU or thread reads the data, but there is no specific lock dedicated to protecting that data. For example, in fork, the parent writes the child’s user memory pages, and the child (a different thread, perhaps on a different CPU) reads those pages; no lock explicitly protects those pages. This is not strictly a locking problem, since the child doesn’t start executing until after the parent has finished writing. It is a potential memory ordering problem (see Chapter 6), since without a memory barrier there’s no reason to expect one CPU to see another CPU’s writes. However, since the parent releases locks, and the child acquires locks as it starts up, the memory barriers in acquire and release ensure that the child’s CPU sees the parent’s writes.

有几个地方，xv6在完全没有锁的情况下共享可变数据。一个是在spinlocks的实现中，尽管人们可以把RISC-V原子指令看作是依靠硬件实现的锁。另一个是main.c(kernel/main.c:7)中的start变量，用于防止其他CPU运行，直到CPU zero完成xv6的初始化；volatile确保编译器真正产生加载和存储指令。第三个是在proc.c（kernel/proc.c:398）（kernel/proc.c:306）中对p->parent的一些使用，适当的锁定可能会死锁，但似乎很明显，没有其他进程可以同时修改p->parent。

第四个例子是p->killed，它是在持有p->锁的情况下设置的(kernel/proc.c:611)，但是在没有持有锁的情况下检查(kernel/trap.c:56)。

Xv6中包含这样的情况：一个CPU或线程写一些数据，另一个CPU或线程读数据，但没有专门的锁来保护这些数据。例如，在fork中，父线写入子线程的用户内存页，子线程（不同的线程，也许在不同的CPU上）读取这些页；没有锁明确地保护这些页。严格来说，这并不是一个锁的问题，因为子程序在父程序写完后才开始执行。这是一个潜在的内存排序问题（见第6章），因为没有内存屏障，就没有理由期望一个CPU看到另一个CPU的写入。然而，由于父代释放锁，而子代在启动时获取锁，所以获取和释放中的内存屏障确保子代的CPU看到父代的写。

## 9.4 Parallelism

Locking is primarily about suppressing parallelism in the interests of correctness. Because performance is also important, kernel designers often have to think about how to use locks in a way that achieves both correctness and good parallelism. While xv6 is not systematically designed for high performance, it’s still worth considering which xv6 operations can execute in parallel, and which might conflict on locks.

Pipes in xv6 are an example of fairly good parallelism. Each pipe has its own lock, so that different processes can read and write different pipes in parallel on different CPUs. For a given pipe, however, the writer and reader must wait for each other to release the lock; they can’t read/write the same pipe at the same time. It is also the case that a read from an empty pipe (or a write to a full pipe) must block, but this is not due to the locking scheme.

Context switching is a more complex example. Two kernel threads, each executing on its own CPU, can call yield, sched, and swtch at the same time, and the calls will execute in parallel.

The threads each hold a lock, but they are different locks, so they don’t have to wait for each other. Once in scheduler, however, the two CPUs may conflict on locks while searching the table of processes for one that is RUNNABLE. That is, xv6 is likely to get a performance benefit from multiple CPUs during context switch, but perhaps not as much as it could.

Another example is concurrent calls to fork from different processes on different CPUs. The calls may have to wait for each other for pid_lock and kmem.lock, and for per-process locks needed to search the process table for an UNUSED process. On the other hand, the two forking processes can copy user memory pages and format page-table pages fully in parallel.

The locking scheme in each of the above examples sacrifices parallel performance in certain cases. In each case it’s possible to obtain more parallelism using a more elaborate design. Whether it’s worthwhile depends on details: how often the relevant operations are invoked, how long the code spends with a contended lock held, how many CPUs might be running conflicting operations at the same time, whether other parts of the code are more restrictive bottlenecks. It can be difficult to guess whether a given locking scheme might cause performance problems, or whether a new design is significantly better, so measurement on realistic workloads is often required.

锁定主要是为了正确性而抑制并行性。因为性能也很重要，所以内核设计者经常要考虑如何使用锁的方式来实现正确性和良好的并行性。虽然xv6并没有系统性地设计高性能，但还是值得考虑哪些xv6操作可以并行执行，哪些操作可能在锁上发生冲突。

xv6中的管道是一个相当好的并行性的例子。每个管道都有自己的锁，因此不同的进程可以在不同的CPU上并行读写不同的管道。但是，对于一个给定的管道，写入者和读取者必须等待对方释放锁，他们不能同时读/写同一个管道。还有一种情况是，从空管子读（或向满管子写）必须阻塞，但这不是由于锁方案造成的。

上下文切换是一个更复杂的例子。两个内核线程，每个线程都在自己的CPU上执行，可以同时调用yield、sched和swtch，这些调用将并行执行。

线程各自持有一个锁，但它们是不同的锁，所以它们不必等待对方。但是一旦进入调度器，两个CPU可能会在锁上发生冲突，同时在进程表中搜索一个是RUNNABLE的。也就是说，xv6在上下文切换过程中很可能从多个CPU中获得性能上的好处，但可能没有那么多。

另一个例子是在不同的CPU上从不同的进程并发调用fork。这些调用可能需要互相等待pid_lock和kmem.lock，以及在进程表中搜索unused进程所需的每进程锁。另一方面，两个分叉进程可以完全并行地复制用户内存页和格式化页表页。

上述每个例子中的锁方案在某些情况下牺牲了并行性能。在每种情况下，都可以使用更复杂的设计来获得更多的并行性。是否值得取决于细节：相关操作被调用的频率、代码在持有争用锁的情况下所花费的时间、同时可能有多少CPU在运行冲突的操作、代码的其他部分是否是更多的限制性瓶颈。很难猜测一个给定的锁方案是否会导致性能问题，或者一个新的设计是否有明显的改进，所以往往需要对现实的工作负载进行测量。

## 9.5 Exercises

1- Modify xv6’s pipe implementation to allow a read and a write to the same pipe to proceed in parallel on different cores.

2- Modify xv6’s scheduler() to reduce lock contention when different cores are looking for runnable processes at the same time.

3- Eliminate some of the serialization in xv6’s fork().

1- 修改xv6的管道实现，允许对同一管道的读写在不同内核上并行进行。

2- 修改xv6的调度器()，以减少不同内核同时寻找可运行进程时的锁争用。

3- 取消xv6的fork()中的一些序列化。