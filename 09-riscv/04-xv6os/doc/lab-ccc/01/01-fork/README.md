# fork

## fork1.c

一个进程可以使用fork系统调用创建一个新进程。Fork创建一个新的进程，称为子进程，其内存内容与调用的进程，称为父进程完全相同。在父进程和子进程中，fork都会返回。在父进程中，fork返回子进程的PID；在子进程中，fork返回0。

```
user@DESKTOP-96FRN6B MSYS /d/ccc109/sp/11-os/xv6-riscv-win/lab/01/01-fork
$ ./fork1
parent: child=1634
child: exiting
child 1634 is done
```


