# Chapter 2 -- Operating system organization

一个操作系统的一个关键要求是同时支持几个活动。例如，使用第1章中描述的系统调用接口，一个进程可以用fork启动新的进程。

操作系统必须在这些进程之间对计算机的资源进行时间分配。例如，即使进程数量多于硬件CPU数量，操作系统也必须确保所有进程都有机会执行。操作系统还必须安排进程之间的隔离。也就是说，如果一个进程出现了bug，发生了故障，不应该影响到不依赖bug进程的进程。然而，完全隔离太强了，因为进程应该有可能有意地进行交互，管道就是一个例子。因此，一个操作系统必须满足三个要求：复用、隔离和交互。

本章概述了如何组织操作系统来实现这三个要求。原来有很多方法，但本文主要介绍以单片机内核为中心的主流设计，很多Unix操作系统都采用这种设计。本章还介绍了xv6进程的概述，xv6进程是xv6中的隔离单元，以及xv6启动时第一个进程的创建。

Xv6运行在多核RISC-V微处理器上，它的许多低级功能（例如，它的进程实现）是RISC-V所特有的。RISC-V是一个64位的CPU，xv6是用 "LP64 "C语言编写的，这意味着C编程语言中的long(L)和指针(P)是64位的，但int是32位的。本书假设读者在某种架构上做过一点机器级的编程，会在出现RISC-V特有的思想时介绍。RISC-V的有用参考资料是 "The RISC-V Reader: 开放架构图集》[12]。用户级ISA[2]和特权架构[1]是官方规范。

> 1. 本文所说的 "多核 "是指多个CPU共享内存，但并行执行，每个CPU有自己的一套寄存器。本文有时使用多处理器一词作为多核的同义词，尽管多处理器也可以更具体地指具有多个不同处理器芯片的计算机。

一台完整的计算机中的CPU周围都是支持硬件，其中大部分是I/O接口的形式。Xv6是为qemu的"-machine virt "选项模拟的支持硬件编写的。这包括RAM、包含启动代码的ROM、与用户键盘/屏幕的串行连接以及用于存储的磁盘。

## 2.1 Abstracting physical resources

遇到一个操作系统，人们可能会问的第一个问题是为什么要有它呢？也就是说，我们可以把图1.2中的系统调用作为一个库来实现，应用程序与之链接。在这个计划中，每个应用程序甚至可以根据自己的需要定制自己的库。应用程序可以直接与硬件资源进行交互，并以最适合应用程序的方式使用这些资源（例如，实现高或可预测的性能）。一些用于嵌入式设备或实时系统的操作系统就是以这种方式组织的。

这种库方式的缺点是，如果有多个应用程序在运行，这些应用程序必须表现良好。例如，每个应用程序必须定期放弃CPU，以便其他应用程序能够运行。如果所有的应用程序都相互信任并且没有bug，这样的合作分时方案可能是可以的。比较典型的情况是，应用程序之间互不信任，并且有bug，所以人们往往希望比合作方案提供更强的隔离性。

为了实现强隔离，禁止应用程序直接访问敏感的硬件资源，而将资源抽象为服务是很有帮助的。例如，Unix应用程序只通过文件系统的打开、读取、写入和关闭系统调用与存储进行交互，而不是直接读写磁盘。这为应用程序提供了路径名的便利，而且它允许操作系统（作为接口的实现者）管理磁盘。即使不考虑隔离问题，那些有意交互的程序（或者只是希望互不干扰）很可能会发现文件系统是一个比直接使用磁盘更方便的抽象。

同样，Unix在进程之间透明地切换硬件CPU，必要时保存和恢复寄存器状态，因此应用程序不必意识到时间共享。这种透明度使得操作系统可以共享CPU，即使一些应用程序处于无限循环中。

另一个例子是，Unix进程使用exec来建立它们的内存映像，而不是直接与物理内存交互。这使得操作系统可以决定将进程放在内存的什么位置；如果内存紧张，操作系统甚至可能将进程的部分数据存储在磁盘上。Exec还为用户提供了文件系统存储可执行程序映像的便利。

Unix进程之间的许多形式的交互都是通过文件描述符进行的。文件描述符不仅可以抽象出许多细节（例如，管道或文件中的数据存储在哪里），而且它们的定义方式也可以简化交互。例如，如果管道中的一个应用程序失败了，内核就会为管道中的下一个进程产生一个文件结束信号。

图1.2中的系统调用接口经过精心设计，既为程序员提供了方便，又提供了强隔离的可能。Unix接口并不是抽象资源的唯一方式，但事实证明它是一种非常好的方式。

## 2.2 User mode, supervisor mode, and system calls

强隔离要求应用程序和操作系统之间有一个硬边界。如果应用程序犯了错误，我们不希望操作系统失败，也不希望其他应用程序失败。相反，操作系统应该能够清理失败的应用程序并继续运行其他应用程序。为了实现强隔离，操作系统必须安排应用程序不能修改（甚至不能读取）操作系统的数据结构和指令，应用程序不能访问其他进程的内存。

CPU提供了强隔离的硬件支持。例如，RISC-V有三种模式，CPU可以执行指令：机器模式、监督者模式和用户模式。在机器模式下执行的指令具有完全的权限，一个CPU在机器模式下启动。机器模式主要用于配置计算机。Xv6在机器模式下执行几行指令，然后转为监督者模式。

在监督者模式下，CPU被允许执行特权指令：例如，启用和禁用中断，读写保存页表地址的寄存器等。如果用户模式下的应用程序试图执行一条特权指令，那么CPU不会执行该指令，而是切换到监督者模式，这样监督者模式的代码就可以终止应用程序，因为它做了不该做的事情。第1章的图1.1说明了这种组织方式。一个应用程序只能执行用户模式的指令（如加数字等），被称为运行在用户空间，而处于监督者模式的软件也可以执行特权指令，被称为运行在内核空间。运行在内核空间（或主管模式）的软件称为内核。

一个应用程序如果要调用内核函数（如xv6中的读系统调用），必须过渡到内核。CPU提供了一个特殊的指令，可以将CPU从用户模式切换到监督模式，并在内核指定的入口处进入内核。(RISC-V为此提供了ecall指令。)一旦CPU切换到监督者模式，内核就可以验证系统调用的参数，决定是否允许应用程序执行请求的操作，然后拒绝或执行。内核控制过渡到监督者模式的入口点是很重要的，如果应用程序可以决定内核的入口点，那么恶意应用程序就可以，例如，在跳过验证参数的点进入内核。

## 2.3 Kernel organization

一个关键的设计问题是操作系统的哪一部分应该在监督者模式下运行。一种可能是整个操作系统驻留在内核中，这样所有系统调用的实现都在监督者模式下运行。这种组织方式称为单片内核。

在这种组织中，整个操作系统以全硬件权限运行。这种组织方式很方便，因为操作系统设计者不必决定操作系统的哪一部分不需要全硬件权限。此外，操作系统的不同部分更容易合作。例如，一个操作系统可能有一个缓冲区缓存，可以由文件系统和虚拟内存系统共享。

单片机组织的一个缺点是操作系统的不同部分之间的接口通常是复杂的（我们将在本文的其余部分看到），因此操作系统开发者很容易犯错误。在单片机内核中，一个错误是致命的，因为在主管模式下的错误往往会导致内核失败。如果内核失败，计算机就会停止工作，因此所有的应用程序也会失败。计算机必须重启才能再次启动。

为了降低内核出错的风险，操作系统设计者可以尽量减少在主管模式下运行的操作系统代码量，而在用户模式下执行操作系统的大部分代码。这种内核组织方式称为微内核。

![](../img/Figure2.1.png)

图2.1说明了这种微内核设计。在图中，文件系统作为一个用户级进程运行。作为进程运行的操作系统服务称为服务器。为了让应用程序与文件服务器进行交互，内核提供了一种进程间通信机制，用于从一个用户模式进程向另一个进程发送消息。例如，如果一个像shell这样的应用程序想要读取或写入一个文件，它就会向文件服务器发送一个消息并等待响应。

在微内核中，内核接口由一些低级函数组成，用于启动应用程序、发送消息、访问设备硬件等。这种组织方式使得内核相对简单，因为大部分操作系统驻留在用户级服务器中。

xv6和大多数Unix操作系统一样，是以单片机内核的形式实现的。因此，xv6内核接口与操作系统接口相对应，内核实现了完整的操作系统。由于xv6不提供很多服务，所以它的内核比一些微内核小，但从概念上讲xv6是单片的。

## 2.4 Code: xv6 organization

xv6内核源码在kernel/子目录下。按照模块化的概念，源码被分成了多个文件，图2.2列出了这些文件。模块间的接口在defs.h(kernel/defs.h)中定义。

![](../img/Figure2.2.png)

## 2.5 Process overview

xv6中的隔离单位（和其他Unix操作系统一样）是一个进程。进程抽象可以防止一个进程破坏或监视另一个进程的内存、CPU、文件描述符等。它还可以防止一个进程破坏内核本身，所以一个进程不能颠覆内核的隔离机制。内核必须小心翼翼地实现进程抽象，因为一个错误的或恶意的应用程序可能会欺骗内核或硬件做一些不好的事情（例如，规避隔离）。内核用来实现进程的机制包括用户/监督模式标志、地址空间和线程的时间分割。

为了帮助实施隔离，进程抽象为程序提供了一种错觉，即它有自己的私人机器。一个进程为程序提供了一个看似私有的内存系统，或者说是地址空间，其他进程不能对其进行读写。一个进程还为程序提供了看似是自己的CPU来执行程序的指令。

Xv6使用页表（由硬件实现）给每个进程提供自己的地址空间。RISC-V页表将虚拟地址(RISC-V指令操作的地址)转换(或 "映射")为物理地址(CPU芯片发送到主存储器的地址)。

Xv6为每个进程维护一个单独的页表，定义该进程的地址空间。如图2.3所示，一个地址空间包括从虚拟地址0开始的进程的用户内存。指令排在第一位，其次是全局变量，然后是堆，最后是一个 "堆 "区域（用于malloc），进程可以根据需要扩展。有一些因素限制了进程的地址空间的最大大小：RISC-V上的指针是64位宽；在页表中查找虚拟地址时，硬件只使用低的39位；xv6只使用这39位中的38位。因此，最大地址是`2^38 - 1 = 0x3fffffffff`，即MAXVA（kernel/riscv.h:348）。在地址空间的顶端，xv6保留了一个页，用于放置一个蹦床和一个映射进程的trapframe的页，以便切换到内核，我们将在第4章中解释。

![](../img/Figure2.3.png)

xv6内核为每个进程维护了许多状态，它将这些状态收集到一个proc结构中(kernel/proc.h:86)。一个进程最重要的内核状态是它的页表、内核栈和运行状态。我们用p->xxx来表示proc结构的元素，例如，p->pagetable是指向进程页表的指针。


```cpp
enum procstate { UNUSED, USED, SLEEPING, RUNNABLE, RUNNING, ZOMBIE };

// Per-process state
struct proc {
  struct spinlock lock;

  // p->lock must be held when using these:
  enum procstate state;        // Process state
  void *chan;                  // If non-zero, sleeping on chan
  int killed;                  // If non-zero, have been killed
  int xstate;                  // Exit status to be returned to parent's wait
  int pid;                     // Process ID

  // proc_tree_lock must be held when using this:
  struct proc *parent;         // Parent process

  // these are private to the process, so p->lock need not be held.
  uint64 kstack;               // Virtual address of kernel stack
  uint64 sz;                   // Size of process memory (bytes)
  pagetable_t pagetable;       // User page table
  struct trapframe *trapframe; // data page for trampoline.S
  struct context context;      // swtch() here to run process
  struct file *ofile[NOFILE];  // Open files
  struct inode *cwd;           // Current directory
  char name[16];               // Process name (debugging)
};
```


每个进程都有一个执行线程（简称线程），执行进程的指令。一个线程可以被暂停，然后再恢复。为了在进程之间透明地切换，内核会暂停当前运行的线程，并恢复另一个进程的线程。线程的大部分状态（局部变量、函数调用返回地址）都存储在线程的栈上。

每个进程都有两个栈：用户栈和内核栈（p->kstack）。当进程在执行用户指令时，只有它的用户栈在使用，而它的内核栈是空的。当进程进入内核时（为了系统调用或中断），内核代码在进程的内核堆栈上执行；当进程在内核中时，它的用户堆栈仍然包含保存的数据，但不被主动使用。一个进程的线程在主动使用其用户栈和内核栈之间交替进行。

内核堆栈是独立的（避免用戶代碼存取），因此即使一个进程破坏了它的用户堆栈，内核也可以执行。一个进程可以通过执行RISC-V ecall指令进行系统调用。这条指令会提高硬件权限级别，并将程序计数器改为内核定义的入口点。

入口处的代码切换到内核栈，执行实现系统调用的内核指令。当系统调用完成后，内核切换回用户栈，并通过调用sret指令返回用户空间，降低硬件权限级别，恢复执行系统调用指令后的用户指令。一个进程的线程可以在内核中 "阻塞 "等待I/O，当I/O完成后，再从原地恢复。

p->state表示进程是被分配、准备运行、运行、等待I/O，还是退出。

p->pagetable以RISC-V硬件所期望的格式保存进程的页表，xv6使分页硬件在用户空间执行进程时使用进程的p->pagetable。进程的页表还作为分配给存储进程内存的物理页的地址记录。

## 2.6 Code: starting xv6 and the first process

为了使xv6更加具体，我们将概述内核如何启动和运行第一个进程。后面的章节将更详细地描述这个概述中出现的机制。

当RISC-V计算机开机时，它会初始化自己，并运行一个存储在只读存储器中的引导加载器。引导加载器将xv6内核加载到内存中。然后，在机器模式下，CPU从_entry（kernel/entry.S:6）开始执行xv6。RISC-V在禁用分页硬件的情况下启动：虚拟地址直接映射到物理地址。

entry.S 

```s
	# qemu -kernel loads the kernel at 0x80000000
        # and causes each CPU to jump there.
        # kernel.ld causes the following code to
        # be placed at 0x80000000.
.section .text
_entry:
	# set up a stack for C.
        # stack0 is declared in start.c,
        # with a 4096-byte stack per CPU.
        # sp = stack0 + (hartid * 4096)
        la sp, stack0
        li a0, 1024*4
	csrr a1, mhartid
        addi a1, a1, 1
        mul a0, a0, a1
        add sp, sp, a0
	# jump to start() in start.c
        call start
spin:
        j spin

```

start.c

```cpp
...
// entry.S needs one stack per CPU.
__attribute__ ((aligned (16))) char stack0[4096 * NCPU];
...
```


载入器将xv6内核加载到物理地址0x80000000的内存中。之所以将内核放在0x80000000而不是0x0，是因为地址范围0x0:0x80000000包含I/O设备。

kernel.ld

```ld
OUTPUT_ARCH( "riscv" )
ENTRY( _entry )

SECTIONS
{
  /*
   * ensure that entry.S / _entry is at 0x80000000,
   * where qemu's -kernel jumps.
   */
  . = 0x80000000;
...
```


_entry处的指令设置了一个栈，这样xv6就可以运行C代码。Xv6在文件start.c(kernel/start.c:11)中声明了初始栈的空间，即stack0。在_entry处的代码加载堆栈指针寄存器sp，地址为stack0+4096，也就是堆栈的顶部，因为RISC-V的堆栈是向下生长的。现在内核有了栈，_entry在start(kernel/start.c:21)处调用到C代码。

函数start执行一些只有在机器模式下才允许的配置，然后切换到主管模式。为了进入主管模式，RISC-V提供了指令mret。这个指令最常用来从上一次的调用中返回，从supervisor模式到机器模式，start并不是从这样的调用中返回，而是把事情设置得像有过这样的调用一样：它在寄存器mstatus中把以前的特权模式设置为supervisor，它通过把main的地址写入寄存器mepc来把返回地址设置为main，通过把0写入页表寄存器satp来禁用supervisor模式下的虚拟地址转换，并把所有的中断和异常委托给supervisor模式。

start.c

```cpp
// entry.S jumps here in machine mode on stack0.
void
start()
{
  // set M Previous Privilege mode to Supervisor, for mret.
  unsigned long x = r_mstatus();
  x &= ~MSTATUS_MPP_MASK;
  x |= MSTATUS_MPP_S;
  w_mstatus(x);

  // set M Exception Program Counter to main, for mret.
  // requires gcc -mcmodel=medany
  w_mepc((uint64)main);

  // disable paging for now.
  w_satp(0);

  // delegate all interrupts and exceptions to supervisor mode.
  w_medeleg(0xffff);
  w_mideleg(0xffff);
  w_sie(r_sie() | SIE_SEIE | SIE_STIE | SIE_SSIE);

  // ask for clock interrupts.
  timerinit();

  // keep each CPU's hartid in its tp register, for cpuid().
  int id = r_mhartid();
  w_tp(id);

  // switch to supervisor mode and jump to main().
  asm volatile("mret"); // 因為前面的 w_mepc((uint64)main) ，所以會跳到 main
}
```

在跳入主管模式之前，start还要执行一项任务：对时钟芯片进行编程以产生定时器中断。在完成了这些内务管理后，start通过调用mret "返回 "到监督模式。这将导致程序计数器变为main（kernel/main.c:11）。

main.c

```cpp
// start() jumps here in supervisor mode on all CPUs.
void
main()
{
  if(cpuid() == 0){
    consoleinit();
    printfinit();
    printf("\n");
    printf("xv6 kernel is booting\n");
    printf("\n");
    kinit();         // physical page allocator
    kvminit();       // create kernel page table
    kvminithart();   // turn on paging
    procinit();      // process table
    trapinit();      // trap vectors
    trapinithart();  // install kernel trap vector
    plicinit();      // set up interrupt controller
    plicinithart();  // ask PLIC for device interrupts
    binit();         // buffer cache
    iinit();         // inode cache
    fileinit();      // file table
    virtio_disk_init(); // emulated hard disk
    userinit();      // first user process
    __sync_synchronize();
    started = 1;
  } else {
    while(started == 0)
      ;
    __sync_synchronize();
    printf("hart %d starting\n", cpuid());
    kvminithart();    // turn on paging
    trapinithart();   // install kernel trap vector
    plicinithart();   // ask PLIC for device interrupts
  }

  scheduler();        
}
```

在main(kernel/main.c:11)初始化几个设备和子系统后，它通过调用userinit(kernel/proc.c:212)创建第一个进程。第一个进程执行一个用RISC-V汇编编写的小程序initcode.S（user/initcode.S:1），它通过调用exec系统调用重新进入内核。正如我们在第一章中所看到的，exec用一个新的程序（本例中是/init）替换当前进程的内存和寄存器。一旦内核完成exec，它就会在/init进程中返回到用户空间。Init (user/init.c:15)在需要时创建一个新的控制台设备文件，然后以文件描述符0、1和2的形式打开它。然后它在控制台上启动一个shell。系统已经启动了。


initcode.S

```s
# Initial process that execs /init.
# This code runs in user space.

#include "syscall.h"

# exec(init, argv)
.globl start
start:
        la a0, init
        la a1, argv
        li a7, SYS_exec
        ecall

# for(;;) exit();
exit:
        li a7, SYS_exit
        ecall
        jal exit

# char init[] = "/init\0";
init:
  .string "/init\0"

# char *argv[] = { init, 0 };
.p2align 2
argv:
  .long init
  .long 0
```

init.c

```cpp
// init: The initial user-level program

#include "kernel/types.h"
#include "kernel/stat.h"
#include "kernel/spinlock.h"
#include "kernel/sleeplock.h"
#include "kernel/fs.h"
#include "kernel/file.h"
#include "user/user.h"
#include "kernel/fcntl.h"

char *argv[] = { "sh", 0 };

int
main(void)
{
  int pid, wpid;

  if(open("console", O_RDWR) < 0){
    mknod("console", CONSOLE, 0);
    open("console", O_RDWR);
  }
  dup(0);  // stdout
  dup(0);  // stderr

  for(;;){
    printf("init: starting sh\n");
    pid = fork();
    if(pid < 0){
      printf("init: fork failed\n");
      exit(1);
    }
    if(pid == 0){
      exec("sh", argv);
      printf("init: exec sh failed\n");
      exit(1);
    }

    for(;;){
      // this call to wait() returns if the shell exits,
      // or if a parentless process exits.
      wpid = wait((int *) 0);
      if(wpid == pid){
        // the shell exited; restart it.
        break;
      } else if(wpid < 0){
        printf("init: wait returned an error\n");
        exit(1);
      } else {
        // it was a parentless process; do nothing.
      }
    }
  }
}
```

## 2.7 Real world

在现实世界中，既可以找到单片机内核，也可以找到微内核。许多Unix内核都是单片式的。例如，Linux的内核是单片的，尽管有些操作系统的功能是作为用户级服务器运行的（如窗口系统）。L4、Minix和QNX等内核是以服务器的形式组织的微内核，并在嵌入式环境中得到广泛部署。

大多数操作系统都采用了进程概念，大多数进程的外观与xv6的相似。然而，现代操作系统支持一个进程中的多个线程，以允许一个进程利用多个CPU。在一个进程中支持多个线程涉及到不少xv6没有的机制，包括潜在的接口变化（如Linux的克隆，fork的变种），以控制进程线程共享哪些方面。

## 2.8 Exercises

1 - 你可以使用gdb来观察内核到用户的第一次转换。运行make qemu-gdb。在同一目录下的另一个窗口中，运行gdb。输入gdb命令break *0x3ffffff10e，在内核中跳转到用户空间的sret指令处设置一个断点。输入continue gdb命令，gdb应该在断点处停止，即将执行sret。gdb现在应该显示它正在地址0x0处执行，在initcode.S的开始处是在用户空间。

