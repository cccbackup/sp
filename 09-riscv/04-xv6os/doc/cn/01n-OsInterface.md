# Chapter 1 -- Operating system interfaces

操作系统的工作是在多个程序之间共享一台计算机，并提供一套比硬件单独支持更有用的服务。操作系统管理和抽象低级硬件，因此，例如，文字处理机不需要关心正在使用哪种类型的磁盘硬件。操作系统在多个程序之间共享硬件，使它们能同时运行（或看起来是运行）。最后，操作系统为程序提供了可控的交互方式，使它们能够共享数据或共同工作。

一个操作系统通过一个界面为用户程序提供服务。设计一个好的界面原来是很困难的。一方面，我们希望界面简单而狭窄，因为这样更容易得到正确的实现。另一方面，我们可能会想为应用程序提供许多复杂的功能。解决这种紧张关系的诀窍是设计出依靠一些机制的接口，这些机制可以结合起来提供很多通用性。

本书以一个单一的操作系统作为具体的例子来说明操作系统的概念。该操作系统xv6提供了Ken Thompson和Dennis Ritchie的Unix操作系统[14]所介绍的基本接口，同时也模仿了Unix的内部设计。

Unix提供了一个狭窄的接口，其机制结合得很好，提供了惊人的通用性。这种接口非常成功，以至于现代操作系统--BSD、Linux、Mac OS X、Solaris，甚至在较小的程度上，微软的Windows也有类似Unix的接口。

了解xv6是了解这些系统和其他许多系统的一个良好开端。

如图1.1所示，xv6采用了传统的内核形式，一个为运行中的程序提供服务的特殊程序。每个正在运行的程序，称为进程，有包含指令、数据和堆栈的内存。指令实现了程序的计算。数据是计算作用的变量。栈组织了程序的过程调用。

一台特定的计算机通常有许多进程，但只有一个内核。

当一个进程需要调用一个内核服务时，它就会调用一个系统调用，也就是操作系统接口中的一个调用。系统调用进入内核，内核执行服务并返回。这样一个进程就在用户空间和内核空间中交替执行。

![](../img/Figure1.1.png)

内核使用CPU[1]提供的硬件保护机制来确保在用户空间执行的每个进程只能访问自己的内存。内核以实现这些保护所需的硬件权限来执行，而用户程序的执行则没有这些权限。当用户程序调用系统调用时，硬件会提高特权级别，并开始执行内核中预先安排好的函数。

内核提供的系统调用集合是用户程序看到的界面。xv6内核提供了Unix内核传统上提供的服务和系统调用的子集。

图1.2列出了xv6的所有系统调用。

![](../img/Figure1.2.png)


本章的其余部分概述了xv6的服务--处理、内存、文件描述符、管道和文件系统，并通过代码片段和讨论shell（Unix的命令行用户界面）如何使用它们来说明。shell对系统调用的使用说明了它们是如何被精心设计的。

shell是一个普通的程序，它从用户那里读取命令并执行它们。事实上，shell是一个用户程序，而不是内核的一部分，这说明了系统调用接口的力量：shell没有什么特别之处。这也意味着shell很容易替换；因此，现代Unix系统有多种shell可供选择，每种shell都有自己的用户界面和脚本功能。xv6 shell是Unix Bourne shell本质的简单实现。它的实现可以在(user/sh.c:1)找到。

## 1.1 Processes and memory

一个xv6进程由用户空间内存（指令、数据和堆栈）和内核私有的每个进程状态组成。Xv6共享进程的时间：它透明地在等待执行的进程集之间切换可用的CPU。当一个进程不执行时，xv6会保存它的CPU寄存器，在下次运行进程时恢复它们。内核为每个进程关联一个进程标识符，或PID。

一个进程可以使用fork系统调用创建一个新进程。Fork创建一个新的进程，称为子进程，其内存内容与调用的进程，称为父进程完全相同。在父进程和子进程中，fork都会返回。在父进程中，fork返回子进程的PID；在子进程中，fork返回0。例如，考虑以下用C程序语言编写的程序片段[6]。

```cpp
int pid = fork();
if(pid > 0){
  printf("parent: child=%d\n", pid);
  pid = wait((int *) 0);
  printf("child %d is done\n", pid);
} else if(pid == 0){
  printf("child: exiting\n");
  exit(0);
} else {
  printf("fork error\n");
}
```

退出系统调用使调用进程停止执行，并释放资源，如内存和打开的文件。exit需要一个整数状态参数，传统上0表示成功，1表示失败。wait系统调用返回当前进程的一个已退出（或被杀死）的子进程的PID，并将该子进程的退出状态复制到传递给wait的地址；如果调用者的子进程都没有退出，则wait等待一个子进程退出。如果调用者没有子女，wait立即返回-1。如果父代不关心子代的退出状态，可以传递一个0地址给wait。

在这个例子中，输出行可能以任何一种顺序出来，这取决于父程序还是子程序先到它的printf调用。在子程序退出后，父程序的wait返回，导致父程序打印父程序：`child 1234 is done`


```
parent: child=1234
child: exiting
```

虽然子进程最初的内存内容与父进程相同，但父进程和子进程的执行内存和寄存器不同：改变其中一个进程的变量不会影响另一个进程。例如，当wait的返回值被存储到父进程的pid中时，并不会改变子进程中的变量pid。子进程中的pid值仍然为零。

exec系统调用用从文件系统中存储的文件加载的新内存映像替换调用进程的内存。该文件必须有特定的格式，它规定了文件中哪部分存放指令，哪部分是数据，在哪条指令启动等，xv6使用ELF格式，第3章将详细讨论。当exec成功时，它并不返回到调用程序；相反，从文件中加载的指令在ELF头声明的入口点开始执行。exec需要两个参数：包含可执行文件的文件名和一个字符串参数数组。例如

```cpp
char *argv[3];
argv[0] = "echo";
argv[1] = "hello";
argv[2] = 0;
exec("/bin/echo", argv);
printf("exec error\n");
```

这个片段将调用的程序替换为运行参数列表echo hello的程序/bin/echo的实例。大多数程序都会忽略参数数组的第一个元素，也就是传统的程序名称。

v6 shell使用上述调用代表用户运行程序。shell的主要结构很简单，参见main(user/sh.c:145)。主循环用getcmd读取用户的一行输入，然后调用fork，创建shell的副本。然后调用fork，创建一个shell进程的副本。父循环调用wait，而子循环则运行命令。例如，如果用户向shell输入了 "echo hello"，那么runcmd就会被调用，参数是 "echo hello"，runcmd(user/sh.c:58)运行实际的命令。对于 "echo hello"，它会调用exec (user/sh.c:78)。如果exec成功，那么子程序将执行来自echo的指令，而不是runcmd。在某些时候，echo会调用exit，这将导致父程序从main(user/sh.c:145)中的wait返回。

你可能会问为什么fork和exec不在一个调用中合并；我们稍后会看到shell在实现I/O重定向时利用了这种分离。为了避免创建一个重复的进程，然后立即替换它（用exec）的浪费，操作内核通过使用虚拟内存技术（如copy-on-write）来优化这种用例的fork实现（见4.6节）。

Xv6隐式分配大部分用户空间内存：fork分配子程序拷贝父程序内存所需的内存，exec分配足够的内存来容纳可执行文件。一个进程如果在运行时需要更多的内存（可能是为了malloc），可以调用sbrk(n)将其数据内存增长n个字节；sbrk返回新内存的位置。

## 1.2 I/O and File descriptors

文件描述符是一个小的整数，代表一个进程可以读取或写入的内核管理对象。一个进程可以通过打开一个文件、目录或设备，或通过创建一个管道，或通过复制一个现有的描述符来获得一个文件描述符。为了简单起见，我们经常把文件描述符所指的对象称为 "文件"；文件描述符接口抽象掉了文件、管道和设备之间的差异，使它们看起来都像字节流。我们会把输入和输出称为I/O。

在内部，xv6内核使用文件描述符作为每个进程表的索引，因此每个进程都有一个从0开始的文件描述符私有空间。按照惯例，一个进程从文件描述符0(标准输入)读取数据，向文件描述符1(标准输出)写入输出，向文件描述符2(标准错误)写入错误信息。正如我们将看到的，shell利用了 惯例来实现I/O重定向和管道。shell确保它始终有三个文件描述符打开（user/sh.c:151），这些文件描述符默认是控制台的文件描述符。

读写系统调用从文件描述符命名的文件中读取字节，并向打开的文件写入字节。调用read(fd, buf, n)从文件描述符fd中最多读取n个字节，将它们复制到buf中，并返回读取的字节数。每个引用文件的文件描述符都有一个与之相关的偏移量。读取从当前文件偏移量中读取数据，然后按读取的字节数推进偏移量：随后的读取将返回第一次读取所返回的字节之后的字节。当没有更多的字节可读时，读返回零，表示文件的结束。

调用write(fd, buf, n)将buf中的n个字节写入文件描述符fd，并返回写入的字节数。少于n个字节的数据只有在发生错误时才会写入。和read一样，write在当前文件偏移量处写入数据，然后按写入的字节数将偏移量向前推进：每次写入都从上一次写入的地方开始。

下面的程序片段（它构成了程序cat的本质）将数据从标准输入复制到标准输出。如果发生错误，它就会向标准错误写入一条信息。

```cpp
char buf[512];
int n;
for(;;){
  n = read(0, buf, sizeof buf);
  if(n == 0)
    break;
  if(n < 0){
    fprintf((FILE*)2, "read error\n");
    exit(1);
  }
  if(write(1, buf, n) != n) {
    fprintf((FILE*)2, "write error\n");
    exit(1);
  }
}
```

在代码片段中需要注意的是，cat不知道它是在从文件、控制台还是管道中读取。同样，cat也不知道它是打印到控制台、文件还是其他什么地方。使用文件描述符和文件描述符 0 是输入，文件描述符 1 是输出的约定，使得 cat 的实现非常简单。

关闭系统调用会释放一个文件描述符，使它可以被未来的open、pipe或dup系统调用所自由重用（见下文）。新分配的文件描述符总是当前进程中编号最小的未使用描述符。

文件描述符和fork相互作用，使I/O重定向易于实现。Fork将父进程的文件描述符表和它的内存一起复制，这样子进程开始时打开的文件和父进程完全一样。系统调用exec替换了调用进程的内存，但保留了它的文件表。这种行为允许shell通过分叉实现I/O重定向，在子进程中重新打开选定的文件描述符，然后调用exec运行新的程序。下面是shell运行`cat < input.txt`命令的简化版代码。

```cpp
char *argv[2];
argv[0] = "cat";
argv[1] = 0;
if(fork() == 0) {
  close(0);
  open("input.txt", O_RDONLY);
  exec("cat", argv);
}
```

在子程序关闭文件描述符0后，open保证对新打开的input.txt使用该文件描述符。0将是最小的可用文件描述符。然后Cat执行时，文件描述符0（标准输入）指的是input.txt。这个序列不会改变父进程的文件描述符，因为它只修改子进程的描述符。

xv6 shell中I/O重定向的代码完全是这样工作的（user/sh.c:82）。回想一下，在代码的这一点上，shell已经分叉了子shell，runcmd将调用exec来加载新的程序。

open的第二个参数由一组用位表示的标志组成，用来控制open的工作。可能的值在文件控制(fcntl)头(kernel/fcntl.h:1-5)中定义。O_RDONLY、O_WRONLY、O_RDWR、O_CREATE和O_TRUNC，它们指示open打开文件进行读、写、或同时进行读和写，如果文件不存在，则创建文件，并将文件截断为零长度。

现在应该清楚为什么fork和exec是分开调用的：在这两个调用之间，shell有机会重定向子系统的I/O，而不干扰主shell的I/O设置。我们可以想象一个假想的结合forkexec的系统调用，但用这种调用来做I/O重定向的选择似乎很尴尬。shell可以在调用forkexec之前修改自己的I/O设置（然后取消这些修改）；或者forkexec可以将I/O重定向的指令作为参数；或者（最不吸引人的是）每个程序（比如cat）都可以被教导做自己的I/O重定向。

虽然fork复制了文件描述符表，但每个底层文件的偏移量是父子共享的。考虑这个例子。

```cpp
if(fork() == 0) {
  write(1, "hello ", 6);
  exit(0);
} else {
  wait(0);
  write(1, "world\n", 6);
}
```

在这个片段的最后，文件描述符1所附的文件将包含数据hello world。父文件中的写（由于有了wait，只有在子文件完成后才会运行）会从子文件的写结束的地方开始。这种行为有助于从shell命令的序列中产生连续的输出，比如`(echo hello; echo world) >output.txt`。

dup系统调用复制一个现有的文件描述符，返回一个新的描述符，它指向同一个底层I/O对象。两个文件描述符共享一个偏移量，就像被fork复制的文件描述符一样。这是将hello world写入文件的另一种方式。

```cpp
fd = dup(1);
write(1, "hello ", 6);
write(fd, "world\n", 6);
```

如果两个文件描述符是通过一系列的fork和dup调用从同一个原始文件描述符衍生出来的，那么这两个文件描述符共享一个偏移量。否则，文件描述符不共享偏移量，即使它们是由同一文件的打开调用产生的。Dup允许shell实现这样的命令。

`ls existing-file non-existing-file > tmp1 2>&1`。2>&1告诉shell给命令一个与描述符1重复的文件描述符2。现有文件的名称和不存在文件的错误信息都会显示在文件tmp1中。xv6 shell不支持错误文件描述符的I/O重定向，但现在你知道如何实现它了。

文件描述符是一个强大的抽象，因为它们隐藏了它们所连接的细节：一个向文件描述符1写入的进程可能是在向一个文件、控制台等设备或向一个管道写入。

## 1.3 Pipes

管道是一个小的内核缓冲区，作为一对文件描述符暴露给进程，一个用于读，一个用于写。将数据写入管道的一端就可以从管道的另一端读取数据。管道为进程提供了一种通信方式。

下面的示例代码运行程序wc，标准输入连接到管道的读端。

```cpp
int p[2];
char *argv[2];
argv[0] = "wc";
argv[1] = 0;
pipe(p);
if(fork() == 0) {
  close(0);
  dup(p[0]); // 複製 p[0] 到 fd:0
  close(p[0]);
  close(p[1]);
  exec("/bin/wc", argv); // child: 這樣 wc 會由 stdin (p[0], fd:0) 讀入，於是讀到 hello world
} else {
  close(p[0]);
  write(p[1], "hello world\n", 12); // parent: 寫入 hello world 到 p[1]
  close(p[1]);
}
```

程序调用pipe，创建一个新的管道，并将读写文件描述符记录在数组p中，经过fork后，父代和子代的文件描述符都指向管道。子程序调用close和dup使文件描述符0引用到管道的读端，关闭p中的文件描述符，并调用exec运行wc。当wc从其标准输入端读取时，它从管道讀。母体关闭管道的读端，向管道写入，然后关闭写端。

如果没有数据，管道上的读会等待数据被写入或等待所有引用写端的文件描述符被关闭；在后一种情况下，读将返回0，就像已经到达数据文件的末端一样。事实上，读会阻塞直到不可能有新的数据到达，这也是一个原因，即在执行 上面的wc：如果wc的文件描述符之一指向管道的写端，wc将永远不会看到文件结束。

xv6的shell实现管道，如 `grep fork sh.c | wc -l` 的方式类似于上面的代码（user/sh.c:100）。子进程创建一个管道来连接管道的左端和右端。然后，它为管道左端调用fork和runcmd，为右端调用fork和runcmd，并等待两者的完成。管道的右端可以是一个命令，它本身包括一个管道（例如，a | b | c），它本身会分叉两个新的子进程（一个是b，一个是c）。因此，shell可以创建一棵进程树。这棵树的叶子是命令，内部节点是等待左右子进程完成的进程。

原则上，我们可以让内部节点运行管道的左端，但正确地这样做会使实现变得复杂。考虑只做以下修改：将sh.c改为不为p->left分叉，在内部进程中运行runcmd(p->left)。然后，例如，echo hi | wc不会产生输出，因为当echo hi在runcmd中退出时，内部进程会退出，并且从未调用fork来运行管道的右端。这种不正确的行为可以通过不在runcmd中对内部进程调用exit来修正，但是这种修正使代码变得复杂：现在runcmd需要知道它是否是内部进程。当不对runcmd(p->right)进行分叉时，也会产生复杂的情况。例如，仅仅是这样的修改，sleep 10 | echo hi会立即打印 "hi"，而不是在10秒后，因为echo会立即运行并退出，而不是等待sleep结束。由于sh.c的目标是要让它像 尽可能的简单，它并不试图避免创建内部流程。

管道似乎并不比临时文件强大：管道 `echo hello world | wc`可以不用管道实现为 `echo hello world >/tmp/xyz; wc </tmp/xyz ` 在这种情况下，管道比临时文件至少有四个优势。首先，管道会自动清理自己；如果使用文件重定向，shell在完成后必须小心翼翼地删除/tmp/xyz。第二，管道可以传递任意长的数据流，而文件重定向则需要磁盘上有足够的空闲空间来存储所有数据。第三，管道可以实现管道阶段的并行执行，而文件方式则需要在第二个程序开始之前完成第一个程序。第四，如果你要实现进程间的通信，管道的阻塞读写比文件的非阻塞语义更有效率。

## 1.4 File system

xv6 文件系统提供了数据文件（包含未解释的字节数组）和目录（包含对数据文件和其他目录的命名引用）。这些目录形成一棵树，从一个称为根目录的特殊目录开始。像/a/b/c这样的路径指的是根目录/中名为b的目录内名为c的文件或名为c的目录，不以/开头的路径是相对于调用进程的当前目录进行评估的，可以通过chdir系统调用来改变。这两个代码片段都会打开同一个文件（假设所有涉及的目录都存在）。

```cpp
chdir("/a");
chdir("b");
open("c", O_RDONLY);
open("/a/b/c", O_RDONLY);
```

第一个片段将进程的当前目录改为/a/b；第二个片段既不引用也不改变进程的当前目录。

有一些系统调用来创建新的文件和目录：mkdir创建一个新的目录，用O_CREATE标志打开创建一个新的数据文件，以及mknod创建一个新的设备文件。这个例子说明了这三种方法。

```cpp
mkdir("/dir");
fd = open("/dir/file", O_CREATE|O_WRONLY);
close(fd);
mknod("/console", 1, 1);
```

Mknod创建了一个特殊的文件，它指的是一个设备。与设备文件相关联的是主要设备号和次要设备号(mknod的两个参数)，它们唯一地标识一个内核设备。

当一个进程后来打开设备文件时，内核会将系统的读写调用分流给内核设备实现，而不是将它们传递给文件系统。

一个文件的名称与文件本身是不同的；同一个基础文件，称为inode，可以有多个名称，称为链接。每个链接由目录中的一个条目组成；该条目包含一个文件名和对inode的引用。一个inode保存着一个文件的元数据，包括它的类型(文件或目录或设备)、它的长度、文件内容在磁盘上的位置以及到文件的链接数量。

fstat系统调用从文件描述符引用的inode中检索信息。它在 stat.h (kernel/stat.h)中定义的 stat 结构中填入。

```cpp
#define T_DIR 1 // Directory
#define T_FILE 2 // File
#define T_DEVICE 3 // Device

struct stat {
  int dev; // File system’s disk device
  uint ino; // Inode number
  short type; // Type of file
  short nlink; // Number of links to file
  uint64 size; // Size of file in bytes
};
```

链接系统调用创建了另一个文件系统名，引用同一个inode作为现有文件。这个片段创建了一个同时命名为a和b的新文件。

```cpp
open("a", O_CREATE|O_WRONLY);
link("a", "b");
```

从a读入或写入a与从b读入或写入b是一样的，每个inode都有一个唯一的inode号来标识。经过上面的代码序列，可以通过检查fstat的结果来确定a和b指的是同一个底层内容：两者将返回相同的inode号（ino），并且nlink计数将被设置为2。

取消链接系统调用从文件系统中删除一个名字。只有当文件的链接数为零且没有文件描述符引用它时，文件的inode和存放其内容的磁盘空间才会被释放。因此，在文件系统中添加

```cpp
unlink("a");
```

到最后一个代码序列，使inode和文件内容以b的形式被访问，此外,

```cpp
fd = open("/tmp/xyz", O_CREATE|O_RDWR);
unlink("/tmp/xyz");
```

是一种习惯性的方法，用来创建一个没有名字的临时inode，当进程关闭fd或退出时，这个inode会被清理掉。

Unix提供了可从shell调用的用户级文件实用程序，例如mkdir、ln和rm。这种设计允许任何人通过添加新的用户级程序来扩展命令行界面。事后看来，这个计划似乎是显而易见的，但在Unix时期设计的其他系统通常将这类命令内置到shell中（并将shell内置到内核中）。

一个例外是cd，它是内置在shell中的(user/sh.c:160)，cd必须改变shell本身的当前工作目录。如果cd是作为常规命令运行，那么shell将分叉一个子进程，子进程将运行cd，cd将改变子进程的工作目录。父进程（即shell）的工作目录不会改变。

## 1.5 Real world

Unix将 "标准 "文件描述符、管道和方便的shell语法结合起来对它们进行操作，是编写通用可重用程序的一大进步。这个想法引发了一种 "软件工具 "文化，Unix的强大和流行在很大程度上要归功于这种文化，shell是第一种所谓的 "脚本语言"。今天，Unix系统调用接口在BSD、Linux和Mac OS X等系统中依然存在。

Unix系统调用接口已经通过便携式操作系统接口(POSIX)标准进行了标准化。Xv6 并不符合 POSIX 标准：它缺少许多系统调用（包括基本的系统调用，如 lseek），而且它提供的许多系统调用与标准不同。我们对xv6的主要目标是简单明了，同时提供一个简单的类似UNIX的系统调用接口。一些人已经用一些更多的系统调用和一个简单的C库扩展了xv6，以便运行基本的Unix程序。然而，现代内核比xv6提供了更多的系统调用和更多种类的内核服务。例如，它们支持网络、窗口系统、用户级线程、许多设备的驱动程序等等。现代内核不断快速发展，并提供了许多超越POSIX的功能。

Unix用一套文件名和文件描述符接口统一了对多种类型资源（文件、目录和设备）的访问。这个思想可以扩展到更多种类的资源；一个很好的例子是Plan 9[13]，它把 "资源就是文件 "的概念应用到网络、图形等方面。然而，大多数Unix衍生的操作系统都没有遵循这一路线。

文件系统和文件描述符已经是强大的抽象。即便如此，操作系统接口还有其他模式。Multics是Unix的前身，它以一种使文件存储看起来像内存的方式抽象了文件存储，产生了一种截然不同的界面风味。Multics设计的复杂性直接影响了Unix的设计者，他们试图建立一些更简单的东西。

Xv6没有提供用户的概念，也没有提供保护一个用户与另一个用户的概念；用Unix的术语来说，所有的xv6进程都以root身份运行。

本书研究的是xv6如何实现其类似Unix的接口，但其思想和概念不仅仅适用于Unix。任何操作系统都必须将进程复用到底层硬件上，将进程相互隔离，并提供受控进程间通信的机制。在学习了xv6之后，您应该能够研究其他更复杂的操作系统，并在这些系统中看到xv6的基本概念。

## 1.6 Exercises

1. 编写一个程序，使用UNIX系统调用在两个进程之间通过一对管道（每个方向一个）"乒乓 "一个字节。测量该程序的性能，以每秒交换次数为单位。
