# RISC-V reference

* [Great Ideas in Computer Architecture (Machine Structures)](https://cs61c.org/fa20/)
    * CS 61C at UC Berkeley with Dan Garcia and Borivoje Nikolic - Fall 2020
    * https://github.com/61c-teach/fa20-lab-starter
