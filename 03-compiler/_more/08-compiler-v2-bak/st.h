#ifndef __ST_H__
#define __ST_H__

#include <string.h>

typedef struct _St {
  char *text, *end;
  int size;
  // char *textEnd;
} St;

extern void stNew(St *t, char *text, int size);
extern char *stAdd(St *t, char *str);
extern char *stAddn(St *t, char *str, int len);

#endif
