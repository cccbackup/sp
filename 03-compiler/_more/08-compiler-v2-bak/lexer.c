#include "lexer.h"

char *typeName[6] = {"Id", "Int", "Keyword", "Literal", "Op", "End"};
char code[TMAX], *p;

St st;
char strTable[TMAX];
char *token=NULL; 
TokenType type;
int line = 1;

char *scan() {
  while (isspace(*p)) {
    if (*p == '\n') {
      // printf("scan:line=%d\n", line);
      line++;
    }
    p++;
  }

  char *start = p;
  if (*p == '\0') {
    type = End;
  } else if (*p == '"') {
    p++;
    while (*p != '"') p++;
    p++;
    type = Literal;
  } else if (*p >='0' && *p <='9') { // 數字
    while (*p >='0' && *p <='9') p++;
    type = Int;
  } else if (isalpha(*p) || *p == '_') { // 變數名稱或關鍵字
    while (isalpha(*p) || isdigit(*p) || *p == '_') p++;
    type = Id;
  } else if (strchr("+-*/%%&|<>!=", *p) >= 0) {
    char c = *p++;
    if (*p == '=') p++; // +=, ==, <=, !=, ....
    else if (strchr("+-&|", c) >= 0 && *p == c) p++; // ++, --, &&, ||
    type = Op;
  } else {
    p++;
    type = Op;
  }

  int len = p-start;
  token = stAddn(&st, start, len);
  // printf("token = %-10s type = %-10s\n", token, typeName[type]);
  return token;
}

char *next() {
  char *tk = token;
  scan();
  return tk;
}

int isNext(char *set) {
  char eset[SMAX], etoken[SMAX];
  sprintf(eset, " %s ", set);
  sprintf(etoken, " %s ", token);
  return (!isEnd() && strstr(eset, etoken) != NULL);
}

int istype(TokenType ptype) {
  return (type == ptype);
}

int isEnd() {
  return type == End;
}

char *skip(char *set) {
  if (isNext(set)) {
    return next();
  } else {
    lerror("skip(%s) but got %s\n", set, next());
  }
}

char *skipType(TokenType ptype) {
  if (istype(ptype)) {
    return next();
  } else {
    lerror("skipType(%s) got %s fail!\n", typeName[ptype], typeName[type]);
  }
}

void lexer(char *code) {
  p = code;
  stNew(&st, strTable, sizeof(strTable));
  next();
}
