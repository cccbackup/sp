#ifndef __LEXER_H__
#define __LEXER_H__

#include "util.h"
#include "st.h"

#define lerror(...) { printf("Error at line %d: ", line); error(__VA_ARGS__); }

typedef enum { Id, Int, Keyword, Literal, Op, End } TokenType;

extern char *typeName[];
extern char code[];
extern char strTable[], *strTableEnd;
extern TokenType type;
extern int line;

extern void lexer(char *code);
extern char *next();
extern int isNext(char *set);
extern int isNextType(TokenType type);
extern int isEnd();
extern char *skip(char *set);
extern char *skipType(TokenType type);

#endif