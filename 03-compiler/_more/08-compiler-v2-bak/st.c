#include "st.h"

void stNew(St *t, char *text, int size) {
  t->text = text;
  t->size = size;
  t->end = text;
}

char *stAddn(St *t, char *str, int len) {
  char *tp = t->end;
  strncpy(t->end, str, len);
  tp[len] = '\0';
  t->end += len + 1;
  return tp;
}

char *stAdd(St *t, char *str) {
  return stAddn(t, str, strlen(str));
  /*
  char *tp = t->end;
  strcpy(t->end, str);
  t->end += strlen(str) + 1;
  return tp;
  */
}