# Compiler

main.c => lexer.c
          compiler.c
          ir.c       => irvm.c

```
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp/02-compiler/07-compiler-func (master)     
$ make clean
rm -f *.o *.exe

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp/02-compiler/07-compiler-func (master)     
$ make
gcc -std=c99 -O0 ir.c irvm.c map.c util.c lexer.c compiler.c main.c -o compiler

user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp/02-compiler/07-compiler-func (master)     
$ ./compiler test/func.c -ir
00:token = def        type = Id        
01:token = add        type = Id        
02:token = (          type = Op        
03:token = a          type = Id        
04:token = ,          type = Op        
05:token = b          type = Id        
06:token = )          type = Op        
07:token = {          type = Op        
08:token = return     type = Id        
09:token = a          type = Id        
10:token = +          type = Op        
11:token = b          type = Id
12:token = ;          type = Op        
13:token = }          type = Op        
14:token = x          type = Id        
15:token = =          type = Op
16:token = 3          type = Int       
17:token = ;          type = Op        
18:token = y          type = Id        
19:token = =          type = Op        
20:token = 5          type = Int       
21:token = ;          type = Op        
22:token = z          type = Id        
23:token = =          type = Op        
24:token = add        type = Id        
25:token = (          type = Op        
26:token = x          type = Id        
27:token = ,          type = Op        
28:token = y          type = Id        
29:token = )          type = Op        
30:token = ;          type = Op        
=======irDump()==========
00: function add 2
01: param a
02: param b
03: t1 = a
04: t2 = b
05: t3 = t1 + t2
06: return t3
07: t1 = 3
08: x = t1
09: t1 = 5
10: y = t1
11: t2 = x
12: t3 = y
13: arg t2
14: arg t3
15: call add 2
16: t1 = add
17: z = t1
```
