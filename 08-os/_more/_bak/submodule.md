# submodule

```sh
user@DESKTOP-96FRN6B MINGW64 /d/ccc109/sp (master)
$ git submodule update --init --recursive
Submodule 'mini-riscv-os' (https://github.com/cccriscv/mini-riscv-os.git) registered for path '11-os/mini-riscv-os'
Submodule 'xv6-riscv-win' (https://github.com/cccriscv/xv6-riscv-win.git) registered for path '11-os/xv6-riscv-win'
Cloning into 'D:/ccc109/sp/11-os/mini-riscv-os'...
Cloning into 'D:/ccc109/sp/11-os/xv6-riscv-win'...
Submodule path '11-os/mini-riscv-os': checked out 'b1cfa86914848a027f835308389199e37fd2e89b'
Submodule path '11-os/xv6-riscv-win': checked out 'fdd9c3ca0b69e16c55f9028e56ab666f4e1dbc9a'
```
