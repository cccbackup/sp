# 本課程使用工具

* gcc -- 包含 C 語言編譯器 gcc, C++ 編譯器 g++, 專案建置工具 make, 二進位工具 objdump, ar, string, ...
    * Dev C++/CodeBlocks/msys2 內都有，或用 choco 安裝亦可！
* git bash -- git 內附的類似 Linux bash 介面。
* msys2 -- windows 上的類似 UNIX 環境，使用 pacman 安裝程式。
* linux -- 用 ssh 連到 guest@misavo.com

## 本課程範例

這些程式使用 gcc + 類 UNIX 環境 (POSIX)，可以在 Linux, Mac 編譯執行

但要在 windows 底下執行，必須安裝 gcc (CodeBlocks 或 MSYS2)

* http://www.codeblocks.org/
* http://www.msys2.org/

安裝 CodeBlocks 之後必須要設定 gcc.exe 資料夾的路徑到系統 PATH 當中。

## 參考文獻

* [你所不知道的 C 語言](https://hackmd.io/@sysprog/c-prog)
* [Computer Science from the Bottom Up](https://www.bottomupcs.com/)
* [C 语言编程透视](https://tinylab.gitbooks.io/cbook/)
